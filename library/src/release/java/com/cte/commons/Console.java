/**
 *
 */
package com.cte.commons;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import com.cte.commons.utils.FieldInspector;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * For Debugging.<br/>
 * This class provides 3 sets of methods:
 * <ol>
 * <li>{@link #v}, {@link #d}, {@link #i}, {@link #w}, {@link #e}, which allow to print informations of "basic" types (see below)</li>
 * <li>Various {@code print*(...)} methods for printing complex objects with a proper formatting</li>
 * <li>Some utilities like {@link #notImplemented}, {@link #timedInfo}, {@link #notify} for special purposes.</li>
 * </ol>
 * 
 * The "basic" types handled by the first set of methods comprise:
 * <ul>
 * <li>all primitive types</li>
 * <li>any type that properly implements {@link Object#toString()}</li>
 * <li>arrays or iterable collections of the above types</li>
 * </ul>
 */
@SuppressWarnings("nls")
public class Console
{
	static {
		android.util.Log.i("Console", "running in release mode");
	}
	private final static String			MY_NAME				= "Console";

	/**
	 * Indicates if we can use {@link android.util.Log} class.
	 * All {@code public} methods <b>MUST</b> be encapsulated in block "{@code if (Console.DEBUG_MODE)}" in order to allow Proguard to remove code from released builds.<br/>
	 * {@link #DEBUG_MODE} <b>MUST</b> be a statically initialized constant.
	 */
	public final static boolean			DEBUG_MODE			= false;

	/** Minimum log level. Log below this level won't be printed to the console */
	public static int			DEBUG_MIN_LEVEL		= Log.ASSERT;

	/** Filter to allow debugs from ONLY the classes which filenames are in this list. Leave empty for allowing all classes */
	public final static List<String>	ALLOWED_FILENAMES	= new java.util.ArrayList<>();

	/** Filter to REMOVE debugs from the classes which filenames are in this list. Leave empty for allowing all classes */
	public final static List<String>	FILTERED_FILENAMES	= new java.util.ArrayList<>();

	/**
	 * Special modifier:
	 * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
	 * and if the following parameter is a number(*),
	 * then this number will be displayed as an hexadecimal string.<br>
	 * <br>
	 * (*): supported types are:
	 * <ul>
	 * <li>{@link Integer}, {@link Long}, {@link Byte}, {@link Short} and their primitive equivalents,</li>
	 * <li>{@link java.util.concurrent.atomic.AtomicInteger}, {@link java.util.concurrent.atomic.AtomicLong}</li>
	 * </ul>
	 */
	public final static Object			HEX					= new Object();
	/**
	 * Special modifier:
	 * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
	 * the following parameter will be displayed using a {@link com.cte.commons.utils.FieldInspector}.
	 */
	public final static Object			FIELDS				= new Object();
	/**
	 * Special modifier:
	 * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
	 * it will prevent spaces to be inserted in-between following parameters.
	 */
	public final static Object			NOSP				= new Object();
	/**
	 * Special modifier:
	 * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
	 * it will request spaces to be inserted in-between following parameters.
	 */
	public final static Object			SP					= new Object();

	/**
	 * Special modifier:
	 * When used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e}, the message will also be toasted.
	 * When used as a parameter of {@link #notImplemented}, the message will be toasted instead of notified.
	 */
	public final static Object			TOAST				= new Object();

	/**
	 * Special modifier:
	 * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
	 * it will request the message to be notified in the status bar.
	 */
	public final static Object			NOTIFY				= new Object();

	/**
	 * Special modifier:
	 * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
	 * the following parameter will be used as separator when printing arrays of {@link Object}s, {@link java.util.Map}s, {@link Iterable}s or {@link android.os.Bundle}s.
	 * If needed, revert to default using {@link #DEFAULT_DELIM} as following parameter.
	 */
	public final static Object			DELIM				= new Object();

	private final static String			DEFAULT_DELIM		= ", ";
	private static String				sDelim				= DEFAULT_DELIM;

	/** tag for debug */
	private static String				sDebugTag			= MY_NAME;

	private static char[]				sSpaces				= {};
	private static long					sStartTime			= System.currentTimeMillis();
	private static long					sDeltaTime			= sStartTime;

	/**
	 * @param debugTag
	 *            the debug tag that will be used from now on
	 * @since 1 août 2012 09:52:57
	 */
	public static void setDebugTag(final String debugTag)
	{
	}

	/**
	 * @param number
	 *            depth of stack trace
	 * @param informations
	 * @since 1 mars 2011 11:31:42
	 */
	public static void printCallStack(final int number, final Object... informations)
	{
	}

	/**
	 * 
	 * @param fileName
	 * @return {@code true} to CANCEL log, or {@code false} to allow log to be printed to console
	 * @since 25 oct. 2011 13:31:23
	 * @author cte
	 */
	private final static boolean filterOut(final String fileName)
	{
		return false;
	}

	protected static final void println(final int type, final Object... informations)
    {}

	/**
	 * @param sb
	 * @param info
	 * @since 20 avr. 2012 09:49:07
	 * @author cte
	 */
	private static void append(final StringBuilder sb, final Object info)
	{}

	/**
	 * Print some verbose
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void v(final Object... informations) {}

	/**
	 * Print some info
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void i(final Object... informations) {}

	/**
	 * Print some warning
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void w(final Object... informations) {}

	/**
	 * Print some debugs
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void d(final Object... informations) {}

	/**
	 * Print some error
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void e(final Object... informations) {}

	/**
	 * Toast a message
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void t(final Object... informations) {}

	/**
	 * Resets the current timestamp without printing anything.<br>
	 * Must be called before first call to {@link #timedInfo}.
	 * 
	 * @since 8 déc. 2011 11:53:47
	 * @author cte
	 */
	public final static void markTime() {}

	/**
	 * Print some warning with timestamp, and update the timestamp.<br>
	 * The time displayed is the time elapsed since the last call to this method or to {@link #markTime()}
	 * 
	 * @param informations
	 *            List of informations to print
	 */
	public final static void timedInfo(final Object... informations) {}

	/**
	 * Print some warning and resets timestamp
	 * 
	 * @param informations
	 * @since 22 sept. 2011 17:13:23
	 */
	public final static void r(final Object... informations) {}

	private static boolean checkNull(final String label, final Object o) {return false;}

	/**
	 * enumerates content of an Intent
	 * 
	 * @param intent
	 */
	@MediumTest
	public final static void print(final Intent intent) {}

	/**
	 * prints indented json
	 * 
	 * @param json
	 * @since 24 nov. 2011 10:59:22
	 * @author cte
	 */
	public final static void print(final JSONObject json) {}

	/**
	 * enumerates content of a Bundle
	 * 
	 * @param bundle
	 */
	public final static void print(final Bundle bundle) {}

	/**
	 * enumerates content of a ContentValues
	 * 
	 * @param values
	 */
	public final static void print(final ContentValues values) {}

	public final static void print(final Cursor cursor) {}

	/**
	 * enumerates content of a Cursor
	 * 
	 * @param cursor
	 */
	public final static void print(final Cursor cursor, final boolean autoAdjust, final boolean withNulls) {}

	/**
	 * Convenience for calling {@link #printAll(android.database.Cursor)}
	 * 
	 * @since 16 juil. 2012 18:31:16
	 * @author cte
	 */
	public final static void printAll(final Context context, final Uri uri) {}

	/**
	 * Convenience for calling {@link #print(android.database.Cursor)} with a given limit
	 * 
	 * @since 16 juil. 2012 18:31:16
	 * @author cte
	 */
	public final static void print(final Context context, final Uri uri, final int width) {}

	/**
	 * {@link #printAll(android.database.Cursor, int, int, boolean)}
	 * 
	 * @param cursor
	 * @since 16 juil. 2012 18:31:16
	 * @author cte
	 */
	public final static void printAll(final Cursor cursor) {}

	/**
	 * print all rows in a cursor
	 * 
	 * @param cursor
	 * @since 11 janv. 2012 14:25:13
	 * @author cte
	 * @param limit
	 * @param head
	 *            if {@code true}, display the first rows of the cursor (head), else the last rows (tail)
	 */
	public final static void printAll(final Cursor cursor, final int width, final int limit, final boolean head) {}

	/**
	 * @param sb
	 * @param name
	 * @since 11 janv. 2012 14:35:06
	 * @author cte
	 */
	private static void appendFixedWidth(final StringBuilder sb, final int width, final String separator, final Object... values) {}

	/**
	 * @param sb
	 * @param cursor
	 * @param singleLine
	 * @param withNulls
	 *            TODO
	 * @since 11 janv. 2012 14:15:36
	 * @author cte
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private static void extractRow(final StringBuilder sb, final Cursor cursor, final boolean singleLine, final int width, final boolean withNulls) {}

	/**
	 * Print an exception with full stacktrace
	 * 
	 * @param exception
	 *            Exception to print
	 */
	public final static void ex(final Throwable exception, final Object... informations) {}

	/**
	 * Prepare and return a formatted exception with full stacktrace
	 * 
	 * @param exception
	 *            Exception to print
	 * @param forDebug
	 * @return a StringBuilder containing the full exception trace
	 */
	public final static StringBuilder prepareException(final Throwable exception, final boolean forDebug)
	{
		final long now = System.currentTimeMillis();
		final StringBuilder sb = new StringBuilder(4096);

		if (forDebug)
		{
			sb.append("EXCEPTION---START---EXCEPTION\n");
		}

		sb.append(String.format("%tF %tT.%tL %tz : %s", now, now, now, now, exception.getMessage()));
		sb.append(" : ");
		sb.append(exception.toString());
		sb.append(" : ");
		sb.append(exception.getClass().getName());
		sb.append('\n');

		appendTrace(sb, exception.getStackTrace(), 0, exception.getCause());

		if (forDebug)
		{
			sb.append("EXCEPTION---END---EXCEPTION");
		}

		sb.trimToSize();
		return sb;
	}

	/**
	 * Append a trace to a StringBuilder and return it
	 * 
	 * @param sStringBuilder
	 *            String buffer
	 * @param trace
	 *            Trace to append
	 * @param index
	 *            Index to start in the trace
	 * @param cause
	 *            Cause of the trace
	 */
	private final static void appendTrace(final StringBuilder StringBuilder, final StackTraceElement[] trace, final int index, final Throwable cause)
	{
		if (trace == null)
		{
			return;
		}

		final int size = trace.length;
		StackTraceElement stackTraceElement;
		for (int i = index; i < size; i++)
		{
			stackTraceElement = trace[i];
			final int lineNumber = stackTraceElement.getLineNumber();
			StringBuilder.append("\t\tat ")
					.append(stackTraceElement.getClassName())
					.append('.')
					.append(stackTraceElement.getMethodName())
					.append("(");
			if (-2 == lineNumber)
			{
				StringBuilder.append("Native Method");
			}
			else
			{
				StringBuilder.append(stackTraceElement.getFileName())
						.append(":")
						.append(lineNumber);
			}
			StringBuilder.append(")\n");
		}

		if (cause != null)
		{
			StringBuilder.append("Caused by : ");
			StringBuilder.append(cause.getMessage());
			StringBuilder.append(" : ");
			StringBuilder.append(cause.getClass().getName());
			StringBuilder.append('\n');
			appendTrace(StringBuilder, cause.getStackTrace(), 0, cause.getCause());
		}
	}

	/**
	 * Log method not implemented now
	 * 
	 * @param messages
	 *            optional
	 */
	public final static void notImplemented(final Context context, final Object... messages) {}

	/**
	 * Sends a notification in status bar
	 * 
	 * @param informations
	 * @since 24 nov. 2011 09:56:45
	 * @author cte
	 */
	public final static void notify(final Context context, final Object... informations) {}

	private static void doNotify(Context context, final String contentText) {}

}

/**
 *
 */
package com.cte.commons.providers;

import android.content.UriMatcher;

/**
 * Interface defining a template used for creating tables in database.<br>
 * Typically implemented on an {@code enum}, which members are the table names.<br>
 * <br>
 * If you plan to use a {@link UriMatcher}, you should use the ordinal value of your enum when doing your {@link UriMatcher#addURI},
 * and expose a method {@code valueOf(int)} to retrieve a table's name from the value returned by {@link UriMatcher#match}.
 * 
 */
public interface ViewsDefinition
{
	/**
	 * @return SQL name of the table
	 */
	public String name();

	/**
	 * @return one of "[ [LEFT | RIGHT | FULL] OUTER | INNER ] JOIN "
	 */
	public String getType();

	/**
	 * 
	 * @return table used as the "left" source of the JOIN
	 */
	public TablesDefinition getLeftTable();

	/**
	 * 
	 * @return table used as the "right" source of the JOIN
	 */
	public TablesDefinition getRightTable();

	/**
	 * 
	 * @return extra columns definitions in the form "expr AS column-alias"
	 * @see <a href="http://sqlite.org/syntaxdiagrams.html#result-column">Sqlite doc</a>
	 * @since 15 mai 2012 17:04:34
	 * @author cte
	 */
	public String[] getExtraColumns();

	/**
	 * 
	 * @return optional where statement (may be {@code null})
	 * @since 11 mai 2012 16:04:10
	 * @author cte
	 */
	public String getWhere();

	/**
	 * 
	 * @return conditions used for the {@code JOIN ON left.condition = right.condition} statement
	 */
	public String[] getJoinOnConditions();

	/**
	 * Whether {@link #getJoinOnConditions()} is used in simple mode or in complex mode.
	 * 
	 * @return
	 */
	public boolean isSimpleJoin();

	/**
	 * 
	 * @return columns from the left table that will be excluded from the View
	 */
	public String[] getLeftExclusions();

	/**
	 * 
	 * @return columns from the right table that will be excluded from the View
	 */
	public String[] getRightExclusions();

}
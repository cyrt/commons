package com.cte.commons.providers;

import java.util.Arrays;
import java.util.List;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.v4.database.DatabaseUtilsCompat;
import android.text.TextUtils;

import com.cte.commons.Console;
import com.cte.commons.utils.ArrayUtils;
import com.cte.commons.utils.PackageUtils;

/**
 * {@link ContentProvider} that implements most facilities, with given conditions:
 * <ol>
 * <li>all tables must be defined in a {@link TablesDefinition}</li>
 * <li>the first segment of the {@link Uri}s must be the table name</li>
 * <li>the last segment of the {@link Uri}s may be an {@link BaseColumns#_ID}</li>
 * </ol>
 * Under this conditions you'll just need to implement {@link #onCreateDbHelper} and {@link #getKeysForInsertOrUpdate}<br>
 * This provider has a special implementation of {@link #bulkInsert} that works in either one of these 2 modes:
 * <ul>
 * <li>"fast bulk insert" which uses a single db transaction for the whole operation (instead the default of one transaction per operation)</li>
 * <li>"update or insert" logic, which is also a single-transaction operation.</li>
 * </ul>
 * Which mode isd used is defined by the return value of {@link #getKeysForInsertOrUpdate}.<br>
 * Implementation notes:
 * <ol>
 * <li>Be sure to provide a constant AUTHORITY initialized with {@link PackageUtils#getProviderAuthority(Class)}</li>
 * <li>You may use {@link PackageUtils#getProviderUri}(AUTHORITY) to make your base {@link Uri} as AUTHORITY_URI</li>
 * <li>Provide constants for your various {@link Uri}s in the form AUTHORITY_URI/TABLE_NAME/...</li>
 * <li>You may define additional path segments or additional {@link Uri} but you will have to override {@link #extractFromUri(Uri)} to take them in account</li>
 * <li>If you use {@link Uri}s with more path segments but don't override {@link #extractFromUri(Uri)}, those additional segments will just be ignored
 * </ol>
 * 
 * @since 13 mars 2012 17:52:02
 * @author cte
 */
public abstract class AbstractContentProvider extends ContentProvider
{

	static boolean	debug	= false;

	public static void setDebug(final boolean debug)
	{
		AbstractContentProvider.debug = debug;
	}

	private static final String			mSQL	= "SQL";
	private static final String			mINS	= "INSERT";
	private static final String			mUPD	= "UPDATE";

	protected AbstractSQLiteOpenHelper	mDbHelper;

	/** initialized with getContext().getContentResolver() */
	protected ContentResolver			mContentResolver;

	protected Uri						mDatabaseUri;
	protected Uri						mUriBaseName;
	protected String					mContentTypeHeader;

	protected UriMatcher				mUriMatcher;

	/**
	 * Stores the information recovered from an {@link Uri} about the targeted table and the additional selection arguments,<br>
	 * and updates a selection and its selectionArgs accordingly.
	 * 
	 */
	public static class ExtractedTableInformation
	{
		private static final String[]	ROWID				= { BaseColumns._ID };
		public String					table;

		public String[]					additionalColumns	= null;
		public String[]					additionalValues	= null;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 * 
		 * @since 19 avr. 2012 10:03:38
		 */
		@Override
		public String toString()
		{
			if (null == this.additionalColumns)
			{
				return "Table: " + this.table;
			}
			return "Table: " + this.table + ", " + TextUtils.join(",", this.additionalColumns) + " / " + TextUtils.join(",", this.additionalValues);
		}

		/**
		 * 
		 * @since 30 mars 2012 14:58:21
		 * @author cte
		 * @param table
		 * @param rowId
		 *            if >= 0, {@link BaseColumns#_ID} of the row to update/delete (e.g. obtained with {@link ContentUris#parseId(Uri)})
		 * @param additionalColumns
		 *            may be {@code null}. If not {@code null}, must be columns from a correct where statement (e.g. "COL1 = val1 AND COL2 <> val2")
		 * @param additionalValues
		 *            values corresponding to <b>additionalColumns</b>, or {@code null}
		 */
		public ExtractedTableInformation(final String table, final long rowId, final String[] additionalColumns, final String[] additionalValues)
		{
			this.table = table;

			if (rowId > -1)
			{
				this.additionalColumns = DatabaseUtilsCompat.appendSelectionArgs(additionalColumns, ROWID);
				this.additionalValues = DatabaseUtilsCompat.appendSelectionArgs(additionalValues, new String[] { String.valueOf(rowId) });
			}
			else
			{
				this.additionalColumns = additionalColumns;
				this.additionalValues = additionalValues;
			}
		}

		/**
		 * @param where
		 * @return where String updated with our stored values
		 * @since 30 mars 2012 15:18:41
		 */
		public String updateWhere(final String where)
		{
			if (null == this.additionalColumns || 0 == this.additionalColumns.length)
			{
				return where;
			}

			final StringBuilder sb = new StringBuilder();

			if (!TextUtils.isEmpty(where))
			{
				sb.append("(")
						.append(where)
						.append(") AND ");
			}

			sb.append(TextUtils.join(" = ? AND ", this.additionalColumns))
					.append(" = ?");

			return sb.toString();
		}

		/**
		 * 
		 * @param whereArgs
		 * @return selection args updated with our stored values
		 * @since 30 mars 2012 15:26:15
		 * @author cte
		 */
		public String[] updateWhereArgs(final String[] whereArgs)
		{
			if (null == this.additionalValues || 0 == this.additionalValues.length)
			{
				return whereArgs;
			}

			return DatabaseUtilsCompat.appendSelectionArgs(whereArgs, this.additionalValues);
		}

		/**
		 * Update values with our stored values
		 * 
		 * @param values
		 * @return a reference to values for chaining
		 * @since 30 mars 2012 15:28:17
		 * @author cte
		 */
		public ContentValues updateValues(final ContentValues values)
		{
			if (null == this.additionalColumns || 0 == this.additionalColumns.length)
			{
				return values;
			}

			final int length = this.additionalColumns.length;
			final ContentValues updated = values != null ? values : new ContentValues();
			for (int i = 0; i < length; i++)
			{
				updated.put(this.additionalColumns[i], this.additionalValues[i]);
			}

			return updated;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#onCreate()
	 * 
	 * @since 13 mars 2012 17:48:56
	 */
	@Override
	public boolean onCreate()
	{
		this.mContentResolver = getContext().getContentResolver();

		this.mDbHelper = onCreateDbHelper();
		return null != this.mDbHelper;
	}

	/**
	 * Callback invoked when an instance of a {@link SQLiteOpenHelper} is required, during {@link #onCreate()}.
	 * 
	 * @return your implementation of {@link AbstractSQLiteOpenHelper}
	 */
	protected abstract AbstractSQLiteOpenHelper onCreateDbHelper();

	@Override
	public Cursor query(final Uri uri, final String[] columns, final String where, final String[] whereArgs, final String sortOrder)
	{
		final ExtractedTableInformation info = extractFromUri(uri);

		if (info == null)
		{
			throw new IllegalArgumentException(uri.toString() + " is not allowed for queries");
		}

		return this.mDbHelper.getReadableDatabase().query(
				/** FROM */
				info.table,
				/** COLS */
				columns,
				/** WHERE */
				info.updateWhere(where), info.updateWhereArgs(whereArgs),
				/** GROUP */
				null, null,
				/** ORDER */
				sortOrder);
	}

	@Override
	public Uri insert(final Uri uri, final ContentValues values)
	{
		if (debug)
		{
			Console.markTime();
		}
		final ExtractedTableInformation info = extractFromUri(uri);

		if (info == null)
		{
			throw new IllegalArgumentException(uri.toString() + " is not allowed for inserts");
		}

		final long id = this.mDbHelper.getWritableDatabase().insert(
				/** INTO */
				info.table,
				/** NULLS */
				null,
				/** VALUES */
				info.updateValues(values));

		final Uri resultUri = ContentUris.withAppendedId(uri, id);

		this.mContentResolver.notifyChange(resultUri, null);

		if (debug)
		{
			Console.timedInfo(mSQL, mINS, "id", id, "in", info.table, "& notifying", resultUri);
		}
		return resultUri;
	}

	@Override
	public int update(final Uri uri, final ContentValues values, final String where, final String[] whereArgs)
	{
		if (debug)
		{
			Console.markTime();
		}
		final ExtractedTableInformation info = extractFromUri(uri);

		if (info == null)
		{
			throw new IllegalArgumentException(uri.toString() + " is not allowed for updates");
		}

		try
		{
			final int count = this.mDbHelper.getWritableDatabase().update(
					/** TABLE */
					info.table,
					/** VALUES */
					values,
					/** WHERE */
					info.updateWhere(where), info.updateWhereArgs(whereArgs)
					);

			if (count > 0)
			{
				this.mContentResolver.notifyChange(uri, null);
				Console.timedInfo(mSQL, mUPD, count, "rows in", info.table, "& notifying", uri);
			}

			if (debug)
			{
				Console.timedInfo(mSQL, mUPD, count, "rows in", info.table);
			}
			return count;
		}
		catch (final Exception e)
		{
			Console.ex(e);
			Console.e(info);
			Console.print(values);
			Console.e("where", where, "whereArgs", whereArgs);
			Console.e("where", info.updateWhere(where), "whereArgs", info.updateWhereArgs(whereArgs));
			return 0;
		}
	}

	@Override
	public int delete(final Uri uri, final String where, final String[] whereArgs)
	{
		final ExtractedTableInformation info = extractFromUri(uri);

		if (info == null)
		{
			throw new IllegalArgumentException(uri.toString() + " is not allowed for deletes");
		}

		final int count = this.mDbHelper.getWritableDatabase().delete(
				/** FROM */
				info.table,
				/** WHERE */
				info.updateWhere(where), info.updateWhereArgs(whereArgs)
				);

		if (count > 0)
		{
			this.mContentResolver.notifyChange(uri, null);
		}

		if (debug)
		{
			Console.v("Deleted", count, "rows in", info.table, "from", uri);
		}
		return count;
	}

	/**
	 * Fast implementation of {@link ContentProvider#bulkInsert} that supports 2 modes of operation:
	 * <ol>
	 * <li>{@link #fastBulkInsert}</li>
	 * <li>{@link #bulkUpdateOrInsert}</li>
	 * </ol>
	 * The choice between these 2 modes is determined by the return value of {@link #getKeysForInsertOrUpdate}
	 * 
	 * @see android.content.ContentProvider#bulkInsert(android.net.Uri, android.content.ContentValues[])
	 * @since 13 mars 2012 14:59:11
	 */
	@Override
	public int bulkInsert(final Uri uri, final ContentValues[] values)
	{
		// safeguard for empty arrays
		if (ArrayUtils.isEmpty(values))
		{
			return 0;
		}

		if (debug)
		{
			Console.markTime();
			// Console.v(uri);
		}

		final ExtractedTableInformation info = extractFromUri(uri);
		final String[] keys = getKeysForInsertOrUpdate(uri, values[0]);

		if (null != keys && keys.length > 0)
		{
			return bulkUpdateOrInsert(uri, info, values, keys);
		}

		return fastBulkInsert(uri, info, values);
	}

	/**
	 * Utility function for {@link #bulkInsert} that allows to switch between "insert" and "update or insert" logic.<br>
	 * Depending on the return value, {@link #bulkInsert} will perform:
	 * <ul>
	 * <li>{@link #fastBulkInsert}, when returning {@code null} (indicates that the table doesn't support "update or insert" method and works only with regular inserts).</li>
	 * <li>{@link #bulkUpdateOrInsert}, when returning an array of keys (first attempt to "update where whereKeys = ?" and then insert if no line is updated)</li>
	 * </ul>
	 * Your implementation may just return {@code null} if you never want to use {@link #bulkUpdateOrInsert} in your implementation.
	 * 
	 * @param uri
	 * @param value
	 *            the {@link ContentValues} that are about to be inserted/updated
	 * @return the array of keys used to build a where clause for an update
	 * @since 13 mars 2012 15:10:31
	 * @author cte
	 */
	protected abstract String[] getKeysForInsertOrUpdate(Uri uri, ContentValues value);

	/**
	 * Optimized implementation of {@link #bulkInsert(Uri, ContentValues[])} that uses a SQL transaction for faster operation.
	 * 
	 * @param uri
	 * @param valuesArray
	 * @return
	 * @since 23 févr. 2012 13:29:05
	 * @author cte
	 */
	private int fastBulkInsert(final Uri uri, final ExtractedTableInformation info, final ContentValues[] valuesArray)
	{
		final String tableName = info.table;
		int totalInsertCounter = 0;

		final SQLiteDatabase writableDB = this.mDbHelper.getWritableDatabase();
		writableDB.beginTransaction();
		try
		{
			for (final ContentValues values : valuesArray)
			{
				if (null != values)
				{
					final long rowID = writableDB.insert(
							/** INTO */
							tableName,
							/** NULLS */
							null,
							/** VALUES */
							info.updateValues(values));
					if (-1 != rowID)
					{
						totalInsertCounter++;
						// Console.i(mSQL, mINS, "OK", "id=", rowID, "in", tableName, "for", args[0], "with", values);
					}
					else
					{
						Console.w(mSQL, mINS, "NOK", "id=", rowID, "in", tableName, "with", values);
					}
				}
			}

			if (totalInsertCounter > 0)
			{
				writableDB.setTransactionSuccessful();
				getContext().getContentResolver().notifyChange(uri, null);

				if (debug)
				{
					Console.timedInfo("bulkInsert on", uri, "with", totalInsertCounter, "inserts");
				}
			}
			else
			{
				if (debug)
				{
					Console.timedInfo("bulkInsert on", uri, " : 0 operations!");
				}
			}
		}
		catch (final Exception e)
		{
			Console.w("SQL Exception during bulkInsertOrUpdate", e.toString(), "in", tableName);
		}
		finally
		{
			writableDB.endTransaction();
		}

		return totalInsertCounter;
	}

	/**
	 * Variant of {@link #bulkInsert} that first tries to update an existing record, and will insert only if it's not found.<br>
	 * The where clause for the SQL update will be built with the values from <b>whereKeys</b>.
	 * It is the responsibility of the caller to ensure that ALL the {@link ContentValues} in <b>valuesArray</b> contains values for all the keys in <b>whereKeys</b>.
	 * 
	 * @param uri
	 * @param valuesArray
	 * @return
	 * @since 23 févr. 2012 13:29:05
	 * @author cte
	 */
	private int bulkUpdateOrInsert(final Uri uri, final ExtractedTableInformation info, final ContentValues[] valuesArray, final String... whereKeys)
	{
		final String tableName = info.table;
		int operationsProcessed = 0;

		int debugTotalInsert = 0; // only for debug
		int debugtotalUpdate = 0; // only for debug

		final int nbKeys = whereKeys.length;
		;
		String where = TextUtils.join(" = ? AND ", whereKeys) + " = ?";
		where = info.updateWhere(where);
		String[] args = new String[nbKeys];
		Arrays.fill(args, "");
		args = info.updateWhereArgs(args);

		final SQLiteDatabase writableDB = this.mDbHelper.getWritableDatabase();
		writableDB.beginTransaction();
		try
		{
			main: for (final ContentValues v : valuesArray)
			{
				final ContentValues values = info.updateValues(v);
				if (null != values)
				{
					for (int i = 0; i < nbKeys; i++)
					{
						// note: getAsString is safe for every type of value in ContentValues
						final String key = whereKeys[i];
						args[i] = values.getAsString(key);

						if (TextUtils.isEmpty(args[i]))
						{
							Console.e(mSQL, "bulkInsert SKIPPED in", tableName, "because unicity constraint on", key, "was not specified for:", values);
							continue main;
						}
					}

					long rowID = -1;
					final int modifiedLines = writableDB.update(
							/** TABLE */
							tableName,
							/** VALUES */
							values,
							/** WHERE */
							where, args);

					if (0 == modifiedLines) // the row does not exist : so we need an insert
					{
						rowID = writableDB.insert(tableName, null, values);
						if (-1 != rowID)
						{
							debugTotalInsert++;
							// Console.i(mSQL, mINS, "OK", "id=", rowID, "in", tableName, "for", args[0], "with", values);
						}
						else
						{
							if (debug)
							{
								Console.w(mSQL, mINS, "NOK", "id=", rowID, "in", tableName, "for", args[0], "with", values);
							}
						}
					}
					else
					{
						debugtotalUpdate += modifiedLines;
						// Console.i(mSQL, mUPD, "OK", "nb=", modifiedLines, "line(s) in", tableName , "for" , args[0] , "with" , values);
					}

					if (modifiedLines > 0 || rowID >= 0)
					{
						operationsProcessed++;
					}
				}
			}

			if (operationsProcessed > 0)
			{
				writableDB.setTransactionSuccessful();
				getContext().getContentResolver().notifyChange(uri, null);

				if (debug)
				{
					Console.timedInfo("bulkInsert on", uri,
							"with", operationsProcessed, "operations (inserts:", debugTotalInsert, ", updates:", debugtotalUpdate, ")");
				}
			}
			else
			{
				if (debug)
				{
					Console.timedInfo("bulkInsert on", uri, "with", operationsProcessed, " : 0 operations !!!");
				}
			}
		}
		catch (final Exception e)
		{
			Console.w("SQL Exception during bulkInsertOrUpdate", e.toString(), "in", tableName);
		}
		finally
		{
			writableDB.endTransaction();
		}

		return operationsProcessed;
	}

	/**
	 * @param tableName
	 * @return an Uri compatible with {@link #extractFromUri}
	 * @since 30 mars 2012 14:48:30
	 * @author cte
	 */
	public Uri getUriFor(final String tableName)
	{
		return Uri.withAppendedPath(this.mDatabaseUri, tableName);
	}

	/**
	 * 
	 * Method called by default implementations of {@link #query}, {@link #insert}, {@link #update} and {@link #delete} to recover contextual information from the {@link Uri} (i.e. table name, row Id, and additional selection args for other complex
	 * semantics of the Uri).
	 * This implementation parses Uri in one of the following forms:
	 * <ol>
	 * <li>content://mAuthority/TABLE</li>
	 * <li>content://mAuthority/TABLE/rowId</li>
	 * </ol>
	 * The validity of the TABLE won't be verified.<br>
	 * Override this to implement your own {@link URIMatcher} or other method if your Uris don't follow this pattern.
	 * 
	 * @param uri
	 * @return table / rowId found in the uri
	 * @author cte
	 */
	protected ExtractedTableInformation extractFromUri(final Uri uri)
	{
		final List<String> path = uri.getPathSegments();

		final int size = path.size();

		if (size < 1)
		{
			return null;
		}

		final String table = path.get(0);

		long rowId;
		if (size > 1)
		{
			try
			{
				rowId = Long.parseLong(path.get(path.size() - 1));
			}
			catch (final NumberFormatException e)
			{
				Console.ex(e);
				return null;
			}
		}
		else
		{
			rowId = -1;
		}

		return new ExtractedTableInformation(table, rowId, null, null);
	}

	@Override
	public String getType(final Uri uri)
	{
		final ExtractedTableInformation extractedTableInformation = extractFromUri(uri);

		if (extractedTableInformation == null)
		{
			throw new IllegalArgumentException("Unknown uri : " + uri);
		}

		return this.mContentTypeHeader + "" + extractedTableInformation.table;
	}

	/**
	 * Simple helper that associates {@link Uri} whose path starts with the table's name, with the table's ordinal.<br>
	 * Takes in account uris like <code>"/TABLE"</code>, <code>"/TABLE/12"</code>, <code>"/TABLE/SUBFILTER/9"</code>.
	 * 
	 * @param authority
	 * @param matcher
	 * @param tables
	 */
	protected void populateMatcher(final String authority, final TablesDefinition[] tables)
	{
		final UriMatcher matcher = this.mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		for (final TablesDefinition t : tables)
		{
			final String name = t.name();
			final int ordinal = t.ordinal();
			matcher.addURI(authority, name, ordinal);
			matcher.addURI(authority, name.concat("/#"), ordinal);
			matcher.addURI(authority, name.concat("/*/#"), ordinal);
		}
	}

	/**
	 * Finds the table name associated with the given {@link Uri}.<br>
	 * Sample code: <code>
<pre>final int match = this.mUriMatcher.match(uri);
if (UriMatcher.NO_MATCH == match)
{
	return null;
}
return MessagesContract.Tables.valueOf(match).name();</pre>
		</code>
	 * 
	 * @param uri
	 * @return
	 */
	protected String match(final Uri uri)
	{
		throw new UnsupportedOperationException("You must override this method");
	}
}
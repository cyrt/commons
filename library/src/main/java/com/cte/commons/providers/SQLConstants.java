/**
 *
 */
package com.cte.commons.providers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 * Interface defining constants for handling SQLite data types.<br>
 * <br>
 * SQLite type affinities are:
 * <ol>
 * <li><b>NULL.</b> The value is a NULL value. Cannot be used as a constraint in a column definition.</li>
 * <li><b>INTEGER.</b> The value is a signed integer, stored in 1, 2, 3, 4, 6, or 8 bytes depending on the magnitude of the value.</li>
 * <li><b>REAL.</b> The value is a floating point value, stored as an 8-byte IEEE floating point number.</li>
 * <li><b>TEXT.</b> The value is a text string, stored using the database encoding (UTF-8, UTF-16BE or UTF-16LE). Please note that unlike SQL, sqlite does NOT limit the length of a string (constraint VARCHAR(x) is silently converted to TEXT)</li>
 * <li><b>BLOB.</b> The value is a blob of data, stored exactly as it was input.</li>
 * </ol>
 * Comparing the SQLite affinities to the values stored in a {@link ContentValues}:
 * <ul>
 * <li>{@link Boolean}, {@link Byte}, {@link Short}, {@link Integer}, {@link Long} are stored as <b>INTEGER</b></li>
 * <li>{@link Float}, {@link Double} are stored as <b>REAL</b></li>
 * <li>{@link String} is stored as <b>TEXT</b></li>
 * <li>{@code byte}[] is stored as <b>BLOB</b></li>
 * </ul>
 * Furthermore, the {@link SQLiteDatabase} and the sqlite engine
 * convert data types when inserting, updating, querying or deleting values: <b>INTEGER</b> and <b>REAL</b> types can be handled as {@link String}s in both ways (e.g. {@link String#valueOf} will be invoked for you).
 * Only <b>BLOB</b> must only be handled as byte[].
 * </ul>
 * This means that all columns but <b>BLOB</b>s can always be recovered with {@link Cursor#getString},
 * and set with {@link ContentValues#put(String, String)} (just make sure the {@link String} actually contains a valid number if the target is a numeric columns).
 */
@SuppressWarnings("nls")
public interface SQLConstants extends BaseColumns
{

	static final String				SQL_INT						= "INTEGER";
	static final String				SQL_INT_DEFZERO				= "INTEGER DEFAULT 0";

	static final String				SQL_BOOL					= "INTEGER DEFAULT 0";

	static final String				SQL_TXT						= "TEXT";
	static final String				SQL_TXT_CASE_INSENSITIVE	= "TEXT COLLATE NOCASE";

	static final String				SQL_BLOB					= "BLOB";

	static final String				SQL_REAL_NOTNUL				= "REAL NOT NULL";

	/** constraint for _ID column */
	static final String				SQL_INT_PK_AUTO				= "INTEGER PRIMARY KEY AUTOINCREMENT";

	/**
	 * All tables generated with an {@link AbstractSQLiteOpenHelper} will have {@link BaseColumns#_ID} as first column.
	 * Other column indexes start at 1.
	 */
	public static final int			COL_ID						= 0;

	/**
	 * Projection containing only the column {@link BaseColumns#_ID}
	 */
	public static final String[]	PROJECTION_ID				= { _ID };

	/**
	 * Standard where clause for matching on id.
	 */
	public static final String		WHERE_ID					= _ID + " = ?";
}

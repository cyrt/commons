/**
 *
 */
package com.cte.commons.providers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;

import com.cte.commons.App;
import com.cte.commons.utils.ArrayUtils;

/**
 * 
 * @since 16 oct. 2012 15:45:38
 * @author cte
 */
public class ContentHelper
{
	private ContentHelper()
	{
		// can not instantiate
	}

	private static final ConcurrentHashMap<Uri, List<ContentValues>>	BULK_PENDING_OPERATIONS	= new ConcurrentHashMap<Uri, List<ContentValues>>();
	private static final long											BULK_PENDING_THROTTLE	= 250L;
	private static AtomicBoolean										sPendingBulkScheduled	= new AtomicBoolean(false);

	/**
	 * Convenience shortcut for {@link ContentHelper#delayedBulkInsert(Uri, List)} with 1 element
	 * 
	 * @param uri
	 * @param values
	 * @since 9 janv. 2012 12:58:52
	 * @author cte
	 */
	public static void delayedBulkInsert(final Uri uri, final ContentValues values)
	{
		final ArrayList<ContentValues> list = new ArrayList<ContentValues>(1);
		list.add(values);

		delayedBulkInsert(uri, list);
	}

	/**
	 * Records uri and values to be bulk-inserted in database in near future.<br>
	 * This allows to accumulate multiple update/insert operations in one bigger operation.<br>
	 * This method guarantees that the actual database update will occur at most {@value #BULK_PENDING_THROTTLE}ms after the call.
	 * 
	 * @param uri
	 * @param values
	 * @since 23 déc. 2011 09:07:40
	 * @author cte
	 */
	public static void delayedBulkInsert(final Uri uri, final List<ContentValues> values)
	{
		// store new values into temporay map
		synchronized (ContentHelper.BULK_PENDING_OPERATIONS)
		{
			final List<ContentValues> pendingContentValues = ContentHelper.BULK_PENDING_OPERATIONS.get(uri);
			if (null == pendingContentValues)
			{
				ContentHelper.BULK_PENDING_OPERATIONS.put(uri, values);
			}
			else
			{
				pendingContentValues.addAll(values);
			}
		}

		// scheduling delayed bulkInserts in database
		// it will occur at most once every BULK_PENDING_THROTTLE ms

		// checking if a pendingBulkInsert is already scheduled. if true do nothing
		if (ContentHelper.sPendingBulkScheduled.compareAndSet(false, true))
		{
			new AsyncTask<Void, Void, Void>()
			{
				@Override
				protected Void doInBackground(final Void... params)
				{
					SystemClock.sleep(ContentHelper.BULK_PENDING_THROTTLE);
					ContentHelper.executePendingBulkInsert();
					return null;
				}
			}.execute();
		}
	}

	private static void executePendingBulkInsert()
	{
		// resetting sPendingBulkScheduled so next call to delayedBulkInsert can start again
		sPendingBulkScheduled.set(false);
		synchronized (BULK_PENDING_OPERATIONS)
		{
			for (final Uri uri : BULK_PENDING_OPERATIONS.keySet())
			{
				final List<ContentValues> values = BULK_PENDING_OPERATIONS.remove(uri);
				App.getResolver().bulkInsert(uri, values.toArray(new ContentValues[values.size()]));
			}
		}
	}

	/**
	 * Helper method to update a row in a database, or insert it if it does not exist.<br>
	 * <b>key</b> is the name of the column used to decide which row(s) must be updated. A value for <b>key</b> must be present in <b>values</b>.<br>
	 * Note: this method is not designed for speed and should not be used in batches.
	 * 
	 * @param context
	 * @param contentUri
	 * @param values
	 *            to update, or to insert
	 * @param key
	 * @since 16 oct. 2012 15:39:42
	 * @author cte
	 */
	public static void slowInsertOrUpdate(final Context context, final Uri contentUri, final ContentValues values, final String key)
	{
		final ContentResolver resolver = context.getContentResolver();

		final boolean mustInsert = 0 == resolver.update(contentUri, values, key + " = ?", new String[] { values.getAsString(key) });
		if (mustInsert)
		{
			resolver.insert(contentUri, values);
		}
	}

	/**
	 * Builds a "column IN (?,?,?,?,?,?)" suitable to use with items as whereArgs
	 * 
	 * @param column
	 * @param items
	 * @return IN clause
	 * @since 10 janv. 2013 13:12:42
	 * @author cte
	 */
	public static String makeInClause(final String column, final String[] items)
	{
		final StringBuilder clause = new StringBuilder().append(column).append(" IN (");
		final int size = items.length;
		for (int i = 0; i < size; i++)
		{
			clause.append("?,");
		}
		clause.setLength(clause.length() - 1);
		clause.append(')');

		return clause.toString();
	}

	public static Bundle cursorToBundle(final Cursor c)
	{
		final Bundle args = new Bundle();
		final String[] names = c.getColumnNames();
		if (!ArrayUtils.isEmpty(names))
		{
			for (int i = 0, len = names.length; i < len; i++)
			{
				args.putString(names[i], c.getString(i));
			}
		}
		return args;
	}
}

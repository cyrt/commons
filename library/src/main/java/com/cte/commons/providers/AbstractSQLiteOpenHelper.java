/**
 *
 */
package com.cte.commons.providers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentProvider;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.cte.commons.Console;

/**
 * Convenience class that adds {@link #createTable} facility to the {@link ContentProvider}.<br>
 * It expects any database to be associated with 2 String arrays: projection (rowId names) and constraints (rowId definitions).
 * 
 * @author cte
 * 
 * @since 30 aout 2011 15:32:58
 */
@SuppressWarnings("nls")
public abstract class AbstractSQLiteOpenHelper extends SQLiteOpenHelper
{
	/** Constant used with {@link AbstractSQLiteOpenHelper#createView} */
	public static final String	INNER_JOIN		= " INNER JOIN ";
	/**
	 * Constant used with {@link AbstractSQLiteOpenHelper#createView}<br>
	 * RIGHT and FULL OUTER JOINs are not currently supported.
	 */
	public static final String	LEFT_OUTER_JOIN	= " LEFT OUTER JOIN ";

	public static void openUniqueTransaction(final SQLiteDatabase db)
	{
		if (!db.inTransaction())
		{
			db.beginTransaction();
		}
	}

	public static void endUniqueTransaction(final SQLiteDatabase db)
	{
		if (db.inTransaction())
		{
			db.endTransaction();
		}
	}

	/**
	 * @param context
	 *            to use to open or create the database
	 * @param name
	 *            of the database file, or null for an in-memory database
	 * @param version
	 *            number of the database (starting at 1); if the database is older, onUpgrade will be used to upgrade the database; if the database is newer, onDowngrade will be used to downgrade the database
	 */
	public AbstractSQLiteOpenHelper(final Context context, final String name, final int version)
	{
		super(context, name, null, version);
	}

	@Override
	public void onCreate(final SQLiteDatabase db)
	{
		db.rawQuery("PRAGMA journal_mode=OFF;", null).close(); // possible values = DELETE | TRUNCATE | PERSIST | MEMORY | WAL | OFF
	}

	/**
	 * Trivial implementation that just calls {@link #onUpgrade(SQLiteDatabase, int, int)}.
	 */
	@Override
	public void onDowngrade(final SQLiteDatabase db, final int oldVersion, final int newVersion)
	{
		onUpgrade(db, oldVersion, newVersion);
	}

	/**
	 * Creates a table in given <b>database</b> with given parameters.
	 * 
	 * @param database
	 *            where the table will be created (corresponds to <i>database-name</i> in [1])
	 * @param tableName
	 *            (corresponds to <i>table-name</i> in [1])
	 * @param projection
	 *            {@code String[]} holding the rowId names (corresponds to <i>rowId-def</i> in [1])
	 * @param types
	 *            {@code String[]} holding the constraints for each rowId (corresponds to <i>table-constraint</i> in [1])
	 * @see [1] <a href="http://www.sqlite.org/lang_createtable.html">sqlite doc</a>
	 * @throws IndexOutOfBoundsException
	 *             if <b>projection</b> and <b>types</b> don't have the same length.
	 */
	protected final static void createTable(final SQLiteDatabase database, final String tableName, final String[] projection, final String[] types)
	{
		final StringBuilder statement = new StringBuilder(1024)
				.append("CREATE TABLE IF NOT EXISTS ")
				.append(tableName).append(" ( ");
		statement.append(BaseColumns._ID).append(' ').append(SQLConstants.SQL_INT_PK_AUTO).append(", ");
		if (BaseColumns._ID.equals(projection[0]))
		{
			for (int i = 1, len = projection.length; i < len; i++)
			{
				statement.append(projection[i]).append(' ').append(types[i - 1]).append(", ");
			}
		}
		else
		{
			for (int i = 0, len = projection.length; i < len; i++)
			{
				statement.append(projection[i]).append(' ').append(types[i]).append(", ");
			}
		}

		statement.setLength(statement.length() - 2); // get rid of last ", "
		statement.append(" );");
		Console.v("SQL=", statement);
		// try
		// {
		database.execSQL(statement.toString());
		// }
		// catch (final Exception e)
		// {
		// Console.ex(e);
		// }
	}

	/**
	 * Wrapper for {@link #createIndex(SQLiteDatabase, String, String)} that builds all given tables following templating defined by {@link #TablesDefinition}
	 * 
	 * @param db
	 * @param tables
	 *            array of tables to be created
	 * @since 14 oct. 2011 12:07:56
	 * @author cte
	 */
	public final static void createTables(final SQLiteDatabase db, final Enum<? extends TablesDefinition> tables[])
	{
		for (final Enum<? extends TablesDefinition> t : tables)
		{
			createTable(db, t);
		}
	}

	/**
	 * Convenience wrapper for {@link #createIndex(SQLiteDatabase, String, String)} that builds parameter following templating defined by {@link #TablesDefinition}
	 * 
	 * @param db
	 * @param table
	 * @since 14 oct. 2011 12:07:56
	 * @author cte
	 */
	private final static void createTable(final SQLiteDatabase db, final Enum<? extends TablesDefinition> table)
	{
		final TablesDefinition def = (TablesDefinition) table;
		if (def.isValid())
		{
			final String tableName = table.name();

			try
			{
				createTable(db, tableName, def.getColumns(), def.getConstraints());
				// Console.i("created", tableName);
			}
			catch (final Exception e)
			{
				throw new NoSuchFieldError("columns or constraints missing for table " + tableName);
			}
		}
	}

	/**
	 * Creates a database index on a given <b>database</b> with given parameters.<br>
	 * The index name is uniquely generated from the columns definition (and stays opaque)<br>
	 * The result is a {@code UNIQUE INDEX}.
	 * 
	 * @param database
	 *            where the index will be created (corresponds to <i>database-name</i> in [1])
	 * @param tableName
	 *            {@code String} (corresponds to <i>table-name</i> in [1])
	 * @param tableColumn
	 *            {@code String[]} the rowId names associated with the index (as a vararg)
	 */
	protected final static void createIndex(final SQLiteDatabase database, final Enum<?> table, final boolean unique, final String... tableColumns)
	{
		try
		{
			final String tableName = table.name();
			final String join = TextUtils.join(",", tableColumns);

			final String index = ("LOOKUP_KEY" + (tableName + join).hashCode()).replace('-', '_');
			final String statement = (unique ? "CREATE UNIQUE INDEX " : "CREATE INDEX ") + index
					+ " ON " + table.name()
					+ "(" + join + ");";
			database.execSQL(statement);

			Console.v("SQL created Index:", statement);
		}
		catch (final SQLiteException e)
		{
			Console.e("SQL Index ex=", e.toString());
		}
	}

	/**
	 * Destroys all tables.
	 * 
	 * @param db
	 * @param tables
	 *            array of tables to be dropped
	 * 
	 * @since 25 juil. 2011 11:58:58
	 */
	protected void dropAllTables(final SQLiteDatabase db, final Enum<? extends TablesDefinition> tables[])
	{
		Console.e(">>>>>>>>>>>>> DELETING TABLES <<<<<<<<<<<<");

		for (final Enum<? extends TablesDefinition> t : tables)
		{
			db.execSQL("DROP TABLE IF EXISTS " + t.name());
			Console.e("!>", t, "deleted...");
		}
	}

	/**
	 * Destroys all views.
	 * 
	 * @param db
	 * @param tables
	 *            array of tables to be dropped
	 * 
	 * @since 25 juil. 2011 11:58:58
	 */
	protected void dropAllViews(final SQLiteDatabase db, final Enum<? extends ViewsDefinition> tables[])
	{
		Console.e(">>>>>>>>>>>>> DELETING VIEWS <<<<<<<<<<<<");

		for (final Enum<? extends ViewsDefinition> t : tables)
		{
			db.execSQL("DROP VIEW IF EXISTS " + t.name());
			Console.e("!>", t, "deleted...");
		}
	}

	/**
	 * Wrapper for {@link #createIndex(SQLiteDatabase, String, String)} that builds all given tables following templating defined by {@link #TablesDefinition}
	 * 
	 * @param db
	 * @param views
	 *            array of tables to be created
	 * @since 14 oct. 2011 12:07:56
	 * @author cte
	 */
	public final static void createViews(final SQLiteDatabase db, final ViewsDefinition views[])
	{
		for (final ViewsDefinition t : views)
		{
			createView(db, t);
		}
	}

	/**
	 * Creates a View from two tables <b>left</b> and <b>right</b> by performing an {@value #INNER_JOIN} or a {@value #LEFT_OUTER_JOIN} on them.<br>
	 * The columns that allow to perform the join (i.e. the ON clause) are given by <b>joinOn</b>.<br>
	 * It is possible to use the last 2 parameters to filter out certain columns from the JOIN.
	 * Typically, {@link BaseColumns#_id} SHOULD be excluded from <b>right</b> for consistency.
	 * 
	 * @param db
	 *            database where the View will be created
	 * @param view
	 *            Enum value which {@link Enum#name() name} will be used to name the View and provides:
	 *            <ul>
	 *            <li>left table that will be used as the left source of the inner join/li>
	 *            <li>right table that will be used as the right source of the inner join/li>
	 *            <li>getJoinOnConditions(): columns that are present in <b>left</b> and <b>right</b> and that are used in the ON clause of the JOIN statement/li>
	 *            <li>getLeftExclusions(): columns in <b>left</b> that will NOT be included in the view/li>
	 *            <li>getRightExclusions(): columns in <b>right</b> that will NOT be included in the view</li>
	 *            </ul>
	 * @since 26 avr. 2012 15:41:23
	 * @author cte
	 */
	/* pkg */static void createView(final SQLiteDatabase db, final ViewsDefinition view)
	{
		final TablesDefinition left = view.getLeftTable();
		final TablesDefinition right = view.getRightTable();
		final String[] extraColumns = view.getExtraColumns();

		final String[] joinOn = view.getJoinOnConditions();
		final String[] columnsExcludedFromLeft = view.getLeftExclusions();
		final String[] columnsExcludedFromRight = view.getRightExclusions();

		final StringBuilder sb = new StringBuilder(1024);
		sb.append("CREATE VIEW ").append(view.name()).append(" AS SELECT ");

		final String leftTableName = left.name();
		final List<String> leftCols = new ArrayList<String>(Arrays.asList(left.getColumns()));

		final String rightTableName = right.name();
		final List<String> rightCols = new ArrayList<String>(Arrays.asList(right.getColumns()));

		if (null != columnsExcludedFromLeft)
		{
			leftCols.removeAll(Arrays.asList(columnsExcludedFromLeft));
		}
		if (null != columnsExcludedFromRight)
		{
			rightCols.removeAll(Arrays.asList(columnsExcludedFromRight));
		}

		rightCols.removeAll(Arrays.asList(joinOn));

		for (final String leftCol : leftCols)
		{
			sb.append(leftTableName).append('.').append(leftCol).append(", ");
		}

		for (final String rightCol : rightCols)
		{
			sb.append(rightTableName).append('.').append(rightCol).append(", ");
		}

		if (null != extraColumns)
		{
			for (final String col : extraColumns)
			{
				sb.append(col).append(", ");
			}
		}

		sb.setLength(sb.length() - 2); // drop excess ", "

		sb.append(" FROM ").append(leftTableName).append(view.getType()).append(rightTableName);
		sb.append(" ON ");

		if (view.isSimpleJoin())
		{
			for (final String joinCol : joinOn)
			{
				sb.append(leftTableName).append('.').append(joinCol)
						.append(" = ")
						.append(rightTableName).append('.').append(joinCol)
						.append(" AND ");
			}
		}
		else
		{
			for (int i = 0, len = joinOn.length; i < len; i += 3)
			{
				sb.append(leftTableName).append('.').append(joinOn[i])
						.append(joinOn[i + 1])
						.append(rightTableName).append('.').append(joinOn[i + 2])
						.append(" AND ");
			}
		}
		sb.setLength(sb.length() - 5); // drop excess " AND "

		final String where = view.getWhere();
		if (!TextUtils.isEmpty(where))
		{
			sb.append(" WHERE ").append(where);
		}

		final String statement = sb.toString();
		Console.v("SQL =", statement);
		db.execSQL(statement);
	}
}

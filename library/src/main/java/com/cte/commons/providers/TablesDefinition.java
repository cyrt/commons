/**
 *
 */
package com.cte.commons.providers;

import android.content.UriMatcher;

/**
 * Interface defining a template used for creating tables in database.<br>
 * Typically implemented on an {@code enum}, which members are the table names.<br>
 * <br>
 * If you plan to use a {@link UriMatcher}, you should use the ordinal value of your enum when doing your {@link UriMatcher#addURI},
 * and expose a method {@code valueOf(int)} to retrieve a table's name from the value returned by {@link UriMatcher#match}.
 * 
 */
public interface TablesDefinition
{
	/**
	 * @return SQL name of the table
	 */
	public String name();

	/**
	 * @return columns names
	 */
	public String[] getColumns();

	/**
	 * 
	 * @return constraints on the columns, in the same order as {@link #getColumns()}
	 */
	public String[] getConstraints();

	/**
	 * 
	 * @return {@code true} if the value is a valid table name
	 */
	public boolean isValid();

	/**
	 * @return ordinal value from underlying {@link Enum}
	 */
	public int ordinal();
}
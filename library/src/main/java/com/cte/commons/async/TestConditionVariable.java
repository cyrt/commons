package com.cte.commons.async;

import android.text.format.DateUtils;

import junit.framework.Assert;

/**
 * Created by cyril on 17/08/2016.
 */
public class TestConditionVariable<T> extends ResultConditionVariable<T> {
    public T test(int seconds) {
        if (block(10 * DateUtils.SECOND_IN_MILLIS)) {
            return popResult();
        } else {
            Assert.fail("request timeout");
        }
        return null;
    }
}

package com.cte.commons.async;

import android.os.ConditionVariable;

/**
 * Created by cyril on 17/08/2016.
 */
public class ResultConditionVariable<T> extends ConditionVariable {
    private T result;

	@Override
    public void open() {
        throw new UnsupportedOperationException();
    }

    public void open(T result) {
        this.result = result;
        super.open();
    }

    public T popResult() {
        T retval = result;
        result = null;
        return retval;
    }
}

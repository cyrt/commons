/**
 *
 */
package com.cte.commons.async;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

/**
 * {@link Handler} that operates on a background {@link HandlerThread}.<br>
 * Contrary to regular {@link Handler}s, it does not operate on the main thread. All instances of this class operate on the same thread.
 * 
 * @since 14 janv. 2013 11:21:28
 * @author cte
 */
public class BackgroundHandler extends Handler
{
	private static HandlerThread	sHandlerThread	= startHandlerThread();

	/**
	 * 
	 * @since 10 sept. 2012 16:20:51
	 * @author cte
	 */
	public BackgroundHandler()
	{
		super(ensureLooper());
	}

	/**
	 * @param callback
	 * @since 10 sept. 2012 16:20:51
	 * @author cte
	 */
	public BackgroundHandler(final Callback callback)
	{
		super(ensureLooper(), callback);
	}

	/**
	 * @return
	 * @since 14 janv. 2013 11:17:04
	 * @author cte
	 */
	private static HandlerThread startHandlerThread()
	{
		final HandlerThread thread = new HandlerThread(BackgroundHandler.class.getSimpleName());
		thread.start();
		return thread;
	}

	/**
	 * 
	 * @since 14 janv. 2013 11:23:13
	 * @author cte
	 * @return
	 */
	private static Looper ensureLooper()
	{
		if (!sHandlerThread.isAlive())
		{
			sHandlerThread.quit();
			sHandlerThread = startHandlerThread();
		}
		return sHandlerThread.getLooper();
	}

	/**
	 * @return the thread backing this class
	 * @since 14 janv. 2013 11:17:58
	 */
	public static HandlerThread getThread()
	{
		return sHandlerThread;
	}
}
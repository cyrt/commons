/**
 *
 */
package com.cte.commons.async;

import java.util.HashMap;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;

/**
 * {@link CursorLoader} with default throttle of {@value #DEFAULT_THROTTLE}ms.<br>
 * This is to limit the refresh rate of adapters that take cursors from loaders.<br>
 * Also allow to {@link #addMonitoredUri} to observe additional Uri that will trigger a reload.
 * 
 * @since 27 janv. 2012 11:43:52
 * @author cte
 */
public class ThrottledLoader extends CursorLoader
{
	public static interface CursorLoaderCallback extends LoaderManager.LoaderCallbacks<Cursor>
	{

	}

	/** Defines the minimum duration (in ms) between 2 background loadings */
	private static final int	DEFAULT_THROTTLE	= 500;

	private Handler				mHandler;

	private class LoaderObserver extends ContentObserver
	{
		/**
		 * @param handler
		 * @since 11 mai 2012 17:37:19
		 * @author cte
		 */
		public LoaderObserver()
		{
			super(getHandler());
		}

		@Override
		public void onChange(final boolean selfChange)
		{
			// Console.v("CursorLoader updated at", getUri());
			onContentChanged();
		};
	};

	private Handler getHandler()
	{
		if (null == this.mHandler)
		{
			this.mHandler = new SoloHandler();
		}

		return this.mHandler;
	}

	private final HashMap<Uri, LoaderObserver>	mUriSet	= new HashMap<Uri, LoaderObserver>();
	private final ContentResolver				mResolver;

	/**
	 * @param context
	 * @param uri
	 * @param projection
	 * @param selection
	 * @param selectionArgs
	 * @param sortOrder
	 * @since 27 janv. 2012 11:44:13
	 * @author cte
	 */
	public ThrottledLoader(final Context context, final Uri uri, final String[] projection, final String selection, final String[] selectionArgs, final String sortOrder)
	{
		super(context, uri, projection, selection, selectionArgs, sortOrder);
		setUpdateThrottle(DEFAULT_THROTTLE);
		this.mResolver = context.getContentResolver();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.content.CursorLoader#deliverResult(android.database.Cursor)
	 * 
	 * @since 10 août 2012 13:31:22
	 */
	@Override
	public void deliverResult(final Cursor cursor)
	{
		// make sure the cursor we return has the same notification Uri as us
		// this ensures correct behavior with provider who do not call setNotificationUri before returning from query()
		if (cursor != null)
		{
			cursor.setNotificationUri(this.mResolver, getUri());
		}
		super.deliverResult(cursor);
	}

	/**
	 * @param context
	 * @since 27 janv. 2012 11:44:13
	 * @author cte
	 */
	public ThrottledLoader(final Context context)
	{
		super(context);
		setUpdateThrottle(500);
		this.mResolver = context.getContentResolver();
	}

	/**
	 * Adds an {@link Uri} that will trigger a self-reload of the loader when a change if notified on it
	 * 
	 * @param uri
	 * @since 27 janv. 2012 15:50:11
	 * @author cte
	 */
	public void addMonitoredUri(final Uri uri)
	{
		this.mUriSet.put(uri, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.content.CursorLoader#onStartLoading()
	 * 
	 * @since 27 janv. 2012 15:47:32
	 */
	@Override
	protected void onStartLoading()
	{
		super.onStartLoading();

		// lazy instantiation+registration of observers
		for (final Uri uri : this.mUriSet.keySet())
		{
			if (null == this.mUriSet.get(uri))
			{
				// unfortunately, we cannot use the same observer for all Uris, we have to instantiate several
				final LoaderObserver observer = new LoaderObserver();
				this.mResolver.registerContentObserver(uri, true, observer);
				this.mUriSet.put(uri, observer);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.content.CursorLoader#onStopLoading()
	 * 
	 * @since 27 janv. 2012 15:47:36
	 */
	@Override
	protected void onStopLoading()
	{
		if (isStarted())
		{
			for (final Uri uri : this.mUriSet.keySet())
			{
				final LoaderObserver observer = this.mUriSet.get(uri);
				if (null != observer)
				{
					this.mResolver.unregisterContentObserver(observer);
					this.mUriSet.put(uri, null);
				}
			}
		}
		super.onStopLoading();
	}
}

/**
 * 
 */
package com.cte.commons.async;

import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * {@link Handler} that allow only one message to be enqueued for any given {@link Message#what}.<br>
 * Every time a new message is posted to this handler, all messages with the same {@link Message#what} that are already in the queue are removed.<br>
 * This class is especially designed for {@link ContentObserver}s, to limit the rate of refreshes they receive during batch operations.
 * @since 11 mai 2012 17:03:34
 * @author cte
 */
public class SoloHandler extends Handler
{
    /**
     * 
     * @since 10 sept. 2012 16:20:51
     * @author cte
     */
    public SoloHandler()
    {
        super();
    }

    /**
     * @param callback
     * @since 10 sept. 2012 16:20:51
     * @author cte
     */
    public SoloHandler(final Callback callback)
    {
        super(callback);
    }

    /**
     * @param looper
     * @param callback
     * @since 10 sept. 2012 16:20:51
     * @author cte
     */
    public SoloHandler(final Looper looper, final Callback callback)
    {
        super(looper, callback);
    }

    /**
     * @param looper
     * @since 10 sept. 2012 16:20:51
     * @author cte
     */
    public SoloHandler(final Looper looper)
    {
        super(looper);
    }

    /* (non-Javadoc)
     * @see android.os.Handler#sendMessageAtTime(android.os.Message, long)
     * @since 11 mai 2012 17:01:39
     */
    @Override
    public boolean sendMessageAtTime(final Message msg, final long uptimeMillis)
    {
        removeMessages(msg.what);
        return super.sendMessageAtTime(msg, uptimeMillis);
    }
}
/**
 *
 */
package com.cte.commons;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;

/**
 * 
 * @since 13 mars 2012 11:39:02
 * @author cte
 */
public class Version
{
	/* gone with the wind... */
	// /** {@code true} if running Android 2.0 ({@link VERSION_CODES#ECLAIR SDK5}) or later */
	// public static final boolean IS_ECLAIR = VERSION.SDK_INT >= VERSION_CODES.ECLAIR;
	// /** {@code true} if running Android 2.1 ({@link VERSION_CODES#ECLAIR_MR1 SDK7}) or later */
	// public static final boolean IS_ECLAIR2 = VERSION.SDK_INT >= VERSION_CODES.ECLAIR_MR1;

	/* current base API */
	// /** {@code true} if running Android 2.2 ({@link VERSION_CODES#FROYO SDK8}) or later */
	// public static final boolean IS_FROYO = VERSION.SDK_INT >= VERSION_CODES.FROYO;

	/** {@code true} if running Android 2.3 ({@link VERSION_CODES#GINGERBREAD SDK9}) or later */
	public static final boolean	IS_GINGERBREAD		= VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD;
	/** {@code true} if running Android 2.3.3 ({@link VERSION_CODES#GINGERBREAD_MR1 SDK10}) or later */
	public static final boolean	IS_GINGERBREAD_MR1	= VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD_MR1;
	/** {@code true} if running Android 3.0 ({@link VERSION_CODES#HONEYCOMB SDK11}) or later */
	public static final boolean	IS_HONEYCOMB		= VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB;
	/** {@code true} if running Android 3.1 ({@link VERSION_CODES#HONEYCOMB_MR1 SDK12}) or later */
	public static final boolean	IS_HONEYCOMB_MR1	= VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR1;
	/** {@code true} if running Android 3.2 ({@link VERSION_CODES#HONEYCOMB_MR2 SDK13}) or later */
	public static final boolean	IS_HONEYCOMB_MR2	= VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR2;
	/** {@code true} if running Android 4.0 ({@link VERSION_CODES#ICE_CREAM_SANDWICH SDK14}) or later */
	public static final boolean	IS_ICS				= VERSION.SDK_INT >= VERSION_CODES.ICE_CREAM_SANDWICH;
	/** {@code true} if running Android 4.0.3 ({@link VERSION_CODES#ICE_CREAM_SANDWICH_MR1 SDK15}) or later */
	public static final boolean	IS_ICS_MR1			= VERSION.SDK_INT >= VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
	/** {@code true} if running Android 4.1 ({@link VERSION_CODES#JELLY_BEAN SDK16}) or later */
	public static final boolean	IS_JELLY			= VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN;
	/** {@code true} if running Android 4.2 ({@link VERSION_CODES#JELLY_BEAN_MR1 SDK17}) or later */
	public static final boolean	IS_JELLY_MR1		= VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR1;
	/** {@code true} if running Android 4.3 ({@link VERSION_CODES#JELLY_BEAN_MR2 SDK18}) or later */
	public static final boolean	IS_JELLY_MR2		= VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR2;
	/** {@code true} if running Android 4.4 ({@link VERSION_CODES#KITKAT SDK19}) or later */
	public static final boolean	IS_KITKAT			= VERSION.SDK_INT >= VERSION_CODES.KITKAT;
	/** {@code true} if running Android 5 ({@link VERSION_CODES#LOLLIPOP SDK21}) or later */
	public static final boolean	IS_LOLLIPOP			= VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP;
}

package com.cte.commons;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;

public class App extends Application
{

    /** Application is a Singleton in the running process */
    protected static App sInstance;

    /**
     * Init the unique Application instance. These are the very first lines of
     * our code that will run.
     */
    public App()
    {
        sInstance = this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Application#onCreate()
     * 
     * @since 25 juil. 2012 10:20:08
     */
    @Override
    public void onCreate()
    {
        super.onCreate();
        Console.setDebugTag(getPackageName());
    }

    /**
     * @return the unique Application instance
     */
    public static App getContext()
    {
        return sInstance;
    }

    /**
     * @see Context#getResources()
     * @return the application resources
     * @since 21 juil. 2011 16:12:18
     */
    public static Resources getR()
    {
        return sInstance.getResources();
    }

    /**
     * @see Context#getContentResolver()
     * @return the application's ContentResolver
     * @since 21 juil. 2011 16:12:18
     */
    public static ContentResolver getResolver()
    {
        return sInstance.getContentResolver();
    }

    /**
     * @see Context#getString(int)
     * @param resId
     * @return String from resources
     * @since 21 juil. 2011 16:14:21
     */
    public static String getStr(final int resId)
    {
        return sInstance.getString(resId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContextWrapper#unregisterReceiver(android.content.
     * BroadcastReceiver)
     * 
     * @since 1 août 2012 11:10:10
     */
    @Override
    public void unregisterReceiver(final BroadcastReceiver receiver)
    {
        try
        {
            super.unregisterReceiver(receiver);
        }
        catch (final IllegalArgumentException ignored)
        {/* ignore exception "Receiver not registered" */
        }
    }

}

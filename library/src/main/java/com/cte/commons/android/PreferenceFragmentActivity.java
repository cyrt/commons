/**
 * 
 */
package com.cte.commons.android;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.text.TextUtils;
import android.view.MenuItem;

import com.cte.commons.Console;
import com.cte.commons.android.PreferenceActivityHelper.IPreferenceActivity;

import java.util.List;

/**
 * 
 * @since 17 nov. 2011 15:24:45
 * @author cte
 */
@TargetApi(11)
public class PreferenceFragmentActivity extends PreferenceActivity implements IPreferenceActivity
{

    /**
     * Populate the activity with the top-level headers.
     */
    @Override
    public void onBuildHeaders(final List<Header> target)
    {

        if (getIntent().getBooleanExtra(WITH_HEADERS, true))
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            loadHeadersFromResource(getIntent().getIntExtra(HEADERS, 0), target);
        }
        else
        {
            startWithFragment(PrefsFragment.class.getName(), getIntent().getExtras(), null, 0);
            finish();
        }
    }

    /**
     * This fragment shows the preferences specified in the arguments.
     */
    public static class PrefsFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);

            final String fragmentId = getArguments().getString(FRAGMENT);
            Console.v("showing preference screen", fragmentId);
            // Load the preferences from an XML resource

            final int xml = getResources().getIdentifier(fragmentId, "xml", getActivity().getPackageName());
            addPreferencesFromResource(xml);

        }

        /*
         * (non-Javadoc)
         * 
         * @see android.app.Fragment#onResume()
         * 
         * @since 24 juil. 2012 18:52:00
         */
        @Override
        public void onResume()
        {
            super.onResume();
            final ActionBar actionBar = getActivity().getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            final String title = getArguments().getString(TITLE);
            if (!TextUtils.isEmpty(title))
            {
                actionBar.setTitle(title);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        if (android.R.id.home == item.getItemId())
        {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Activity#onBackPressed()
     * 
     * @since 24 juil. 2012 17:48:19
     */
    @Override
    public void onBackPressed()
    {
        finish();
    }
}

/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cte.commons.android;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.cte.commons.Console;
import com.cte.commons.R;

/**
 * This {@link AppCompatActivity} implements the {@link DrawerLayout} pattern
 * from the Android support library.
 * <p/>
 * <p>
 * It supports one navigation drawer, which MUST be the second child of the {@link DrawerLayout}, and the action bar's Up affordance opens and closes the navigation drawer. The {@link ActionBarDrawerToggle} facilitates this behavior. Items within the drawer should fall into one
 * of two categories:
 * </p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic policies as list or tab navigation in that a view switch does not create navigation history. This pattern should only be used at the root activity of a task, leaving some form of Up navigation active for
 * activities further down the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an alternate parent for Up navigation. This allows a user to jump across an app's navigation hierarchy at will. The application should treat this as it treats Up navigation from a different task, replacing
 * the current task stack using TaskStackBuilder or similar. This is the only form of navigation drawer that should be used outside of the root activity of a task.</li>
 * </ul>
 * <p/>
 * <p>
 * Left side drawers should be used for navigation, not actions. This follows the pattern established by the Action Bar that navigation should be to the left and actions to the right. An action is any operation performed on the current contents of the window, for example enabling
 * or disabling a data overlay on top of the current content. Navigation is any operation that replaces the current display of the device for something else, e.g a new {@link Activity}.
 * </p>
 */
public abstract class DrawerActivity<D extends View> extends AppCompatActivity implements DrawerListener
{
	private DrawerLayout			mDrawerLayout;
	private ActionBarDrawerToggle	mDrawerToggle;

	private CharSequence			mDrawerTitle;
	private CharSequence			mContentTitle;
	private D						mDrawerView;

	@Override
	protected void onCreate(final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.mContentTitle = this.mDrawerTitle = getTitle();
		this.mDrawerLayout = onCreateView();
		super.setContentView(this.mDrawerLayout);

		this.mDrawerView = (D) this.mDrawerLayout.getChildAt(1);

		// set a custom shadow that overlays the main content when the drawer opens
		this.mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		this.mDrawerToggle = new ActionBarDrawerToggle(
				this, /* host Activity */
				this.mDrawerLayout, /* DrawerLayout object */
				R.drawable.drawer_ab_carret, /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description for accessibility */
				R.string.drawer_close /* "close drawer" description for accessibility */
				);
		this.mDrawerLayout.setDrawerListener(this);

		onMenuDrawerCreated(this.mDrawerView);
	}

	@Override
	public void onDrawerSlide(final View drawerView, final float slideOffset)
	{
		this.mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
	}

	@Override
	public void onDrawerOpened(final View drawerView)
	{
		this.mDrawerToggle.onDrawerOpened(drawerView);
		Console.i("drawer opened");
		getSupportActionBar().setTitle(DrawerActivity.this.mDrawerTitle);
		supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	}

	@Override
	public void onDrawerClosed(final View drawerView)
	{
		this.mDrawerToggle.onDrawerClosed(drawerView);
		Console.i("drawer closed");
		getSupportActionBar().setTitle(DrawerActivity.this.mContentTitle);
		supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	}

	@Override
	public void onDrawerStateChanged(final int newState)
	{
		this.mDrawerToggle.onDrawerStateChanged(newState);
	}

	/**
	 * Inflates and returns the activity's layout.
	 * The root of this Layout MUST be a {@link DrawerLayout}.
	 * The first child of the {@link DrawerLayout} is the actual activity's content.
	 * Additional children are drawers.
	 * 
	 * @return the activity's content, which root is a {@link DrawerLayout}.
	 */
	public abstract DrawerLayout onCreateView();

	/**
	 * Called once the view hierarchy has been setup, for the activity to initialize the drawer's content.
	 * 
	 * @param drawer
	 *            the drawer's view
	 */
	public abstract void onMenuDrawerCreated(D drawer);

	@Override
	public void setContentView(final int layoutResID)
	{
		throw new UnsupportedOperationException("You cannot invoke this method with this DrawerActivity");
	}

	@Override
	public void setContentView(final View view)
	{
		throw new UnsupportedOperationException("You cannot invoke this method with this DrawerActivity");
	}

	@Override
	public void setContentView(final View view, final LayoutParams params)
	{
		throw new UnsupportedOperationException("You cannot invoke this method with this DrawerActivity");
	}

	@Override
	public void setTitle(final CharSequence title)
	{
		this.mContentTitle = title;
		if (!this.mDrawerLayout.isDrawerOpen(this.mDrawerView))
		{
			getSupportActionBar().setTitle(title);
		}
	}

	public void setDrawerTitle(final CharSequence drawerTitle)
	{
		this.mDrawerTitle = drawerTitle;
		if (this.mDrawerLayout.isDrawerOpen(this.mDrawerView))
		{
			getSupportActionBar().setTitle(drawerTitle);
		}
	}

	/**
	 * 
	 * @return the view of the drawer
	 */
	public D getDrawerView()
	{
		return this.mDrawerView;
	}

	/**
	 * 
	 * @return <code>true</code> if the drawer is currently opened.
	 */
	public boolean isDrawerOpen()
	{
		return this.mDrawerLayout.isDrawerOpen(this.mDrawerView);
	}

	/**
	 * {@link DrawerLayout#openDrawer(int) Open} or {@link DrawerLayout#closeDrawers() close} the drawer.
	 * 
	 * @param open
	 */
	public void setDrawerOpen(final boolean open)
	{
		if (open && !this.mDrawerLayout.isDrawerOpen(this.mDrawerView))
		{
			this.mDrawerLayout.openDrawer(this.mDrawerView);
		}
		else if (!open)
		{
			this.mDrawerLayout.closeDrawers();
		}
	}

	/**
	 * {@link DrawerLayout#openDrawer(int) Open} or {@link DrawerLayout#closeDrawers() close} the drawer depending on its current state.
	 * 
	 */
	public void toggleDrawer()
	{
		if (this.mDrawerLayout.isDrawerOpen(this.mDrawerView))
		{
			this.mDrawerLayout.closeDrawers();
		}
		else
		{
			this.mDrawerLayout.openDrawer(this.mDrawerView);
		}
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(final Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		this.mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(final Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggle
		this.mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (this.mDrawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

}
/*
 * Copyright (C) 2008 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cte.commons.android;

import android.app.IntentService;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import com.cte.commons.Console;

import java.util.concurrent.atomic.AtomicInteger;



/**
 * WorkerService is a base class for {@link Service}s that handle asynchronous
 * requests (expressed as {@link Intent}s) on demand. Clients send requests
 * through {@link Context#startService(Intent)} calls; the
 * service is started as needed, handles each Intent in turn using a worker
 * thread, and remains idle waiting for more work.
 * 
 * <p>
 * This "work queue processor" pattern is commonly used to offload tasks from an application's main thread. The WorkerService class exists to simplify this pattern and take care of the mechanics. To use it, extend WorkerService and implement {@link #onHandleIntent(Intent)}. WorkerService will receive the Intents, launch a worker thread if needed, and process the Intents in the thread.
 * 
 * <p>
 * All requests are handled on a single worker thread -- they may take as long as necessary (and will not block the application's main loop), but only one request will be processed at a time.
 * <p>
 * Compared to an {@link IntentService}, it will not stop itself after processing an Intent, but will stay idle, waiting for new Intents. This ensures that asynchronous tasks can complete while the service is alive (whereas an {@link IntentService} would self stop before the task is ended).<br>
 * For this reason it is not recommended to call {@link #stopService} blindly, since some asynchronous task may be active.
 * <br>Instead, consider using {@link #requestStopSelf()} to trigger a {@link #stopSelf} only after all the workers have ended their processing
 * (which requires monitoring your asynchronous tasks to decide when is the proper time to call {@link #requestStopSelf()}, see {@link #dispatchWork(Runnable)}).
 * 
 * <div class="special reference">
 * <h3>Developer Guides</h3>
 * <p>
 * For a detailed discussion about how to create services, read the <a href="{@docRoot}guide/topics/fundamentals/services.html">Services</a> developer guide.
 * </p>
 * </div>
 * 
 * @see android.os.AsyncTask
 */
public abstract class WorkerService extends Service implements AsyncTaskListener
{
    /**
     * Special action that you can use to tell the service it is no longer needed alive
     * and that it can stop as soon as its workers have completed their tasks.
     */
    public static String            ACTION_STOP_SERVICE = WorkerService.class.getName() + ".STOP_SERVICE";

    private static final int        MSG_ON_START        = 0;
    private static final int        MSG_WORK_REQUEST    = 1;
    private static final int        MSG_STOP_SELF       = 2;

    /** true after a {@link #MSG_STOP_SELF} has been issued. */
    private boolean                 mIsFinishing        = false;

    private volatile Looper         mServiceLooper;
    private volatile ServiceHandler mServiceHandler;

    private final String            mName;

    private boolean                 mRedelivery = true;


    private final class ServiceHandler extends Handler implements AsyncTaskListener
    {
        private final AtomicInteger mWorkerCount = new AtomicInteger(0);

        public ServiceHandler(final Looper looper)
        {
            super(looper);
        }

        /* (non-Javadoc)
         * @see android.os.Handler#sendMessageAtTime(android.os.Message, long)
         * @since 29 mars 2012 11:09:27
         */
        @Override
        public boolean sendMessageAtTime(final Message msg, final long uptimeMillis)
        {
            this.mWorkerCount.incrementAndGet();
            return super.sendMessageAtTime(msg, uptimeMillis);
        }


        @Override
        public void handleMessage(final Message msg)
        {
            final int count = this.mWorkerCount.getAndDecrement();
            Console.v("Starting task at position", count, ':', msg);
            switch (msg.what)
            {
                case MSG_ON_START:
                    onHandleIntent((Intent) msg.obj);
                    break;
                case MSG_WORK_REQUEST:
                    ((Runnable) msg.obj).run();
                    break;
                case MSG_STOP_SELF:
                    final boolean hasNoActiveWorker = count <= 1; // we're the only one in the queue
                    if (hasNoActiveWorker)
                    {
                        stopSelf(msg.arg1);
                    }
                    break;
            }
            Console.v("Completed task at position", count);
        }

        /* (non-Javadoc)
         * @see com.sncr.it5.framework.service.WorkerService.AsyncTaskListener#onAsyncTaskStarted()
         * @since 29 mars 2012 11:32:38
         */
        @Override
        public void onAsyncTaskStarted()
        {
            this.mWorkerCount.incrementAndGet();
        }

        /* (non-Javadoc)
         * @see com.sncr.it5.framework.service.WorkerService.AsyncTaskListener#onAsyncTaskEnded()
         * @since 29 mars 2012 11:32:38
         */
        @Override
        public void onAsyncTaskEnded()
        {
            this.mWorkerCount.decrementAndGet();
        }
    }

    /**
     * Creates an WorkerService. Invoked by your subclass's constructor.
     * Consider also {@link #setIntentRedelivery}.
     * @param name Used to name the worker thread, important only for debugging.
     */
    public WorkerService(final String name)
    {
        super();
        this.mName = name;
    }

    /**
     * Sets intent redelivery preferences. Usually called from the constructor
     * with your preferred semantics.
     * 
     * <p>
     * If enabled is true (the default), {@link #onStartCommand(Intent, int, int)} will return {@link Service#START_REDELIVER_INTENT}, so if this process dies before {@link #onHandleIntent(Intent)} returns, the process will be restarted and the intent redelivered. If multiple Intents have been sent, only the most recent one is guaranteed to be redelivered.
     * 
     * <p>
     * If enabled is false, {@link #onStartCommand(Intent, int, int)} will return {@link Service#START_NOT_STICKY}, and if the process dies, the Intent dies along with it.
     */
    public void setIntentRedelivery(final boolean enabled)
    {
        this.mRedelivery = enabled;
    }

    @Override
    public void onCreate()
    {
        // It would be nice to have an option to hold a partial wakelock
        // during processing, and to have a static startService(Context, Intent)
        // method that would launch the service & hand off a wakelock.
        super.onCreate();
        Console.v("Starting service", this.mName);

        final HandlerThread thread = new HandlerThread("WorkerService[" + this.mName + "]", android.os.Process.THREAD_PRIORITY_LESS_FAVORABLE);
        thread.start();

        this.mServiceLooper = thread.getLooper();
        this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
    }

    @Override
    public void onStart(final Intent intent, final int startId)
    {
        Console.print(intent);
        final String action = intent.getAction();

        if (ACTION_STOP_SERVICE.equals(action))
        {
            requestStopSelf();
            return;
        }

        final Message msg = Message.obtain(this.mServiceHandler, MSG_ON_START, startId, 0, intent);

        Console.v("New task", action,"is queued at position", 1 + this.mServiceHandler.mWorkerCount.get());
        this.mServiceHandler.sendMessage(msg);

    }

    /**
     * Dispatch a task to the {@link ServiceHandler}.<br>
     * The task will be enqueued in the common queue of the service to be ran as soon as possible.<br>
     * Since the {@link ServiceHandler} is single-threaded, only one task runs at a time and the other pending tasks are delayed.<br>
     * If {@link #requestStopSelf()} is called, the service will delay its stop until all pending tasks are done.
     * @param task
     * @since 30 mars 2012 13:19:14
     * @author cte
     */
    public void dispatchWork(final Runnable task)
    {
        final Message msg = Message.obtain(this.mServiceHandler, MSG_WORK_REQUEST, -1, 0, task);
        this.mServiceHandler.sendMessage(msg);
    }

    /**
     * Same as {@link #dispatchWork(Runnable)}, but delayed by <b>delayMillis</b> ms.
     * @param task
     * @param delayMillis
     * @since 20 avr. 2012 17:20:46
     * @author cte
     */
    public void dispatchWorkDelayed(final Runnable task, final long delayMillis)
    {
        final Message msg = Message.obtain(this.mServiceHandler, MSG_WORK_REQUEST, -1, 0, task);
        this.mServiceHandler.sendMessageDelayed(msg, delayMillis);
    }

    /**
     * @since 29 mars 2012 11:13:56
     */
    public void requestStopSelf()
    {
        this.mIsFinishing  = true;
        final Message msg = Message.obtain(this.mServiceHandler, MSG_STOP_SELF, -1 /* stopSelf() does stopSelf(-1)*/, 0);
        this.mServiceHandler.sendMessage(msg);
    }

    /**
     * @return {@code true} if a request for finishing the service has been issued
     * @since 20 avr. 2012 17:28:28
     */
    public boolean isFinishing()
    {
        return this.mIsFinishing;
    }
    /**
     * 
     * @return the {@link Handler} that backs our service.
     * @since 29 mars 2012 14:36:47
     * @author cte
     */
    public Handler getHandler()
    {
        return this.mServiceHandler;
    }

    /**
     * You should not override this method for your WorkerService. Instead,
     * override {@link #onHandleIntent}, which the system calls when the WorkerService
     * receives a start request.
     * 
     * @see android.app.Service#onStartCommand
     */
    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId)
    {
        onStart(intent, startId);
        return this.mRedelivery ? START_REDELIVER_INTENT : START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        // in case an asynchronous worker has been forgotten somewhere, we give it the opportunity to self-cancel if it checks isFinishing()
        this.mIsFinishing = true;
        this.mServiceLooper.quit();
    }

    /**
     * Unless you provide binding for your service, you don't need to implement this
     * method, because the default implementation returns null.
     * 
     * @see android.app.Service#onBind
     */
    @Override
    public IBinder onBind(final Intent intent)
    {
        return null;
    }

    /**
     * This method is invoked on the worker thread with a request to process.
     * Only one Intent is processed at a time, but the processing happens on a
     * worker thread that runs independently from other application logic.
     * So, if this code takes a long time, it will hold up other requests to
     * the same WorkerService, but it will not hold up anything else.
     * When all requests have been handled, the WorkerService stops itself,
     * so you should not call {@link #stopSelf}.
     * <p>You can use {@link AsyncTask} or other asynchronous mechanism to
     * return faster and avoid locking other pending intents, however in such
     * case you should call {@link #onAsyncTaskStarted()} and {@link #onAsyncTaskEnded()}
     * to avoid the service self-killing while the processing is still going on.
     * @param intent The value passed to {@link android.content.Context#startService(Intent)}.
     */
    protected abstract void onHandleIntent(Intent intent);

    /**
     * Tells the service an asynchronous task is starting, so that it shouldn't {@link #stopSelf} for now.
     * When overriding this method, don't forget to call {@code super}.
     */
    @Override
    public void onAsyncTaskStarted()
    {
        this.mServiceHandler.onAsyncTaskStarted();
    }


    /**
     * Tells the service an asynchronous task has ended, and that it is therefore safe to {@link #stopSelf} regarding this task.
     * When overriding this method, don't forget to call {@code super}.
     */
    @Override
    public void onAsyncTaskEnded()
    {
        this.mServiceHandler.onAsyncTaskEnded();
    }

    /* (non-Javadoc)
     * @see android.content.ContextWrapper#unregisterReceiver(android.content.BroadcastReceiver)
     * @since 30 mars 2012 17:35:19
     */
    @Override
    public void unregisterReceiver(final BroadcastReceiver receiver)
    {
        try {
            super.unregisterReceiver(receiver);
        } catch (final Exception ignored) {/*ignored*/}
    }
}

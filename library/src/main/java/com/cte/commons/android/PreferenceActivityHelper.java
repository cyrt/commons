/**
 * 
 */
package com.cte.commons.android;

import android.content.Context;
import android.content.Intent;

import com.cte.commons.Version;

/**
 * 
 * @since 24 juil. 2012 15:12:19
 * @author cte
 */
public class PreferenceActivityHelper
{
    interface IPreferenceActivity
    {
        String WITH_HEADERS = "with_headers";
        String HEADERS = "headers";
        String FRAGMENT = "show_fragment";
        String TITLE    = "show_fragment_title";
    }

    public static final Class<? extends IPreferenceActivity> PREFERENCES_CLASS = Version.IS_HONEYCOMB ? PreferenceFragmentActivity.class
                                                                                       : PreferencePlainActivity.class;
    
    
    /**
     * 
     * @param context
     * @param title default title for the preference activity
     * @param modernPrefResId resource id of a preference-headers (modern style)
     * @param legacyPrefResId resource id of a PreferenceScreen that mimics preference-headers 
     * @return an intent to display "fragmented"-style preferences
     * @since 8 août 2012 13:25:37
     * @author cte
     */
    public static Intent makePreferenceIntent(Context context, CharSequence title, int modernPrefResId, int legacyPrefResId)
    {
        Intent intent = new Intent(context, PREFERENCES_CLASS);
        intent.putExtra(IPreferenceActivity.HEADERS, Version.IS_HONEYCOMB ? modernPrefResId : legacyPrefResId);
        intent.putExtra(IPreferenceActivity.TITLE, title);
        intent.putExtra(IPreferenceActivity.WITH_HEADERS, true);
        return intent;
    }
    
    /**
     * 
     * @param context
     * @param title
     * @param preferencescreen
     * @return an Intent to display only one preference screen (without the headers)
     * @since 8 août 2012 13:34:26
     * @author cte
     */
    public static Intent makePreferenceIntent(Context context, CharSequence title, int preferencescreen)
    {
        Intent intent = new Intent(context, PREFERENCES_CLASS);
        String fragmentName = context.getResources().getResourceEntryName(preferencescreen);
        intent.putExtra(IPreferenceActivity.FRAGMENT, fragmentName);
        intent.putExtra(IPreferenceActivity.TITLE, title);
        intent.putExtra(IPreferenceActivity.WITH_HEADERS, false);
        return intent;
    }
}

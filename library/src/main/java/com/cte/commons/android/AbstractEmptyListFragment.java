package com.cte.commons.android;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cte.commons.R;

public abstract class AbstractEmptyListFragment extends AbstractListFragment
{

    private CharSequence mEmptyText;
    private TextView mEmptyView;
    protected ListView mList;
    private boolean mListShown = false;
    private View mListContainer;
    private View mProgressContainer;

    public AbstractEmptyListFragment()
    {
        super();
    }

    /**
     * The default content for a ListFragment has a TextView that can be shown
     * when the list is empty. If you would like to have it shown, call this
     * method to supply the text it should use.
     */
    @Override
    public void setEmptyText(final CharSequence text)
    {
        this.mEmptyView.setText(text);
        if (this.mEmptyText == null)
        {
            this.mList.setEmptyView(this.mEmptyView);
        }
        this.mEmptyText = text;
    }

    protected abstract int getLayout();

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        final View root = inflater.inflate(getLayout(), container, false);
        this.mEmptyView = (TextView) root.findViewById(android.R.id.empty);
        this.mListContainer = root.findViewById(R.id.listContainer);
        this.mList = (ListView) root.findViewById(android.R.id.list);
        this.mProgressContainer = root.findViewById(R.id.progressContainer);
        if (this.mEmptyView != null)
        {
            this.mList.setEmptyView(this.mEmptyView);
        }
        if (this.mProgressContainer != null)
        {
            setListShown(false, false);
        }

        return root;
    }

    /**
     * Control whether the list is being displayed. You can make it not
     * displayed if you are waiting for the initial data to show in it. During
     * this time an indeterminant progress indicator will be shown instead.
     * 
     * <p>
     * Applications do not normally need to use this themselves. The default behavior of ListFragment is to start with the list not being shown, only showing it once an adapter is given with {@link #setListAdapter(ListAdapter)}. If the list at that point had not been shown, when it does get shown it will be do without the user ever seeing the hidden state.
     * 
     * @param shown
     *        If true, the list view is shown; if false, the progress
     *        indicator. The initial value is true.
     */
    @Override
    public void setListShown(final boolean shown)
    {
        setListShown(shown, true);
    }

    /**
     * Like {@link #setListShown(boolean)}, but no animation is used when
     * transitioning from the previous state.
     */
    @Override
    public void setListShownNoAnimation(final boolean shown)
    {
        setListShown(shown, false);
    }

    /**
     * Control whether the list is being displayed. You can make it not
     * displayed if you are waiting for the initial data to show in it. During
     * this time an indeterminant progress indicator will be shown instead.
     * 
     * @param shown
     *        If true, the list view is shown; if false, the progress
     *        indicator. The initial value is true.
     * @param animate
     *        If true, an animation will be used to transition to the new
     *        state.
     */
    private void setListShown(final boolean shown, final boolean animate)
    {
        if (this.mListContainer == null)
        {
            throw new IllegalStateException("You must provide a list container with id R.id.listContainer");
        }
        if (this.mProgressContainer == null)
        {
            throw new IllegalStateException("You must provide a progress container with id R.id.progressContainer");
        }

        if (this.mListShown == shown)
        {
            return;
        }
        this.mListShown = shown;
        if (shown)
        {
            if (animate)
            {
                this.mProgressContainer.startAnimation(AnimationUtils
                        .loadAnimation(getActivity(), android.R.anim.fade_out));
                this.mListContainer.startAnimation(AnimationUtils
                        .loadAnimation(getActivity(), android.R.anim.fade_in));
            }
            else
            {
                this.mProgressContainer.clearAnimation();
                this.mListContainer.clearAnimation();
            }
            this.mProgressContainer.setVisibility(View.GONE);
            this.mListContainer.setVisibility(View.VISIBLE);
        }
        else
        {
            if (animate)
            {
                this.mProgressContainer.startAnimation(AnimationUtils
                        .loadAnimation(getActivity(), android.R.anim.fade_in));
                this.mListContainer.startAnimation(AnimationUtils
                        .loadAnimation(getActivity(), android.R.anim.fade_out));
            }
            else
            {
                this.mProgressContainer.clearAnimation();
                this.mListContainer.clearAnimation();
            }
            this.mProgressContainer.setVisibility(View.VISIBLE);
            this.mListContainer.setVisibility(View.GONE);
        }
    }

}
/**
 * 
 */
package com.cte.commons.android;

/**
 * Interface used to notify its listener that an asynchronous task is running or has finished.
 *
 * @since 29 mars 2012 11:30:01
 * @author cte
 */
public interface AsyncTaskListener
{
    /**
     * Tells the listener a new task has started.
     * 
     * @since 29 mars 2012 11:31:06
     * @author cte
     */
    public void onAsyncTaskStarted();

    /**
     * Tells the listener a new task has completed.
     * 
     * @since 29 mars 2012 11:31:06
     * @author cte
     */
    public void onAsyncTaskEnded();
}
/**
 *
 */
package com.cte.commons.android;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.MenuItem;

import com.cte.commons.Console;
import com.cte.commons.android.PreferenceActivityHelper.IPreferenceActivity;

/**
 * Custom {@link PreferenceActivity} that mimics the behavior of fragment-based
 * preferences (SDK 14+).<br>
 * Also includes an ActionBar
 * 
 * @author cte
 */
public class PreferencePlainActivity extends PreferenceActivity implements OnPreferenceClickListener,
		IPreferenceActivity
{

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(final Bundle savedInstanceState)
	{
		// note: call mActionBarHelper.onCreate before super.onCreate (which
		// adds content and prevent further requestWindowFeature, that we need)
		super.onCreate(savedInstanceState);

		if (getIntent().getBooleanExtra(WITH_HEADERS, true))
		{
			addPreferencesFromResource(getIntent().getIntExtra(HEADERS, 0));

			final PreferenceScreen preferenceScreen = getPreferenceScreen();
			for (int p = preferenceScreen.getPreferenceCount() - 1; p >= 0; p--)
			{
				preferenceScreen.getPreference(p).setOnPreferenceClickListener(this);
			}
		}
		else
		{
			final String screen = getIntent().getStringExtra(FRAGMENT);
			// Load the preferences from an XML resource
			final int xml = getResources().getIdentifier(screen, "xml", getPackageName());
			addPreferencesFromResource(xml);
			final String title = getIntent().getStringExtra(TITLE);
			setTitle(title);
		}

		// final ActionBar actionBar = getSupportActionBar();
		// actionBar.setDisplayHomeAsUpEnabled(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.preference.Preference.OnPreferenceClickListener#onPreferenceClick
	 * (android.preference.Preference)
	 * 
	 * @since 17 nov. 2011 17:58:26
	 */
	@Override
	public boolean onPreferenceClick(final Preference preference)
	{
		Console.v("showing preference screen", preference.getKey());

		final Intent intent = getIntent()
				.putExtra(FRAGMENT, preference.getKey())
				.putExtra(TITLE, preference.getTitle());
		startActivity(intent);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.actionbarsherlock.app.SherlockPreferenceActivity#onOptionsItemSelected
	 * (com.actionbarsherlock.view.MenuItem)
	 * 
	 * @since 24 juil. 2012 14:21:48
	 */
	@Override
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		if (android.R.id.home == item.getItemId())
		{
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}

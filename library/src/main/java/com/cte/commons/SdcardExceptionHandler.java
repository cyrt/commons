package com.cte.commons;

import android.Manifest.permission;
import android.os.Environment;
import com.cte.commons.utils.PackageUtils;

import java.io.File;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class SdcardExceptionHandler implements UncaughtExceptionHandler
{

	private static UncaughtExceptionHandler	defaultUncaughtExceptionHandler;

	public static void install()
	{
		new SdcardExceptionHandler().init();
	}

	public SdcardExceptionHandler init()
	{
		if (PackageUtils.checkPermission(permission.WRITE_EXTERNAL_STORAGE))
		{
			defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
			Thread.setDefaultUncaughtExceptionHandler(this);
		}
		else
		{
			Console.e("Can't install SdcardExceptionHandler: missing permission <" + permission.WRITE_EXTERNAL_STORAGE + ">");
		}
		return this;
	}

	public SdcardExceptionHandler()
	{}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
	 * 
	 * @since 14 févr. 2013 10:43:38
	 */
	@Override
	public void uncaughtException(final Thread thread, final Throwable ex)
	{
		Console.i(">>> begin exception dump");
		Console.ex(ex);
		Console.i("<<<   end exception dump");
		recordException(thread, ex);
		handleException(thread, ex);
	}

	public void recordException(final Thread thread, final Throwable ex) {
		try
		{
			final File folder = getTraceFolder();
			if (folder.exists())
			{
				final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss_z", Locale.US);
				format.setTimeZone(TimeZone.getTimeZone("UTC"));
				final File file = new File(folder, "log_" + format.format(new Date()) + ".txt");
				Console.i("dump to", file.getPath());
				final PrintWriter writer = new PrintWriter(file);
				writer.append("On ").append(thread.getName()).append("\n-----------------------\n\n");
				ex.printStackTrace(writer);
				Throwable cause = ex.getCause();
				{
					while (null != cause)
					{
						writer.append("\n-----Cause detail:-----\n");
						cause.printStackTrace(writer);
						cause = cause.getCause();
					}
				}
				writer.close();
			}
			else
			{
				Console.e("Can't record exception to SD Card. Have you set permission?");
			}
		}
		catch (final Throwable t)
		{
			Console.e("failed recording uncaught exception");
			Console.ex(t);
		}
	}

	protected File getTraceFolder()
	{
		final File folder = new File(Environment.getExternalStorageDirectory(), App.getContext().getPackageName());
		folder.mkdirs();
		return folder;
	}

	protected void handleException(final Thread thread, final Throwable ex)
	{
		defaultUncaughtExceptionHandler.uncaughtException(thread, ex);
	}
}

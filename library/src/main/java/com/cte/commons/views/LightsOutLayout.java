/**
 * 
 */
package com.cte.commons.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.widget.FrameLayout;

import com.cte.commons.Version;

/**
 * 
 * @since 11 févr. 2013 12:51:14
 * @author cte
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LightsOutLayout extends FrameLayout implements OnSystemUiVisibilityChangeListener, Runnable
{

	/**
	 * @param context
	 * @since 11 févr. 2013 12:51:14
	 * @author cte
	 */
	public LightsOutLayout(final Context context)
	{
		super(context);
	}

	/**
	 * @param context
	 * @param attrs
	 * @since 11 févr. 2013 12:51:14
	 * @author cte
	 */
	public LightsOutLayout(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 * @since 11 févr. 2013 12:51:14
	 * @author cte
	 */
	public LightsOutLayout(final Context context, final AttributeSet attrs, final int defStyle)
	{
		super(context, attrs, defStyle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onAttachedToWindow()
	 * 
	 * @since 11 févr. 2013 12:51:51
	 */
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		if (Version.IS_HONEYCOMB)
		{
			setOnSystemUiVisibilityChangeListener(this);
			post(this);
		}
	}

	@Override
	public void run()
	{
		setSystemUiVisibility(SYSTEM_UI_FLAG_VISIBLE | SYSTEM_UI_FLAG_LOW_PROFILE);
	}

	@Override
	public void onSystemUiVisibilityChange(final int visibility)
	{
		if (visibility == VISIBLE)
		{
			postDelayed(this, 2000);
		}
	}

}

package com.cte.commons.views;

import com.cte.commons.R;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ToggleButton;


/**
 * The Static Text Toggle Button UI component is like a Static Text Button
 * widget except that after it is pressed it remains active until it is pressed
 * again.<br>
 * From xml, the following attributes are supported:<ul>
 * <li>{@code android:text} : defines texts for {@link #setTextOn(CharSequence) "on"} and {@link #setTextOff(CharSequence) "off"}</li>
 * <li>{@code android:textOn} : defines text for {@link #setTextOn(CharSequence) "on"} only. Only used if {@code android:text} is missing.</li>
 * <li>{@code android:textOff} : defines text for {@link #setTextOff(CharSequence) "off"} only. Only used if {@code android:text} is missing.</li>
 * </ul>
 * Note that if {@code android:textOff} is missing, 
 */
public class StaticTextToggleButton extends ToggleButton {

	/**
	 * Creates a StaticTextToggleButton widget	
	 * @param context
	 */
	public StaticTextToggleButton(Context context) {
		super(context);
		init();
	}

	/**
	 * Creates an StaticTextToggleButton Widget with a set of attributes.
	 * @param context
	 * @param attrs
	 */
	public StaticTextToggleButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();

		// handle android:text="@string/label"
		int res = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "text", 0);
        if (res != 0)
        {
            setText(res);
        }
        else {
         // handle android:text="label"
            String text = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "text");
            if (!TextUtils.isEmpty(text))
            {
                setText(text);
            }
        }
	}
	
	/**
	 * Initialize the class.
	 */
	protected void init(){
		this.setBackgroundResource(R.drawable.toggle_button);
	}
	
	@Override
	public void setBackgroundResource(int resid) {
		super.setBackgroundResource(resid);
	}
	
	@Override
	public void setBackgroundDrawable(Drawable d) {
		super.setBackgroundDrawable(d);
	}
	

	/**
	 * Set the text of the {@link StaticTextToggleButton}.
	 * @param text Button Text
	 */
    public void setText(CharSequence text, BufferType type) {
		super.setText(text, type);
		setTextOn(text);
		setTextOff(text);				
	}

	@Override
	public void setEnabled(boolean enabled) {
		setTextColor(enabled ? Color.BLACK : Color.GRAY);
		super.setEnabled(enabled);
	}
	
}

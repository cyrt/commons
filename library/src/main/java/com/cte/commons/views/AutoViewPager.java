package com.cte.commons.views;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.cte.commons.R;

public class AutoViewPager extends ViewPager
{
	class AutoPagerAdapter extends PagerAdapter
	{

		@Override
		public Object instantiateItem(final View collection, final int position)
		{
			return getChildAt(position);
		}

		@Override
		public int getCount()
		{
			return getChildCount();
		}

		@Override
		public boolean isViewFromObject(final View view, final Object object)
		{
			return view == object;
		}

		@Override
		public CharSequence getPageTitle(final int position)
		{
			final Object title = getChildAt(position).getTag(R.id.showTitle);
			return title instanceof String ? (String) title : String.valueOf(position);
		}

	}

	public AutoViewPager(final Context context)
	{
		super(context);
	}

	public AutoViewPager(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		setOffscreenPageLimit(getChildCount());
		setAdapter(new AutoPagerAdapter());
	}

}

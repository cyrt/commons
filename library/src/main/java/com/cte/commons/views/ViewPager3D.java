package com.cte.commons.views;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import com.cte.commons.Console;
import com.cte.commons.R;
import com.cte.commons.Version;

/**
 * Reusable ViewPager widget based on the {@link ViewPager} class from Androids V4 compatibility package.
 * Features 3D swipe effect, 3D overscroll effect and subtle fade out during swipe and over scroll.<br>
 * It is borrowed from library <a href="https://github.com/inovex/ViewPager3D">ViewPager3D</a> with theses changes:
 * <ul>
 * <li>changes dependency from NineOldAndroids library to ActionBarSherlock</li>
 * <li>optimized (removed useless code and changed double arithmetics to float)</li>
 * <li>added option to disable 3D effect on pre-{@link VERSION_CODES#HONEYCOMB HC} devices (that don't support hardware acceleration for 3D)</li>
 * </ul>
 * <u>Note:</u>
 * As of {@link VERSION_CODES#JELLY_BEAN Android 4.1}, there is a rendering bug when using static transformations
 * in an hardware-accelerated activity. To selectively enable hardware acceleration depending on the device, use <br>
 * <code>android:hardwareAccelerated="@style/ViewPager3D.hardwareAccelerated"</code><br>
 * in the AndroidManifest.xml for each activity hosting a ViewPager3D.
 *
 * @since 12 sept. 2012 09:50:32
 * @author cte
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ViewPager3D extends ViewPager implements Animator.AnimatorListener
{

	/**
	 * Cannot just implement the interface in {@link ViewPager3D} because {@link ViewPager} already has a method {@link ViewPager#onPageScrolled onPageScrolled}
	 *
	 */
	private class InnerOnPageChangeListener implements OnPageChangeListener
	{
		@Override
		public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels)
		{
			if (ViewPager3D.this.mScrollListener != null)
			{
				ViewPager3D.this.mScrollListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
			}
			ViewPager3D.this.mScrollPosition = position;
			ViewPager3D.this.mScrollPositionOffset = positionOffset;
		}

		@Override
		public void onPageSelected(final int position)
		{
			if (ViewPager3D.this.mScrollListener != null)
			{
				ViewPager3D.this.mScrollListener.onPageSelected(position);
			}
		}

		@Override
		public void onPageScrollStateChanged(final int state)
		{
			if (ViewPager3D.this.mScrollListener != null)
			{
				ViewPager3D.this.mScrollListener.onPageScrollStateChanged(state);
			}
			if (state == SCROLL_STATE_IDLE)
			{
				ViewPager3D.this.mScrollPositionOffset = 0;
			}
		}
	}

	/**
	 * maximum overscroll rotation of the children is 90 divided by this value
	 */
	final static float							DEFAULT_OVERSCROLL_ROTATION				= 6f;

	/**
	 * maximum z distance to translate child view
	 */
	final static int							DEFAULT_OVERSCROLL_TRANSLATION			= 150;

	/**
	 * maximum z distance during swipe
	 */
	final static int							DEFAULT_SWIPE_TRANSLATION				= 100;

	/**
	 * maximum rotation during swipe is 90 divided by this value
	 */
	final static float							DEFAULT_SWIPE_ROTATION					= 3f;

	/**
	 * duration of overscroll animation in ms
	 */
	final private static int					DEFAULT_OVERSCROLL_ANIMATION_DURATION	= 400;

	/**
	 * Default interpolator for animation
	 */
	private static final DecelerateInterpolator	DECELERATE_INTERPOLATOR					= new DecelerateInterpolator();

	/**
	 * if true alpha of children gets animated during swipe and overscroll
	 */
	final private static boolean				DEFAULT_ANIMATE_ALPHA					= true;

	private final static int					INVALID_POINTER_ID						= -1;

	/** avoid doubles and use only float arithmetics */
	private final static float					PI										= (float) Math.PI;
	private final static float					RADIANS									= 180f / PI;

	final private Camera						mCamera									= new Camera();

	private OnPageChangeListener				mScrollListener;
	private float								mLastMotionX							= 0f;
	private int									mActivePointerId						= 0;
	private int									mScrollPosition							= 0;
	private float								mScrollPositionOffset					= 0f;
	private int									mTouchSlop;

	private float								mOverscrollRotation						= DEFAULT_OVERSCROLL_ROTATION;
	private float								mSwipeRotation							= DEFAULT_SWIPE_ROTATION;
	private int									mSwipeTranslation						= DEFAULT_SWIPE_TRANSLATION;
	private int									mOverscrollTranslation					= DEFAULT_OVERSCROLL_TRANSLATION;
	/** int, but stored as a float to avoid auto-boxing in {@link #isOverscrolling} */
	private float								mOverscrollAnimationDuration			= DEFAULT_OVERSCROLL_ANIMATION_DURATION;
	private boolean								mAnimateAlpha							= DEFAULT_ANIMATE_ALPHA;

	// OverscrollEffect
	private float								mOverscroll;
	private Animator mOverscrollAnimator;

	/**
	 * Toggles for 3D effect. When <code>false</code>, this is just a regular {@link ViewPager}
	 */
	private boolean								mIsEnabled;

	public ViewPager3D(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
		if (init(attrs))
		{
			setStaticTransformationsEnabled(true);
			final ViewConfiguration configuration = ViewConfiguration.get(context);
			this.mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(configuration);
			super.setOnPageChangeListener(new InnerOnPageChangeListener());
		}
	}

	public ViewPager3D(final Context context, final AttributeSet attrs, final int defStyle)
	{
		this(context, attrs);
	}

	/**
	 * @param context
	 * @since 13 sept. 2012 10:47:25
	 * @author cte
	 */
	public ViewPager3D(final Context context)
	{
		this(context, null);
	}

	@Override
	public void onRestoreInstanceState(final Parcelable state)
	{
		super.onRestoreInstanceState(state);

		if (this.mIsEnabled)
		{
			// workaround for a bug when rotating screen + current item > 0
			// we need to "force" the state of the animation (elsewhere it will think the displayed page is the first)
			// trick is to setCurrentItem to 0 now, and post a setCurrentItem later when state is initialized
			final int restored = getCurrentItem();
			setCurrentItem(0, false);
			post(new Runnable()
			{

				@Override
				public void run()
				{
					setCurrentItem(restored, false);
				}
			});
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.support.v4.view.ViewPager#setCurrentItem(int)
	 *
	 * @since 24 janv. 2013 16:32:12
	 */
	@Override
	public void setCurrentItem(final int item)
	{
		post(new Runnable()
		{
			@Override
			public void run()
			{
				ViewPager3D.super.setCurrentItem(item);
			}
		});
	}

	private boolean init(final AttributeSet attrs)
	{
		final TypedArray a = null == attrs ? null : getContext().obtainStyledAttributes(attrs, R.styleable.ViewPager3D);

		// by default, we are enabled only on devices supporting 3D acceleration for this view
		// That is: HC and ICS. For Jelly Bean, we disable 3D if hardware acceleration is enabled at Activity level
		// it is possible to override this by using the attribute "enable_3d" in the layout.

		final boolean isSupportedSdk = isSdkSupported();
		this.mIsEnabled = a != null ? a.getBoolean(R.styleable.ViewPager3D_enable_3d, isSupportedSdk) : isSupportedSdk;

		if (!this.mIsEnabled)
		{
			Console.w("ViewPager3D is disabled (supported by SDK?", isSupportedSdk, ")");
			if (a != null)
			{
				a.recycle();
			}
			return false;
		}

		if (a != null)
		{
			this.mOverscrollRotation = a.getFloat(R.styleable.ViewPager3D_overscroll_rotation, DEFAULT_OVERSCROLL_ROTATION);
			this.mSwipeRotation = a.getFloat(R.styleable.ViewPager3D_swipe_rotation, DEFAULT_SWIPE_ROTATION);
			this.mSwipeTranslation = a.getInt(R.styleable.ViewPager3D_swipe_translation, DEFAULT_SWIPE_TRANSLATION);
			this.mOverscrollTranslation = a.getInt(R.styleable.ViewPager3D_overscroll_translation, DEFAULT_OVERSCROLL_TRANSLATION);
			this.mOverscrollAnimationDuration = a.getInt(R.styleable.ViewPager3D_overscroll_animation_duration, DEFAULT_OVERSCROLL_ANIMATION_DURATION);
			this.mAnimateAlpha = a.getBoolean(R.styleable.ViewPager3D_animate_alpha, DEFAULT_ANIMATE_ALPHA);
			a.recycle();
		}

		return true;
	}

	/**
	 * @return
	 * @since 24 sept. 2012 10:31:47
	 * @author cte
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public boolean isSdkSupported()
	{
		return Version.IS_HONEYCOMB || (Version.IS_JELLY && !isHardwareAccelerated());
	}

	public boolean isAnimateAlpha()
	{
		return this.mAnimateAlpha;
	}

	public void setAnimateAlpha(final boolean mAnimateAlpha)
	{
		this.mAnimateAlpha = mAnimateAlpha;
	}

	public int getOverscrollAnimationDuration()
	{
		return (int) this.mOverscrollAnimationDuration;
	}

	public void setOverscrollAnimationDuration(final int mOverscrollAnimationDuration)
	{
		this.mOverscrollAnimationDuration = mOverscrollAnimationDuration;
	}

	public int getSwipeTranslation()
	{
		return this.mSwipeTranslation;
	}

	public void setSwipeTranslation(final int mSwipeTranslation)
	{
		this.mSwipeTranslation = mSwipeTranslation;
	}

	public int getOverscrollTranslation()
	{
		return this.mOverscrollTranslation;
	}

	public void setOverscrollTranslation(final int mOverscrollTranslation)
	{
		this.mOverscrollTranslation = mOverscrollTranslation;
	}

	public float getSwipeRotation()
	{
		return this.mSwipeRotation;
	}

	public void setSwipeRotation(final float mSwipeRotation)
	{
		this.mSwipeRotation = mSwipeRotation;
	}

	public float getOverscrollRotation()
	{
		return this.mOverscrollRotation;
	}

	public void setOverscrollRotation(final float mOverscrollRotation)
	{
		this.mOverscrollRotation = mOverscrollRotation;
	}

	@Override
	public void setOnPageChangeListener(final OnPageChangeListener listener)
	{
		if (this.mIsEnabled)
		{
			this.mScrollListener = listener;
		}
		else
		{
			super.setOnPageChangeListener(listener);
		}
	};

	@Override
	public boolean onInterceptTouchEvent(final MotionEvent ev)
	{
		if (!this.mIsEnabled)
		{
			return super.onInterceptTouchEvent(ev);
		}

		final int action = ev.getAction() & MotionEventCompat.ACTION_MASK;
		switch (action)
		{
			case MotionEvent.ACTION_DOWN: {
				this.mLastMotionX = ev.getX();
				this.mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
				break;
			}
			case MotionEventCompat.ACTION_POINTER_DOWN: {
				final int index = MotionEventCompat.getActionIndex(ev);
				final float x = MotionEventCompat.getX(ev, index);
				this.mLastMotionX = x;
				this.mActivePointerId = MotionEventCompat.getPointerId(ev, index);
				break;
			}
		}
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	public boolean onTouchEvent(final MotionEvent ev)
	{
		if (!this.mIsEnabled)
		{
			return super.onTouchEvent(ev);
		}

		boolean callSuper = false;

		final int action = ev.getAction();
		switch (action)
		{
			case MotionEvent.ACTION_DOWN: {
				this.mLastMotionX = ev.getX();
				this.mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
				break;
			}
			case MotionEventCompat.ACTION_POINTER_DOWN: {
				callSuper = true;
				final int index = MotionEventCompat.getActionIndex(ev);
				final float x = MotionEventCompat.getX(ev, index);
				this.mLastMotionX = x;
				this.mActivePointerId = MotionEventCompat.getPointerId(ev, index);
				break;
			}
			case MotionEvent.ACTION_MOVE: {
				if (this.mActivePointerId != INVALID_POINTER_ID)
				{
					// Scroll to follow the motion event
					final int activePointerIndex = MotionEventCompat.findPointerIndex(ev, this.mActivePointerId);
					float x;
					try
					{
						x = MotionEventCompat.getX(ev, activePointerIndex);
					}
					catch (final IllegalArgumentException pointerIndexOutOfRangeException)
					{
						/* ignore invalid event */
						break;
					}

					if (this.mScrollPositionOffset == 0)
					{
						final float deltaX = this.mLastMotionX - x;
						final float oldScrollX = getScrollX();
						final int width = getWidth();

						final int widthWithMargin = width + getPageMargin();
						final int currentItemIndex = getCurrentItem();
						final float leftBound = Math.max(0, (currentItemIndex - 1) * widthWithMargin);
						final float scrollX = oldScrollX + deltaX;

						if (scrollX < leftBound)
						{
							if (leftBound == 0)
							{
								final float over = deltaX + this.mTouchSlop;
								setPull(over / width);
							}
						}
						else
						{
							final int lastItemIndex = getAdapter().getCount() - 1;
							final float rightBound = Math.min(currentItemIndex + 1, lastItemIndex) * widthWithMargin;
							if (scrollX > rightBound)
							{
								if (rightBound == lastItemIndex * widthWithMargin)
								{
									final float over = scrollX - rightBound - this.mTouchSlop;
									setPull(over / width);
								}
							}
						}
					}
					else
					{
						this.mLastMotionX = x;
					}
				}
				else
				{
					onRelease();
				}
				break;
			}
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL: {
				this.mActivePointerId = INVALID_POINTER_ID;
				onRelease();
				break;
			}
			case MotionEvent.ACTION_POINTER_UP: {
				final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
				final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);
				if (pointerId == this.mActivePointerId)
				{
					// This was our active pointer going up. Choose a new
					// active pointer and adjust accordingly.
					final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
					this.mLastMotionX = ev.getX(newPointerIndex);
					this.mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
					callSuper = true;
				}
				break;
			}
		}

		if (isOverscrolling() && !callSuper)
		{
			return true;
		}
		return super.onTouchEvent(ev);
	}

	@Override
	protected boolean getChildStaticTransformation(final View child, final Transformation t)
	{
		if (child.getWidth() == 0)
		{
			return false;
		}

		final int position = child.getLeft() / child.getWidth();
		final boolean isFirstOrLast = position == 0 || (position == getAdapter().getCount() - 1);
		final Matrix matrix = t.getMatrix();
		final Camera camera = this.mCamera;

		if (isOverscrolling() && isFirstOrLast)
		{
			final float dx = getWidth() / 2;
			final int dy = getHeight() / 2;
			matrix.reset();
			final float translateZ = (float) (this.mOverscrollTranslation * Math.sin(PI * Math.abs(this.mOverscroll)));
			final float degrees = 90 / this.mOverscrollRotation - ((RADIANS * (float) Math.acos(this.mOverscroll)) / this.mOverscrollRotation);

			camera.save();
			camera.rotateY(degrees);
			camera.translate(0, 0, translateZ);
			camera.getMatrix(matrix);
			camera.restore();

			matrix.preTranslate(-dx, -dy);
			matrix.postTranslate(dx, dy);

			if (this.mAnimateAlpha)
			{
				t.setTransformationType(Transformation.TYPE_BOTH);
				t.setAlpha((float) Math.sin((1 - Math.abs(this.mOverscroll)) * PI / 2));
			}

			return true;
		}
		else if (this.mScrollPositionOffset > 0)
		{

			final float dx = getWidth() / 2;
			final int dy = getHeight() / 2;

			final float degrees;
			if (position > this.mScrollPosition)
			{
				if (this.mAnimateAlpha)
				{
					t.setTransformationType(Transformation.TYPE_BOTH);
					t.setAlpha((float) Math.sin(this.mScrollPositionOffset * PI / 2));
				}
				// right side
				degrees = -(90 / this.mSwipeRotation) + (RADIANS * (float) Math.acos(1 - this.mScrollPositionOffset)) / this.mSwipeRotation;
			}
			else
			{
				if (this.mAnimateAlpha)
				{
					t.setTransformationType(Transformation.TYPE_BOTH);
					t.setAlpha((float) Math.sin(this.mScrollPositionOffset * PI / 2 + PI / 2));
				}
				// left side
				degrees = (90 / this.mSwipeRotation) - (RADIANS * (float) Math.acos(this.mScrollPositionOffset)) / this.mSwipeRotation;
			}
			final float translateZ = this.mSwipeTranslation * (float) Math.sin(PI * this.mScrollPositionOffset);

			matrix.reset();
			camera.save();
			camera.rotateY(degrees);
			camera.translate(0, 0, translateZ);
			camera.getMatrix(matrix);
			camera.restore();
			// pivot point is center of child
			matrix.preTranslate(-dx, -dy);
			matrix.postTranslate(dx, dy);
			return true;
		}
		return false;
	}

	/**
	 * @param deltaDistance
	 *            [0..1] 0->no overscroll, 1>full overscroll
	 */
	public void setPull(final float deltaDistance)
	{
		this.mOverscroll = deltaDistance;
		invalidate();
	}

	/**
	 * called when finger is released. starts to animate back to default
	 * position
	 */
	private void onRelease()
	{
		if (this.mOverscrollAnimator != null && this.mOverscrollAnimator.isRunning())
		{
			this.mOverscrollAnimator.addListener(this);
			this.mOverscrollAnimator.cancel();
		}
		else
		{
			startAnimation(0);
		}
	}

	private void startAnimation(final float target)
	{
		this.mOverscrollAnimator = ObjectAnimator.ofFloat(this, "pull", this.mOverscroll, target);
		this.mOverscrollAnimator.setInterpolator(DECELERATE_INTERPOLATOR);
		final float scale = Math.abs(target - this.mOverscroll);
		this.mOverscrollAnimator.setDuration((long) (this.mOverscrollAnimationDuration * scale));
		this.mOverscrollAnimator.start();
	}

	private boolean isOverscrolling()
	{
		if (this.mScrollPosition == 0 && this.mOverscroll < 0)
		{
			return true;
		}
		final boolean isLast = (getAdapter().getCount() - 1) == this.mScrollPosition;
		if (isLast && this.mOverscroll > 0)
		{
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.actionbarsherlock.internal.nineoldandroids.animation.Animator.AnimatorListener#onAnimationStart(com.actionbarsherlock.internal.nineoldandroids.animation.Animator)
	 *
	 * @since 13 sept. 2012 12:20:53
	 */
	@Override
	public void onAnimationStart(final Animator animation)
	{
		// unused
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.actionbarsherlock.internal.nineoldandroids.animation.Animator.AnimatorListener#onAnimationEnd(com.actionbarsherlock.internal.nineoldandroids.animation.Animator)
	 *
	 * @since 13 sept. 2012 12:20:53
	 */
	@Override
	public void onAnimationEnd(final Animator animation)
	{
		startAnimation(0);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.actionbarsherlock.internal.nineoldandroids.animation.Animator.AnimatorListener#onAnimationCancel(com.actionbarsherlock.internal.nineoldandroids.animation.Animator)
	 *
	 * @since 13 sept. 2012 12:20:53
	 */
	@Override
	public void onAnimationCancel(final Animator animation)
	{
		// unused
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.actionbarsherlock.internal.nineoldandroids.animation.Animator.AnimatorListener#onAnimationRepeat(com.actionbarsherlock.internal.nineoldandroids.animation.Animator)
	 *
	 * @since 13 sept. 2012 12:20:53
	 */
	@Override
	public void onAnimationRepeat(final Animator animation)
	{
		// unused
	}
}
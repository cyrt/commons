/**
 * 
 */
package com.cte.commons.views;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.widget.SectionIndexer;

import java.util.ArrayList;

/**
 *
 * @since 5 déc. 2012 10:44:31
 * @author cte
 */
public abstract class IndexedCursorAdapter extends CursorAdapter implements SectionIndexer
{

    protected String[] mSections;
    protected ArrayList<Integer> mSectionPositions;

    /**
     * @param context
     * @param c
     * @param autoRequery
     * @since 5 déc. 2012 10:44:31
     * @author cte
     */
    public IndexedCursorAdapter(final Context context, final Cursor c, final boolean autoRequery)
    {
        super(context, c, autoRequery);
    }

    /**
     * @param context
     * @param c
     * @param flags
     * @since 5 déc. 2012 10:44:31
     * @author cte
     */
    public IndexedCursorAdapter(final Context context, final Cursor c, final int flags)
    {
        super(context, c, flags);
    }

    @Override
    public int getPositionForSection(final int section)
    {
        if (null == this.mSectionPositions)
        {
            return 0;
        }
        final int last = this.mSectionPositions.size() - 1;
        if (section > last)
        {
            return this.mSectionPositions.get(last);
        }
        return this.mSectionPositions.get(section);
    }

    @Override
    public int getSectionForPosition(final int position)
    {
        if (null == this.mSectionPositions)
        {
            return 0;
        }
        final int sectionCount = this.mSectionPositions.size();
        if (sectionCount <= 1)
        {
            return 0;
        }
        if (this.mSectionPositions.get(sectionCount - 1) < position)
        {
            return sectionCount;
        }
        int section = 0;
        while (this.mSectionPositions.get(section) < position)
        {
            section++;
        }
        return section;
    }

}
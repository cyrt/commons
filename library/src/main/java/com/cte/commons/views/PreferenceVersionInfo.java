/**
 *
 */
package com.cte.commons.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.PreferenceScreen;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cte.commons.Console;
import com.cte.commons.R;
import com.cte.commons.Version;

/**
 * {@link DialogPreference} that displays an info popup ("About").<br>
 * <br>
 * Summary of the preference (shown in the preference screen) and message in the popup
 * are built upon {@code R.string.pvi_about_version_info}.<br>
 * <br>
 * Default layout is {@code R.layout.pvi_popup_about}.
 * If customized, following items may be included:
 * <ul>
 * <li>an {@link ImageView} with id {@code android.R.id.icon} will be set to the package icon</li>
 * <li>a {@link TextView} with id {@code android.R.id.title} will be set to the application label</li>
 * <li>a {@link TextView} with id {@code android.R.id.summary} will be set to the summary (see above)</li>
 * <li>a {@link TextView} with id {@code android.R.id.copy} will be set to {@code R.string.pvi_about_copyright}</li>
 * </ul>
 * By default, the dialog is displayed with {@link Builder#setInverseBackgroundForced inverse background}. Override {@link #onPrepareDialogBuilder} to change this behavior. <br>
 * Finally, using {@link #showDialog} allows to display this dialog from outside a {@link PreferenceScreen}.
 * 
 * @since 26 janv. 2012 08:41:16
 * @author cte
 */
public class PreferenceVersionInfo extends DialogPreference
{

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 * @since 26 janv. 2012 08:42:24
	 * @author cte
	 */
	public PreferenceVersionInfo(final Context context, final AttributeSet attrs, final int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}

	/**
	 * @param context
	 * @param attrs
	 * @since 26 janv. 2012 08:42:24
	 * @author cte
	 */
	public PreferenceVersionInfo(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	/**
	 * 
	 * @since 26 janv. 2012 09:56:02
	 * @author cte
	 */
	@SuppressLint("StringFormatMatches")
	@TargetApi(9)
	private void init()
	{
		setPersistent(false);
		setDialogLayoutResource(R.layout.pvi_popup_about);
		setNegativeButtonText(null);

		// label for pvi_about_version_info will be stored & displayed in the summary
		try
		{
			final Context context = getContext();
			final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			final String summaryString;
			if (Version.IS_HONEYCOMB)
			{
				summaryString = context.getString(
						R.string.pvi_about_version_info,
						packageInfo.versionName,
						DateUtils.getRelativeTimeSpanString(context, packageInfo.lastUpdateTime, true).toString().toLowerCase()
						);
			}
			else
			{
				summaryString = context.getString(
						R.string.pvi_about_version_info,
						packageInfo.versionName);
			}
			if (Console.DEBUG_MODE)
			{
				setSummary(summaryString + " [/!\\ DEBUG MODE]"); //$NON-NLS-1$
			}
			else
			{
				setSummary(summaryString);
			}

		}
		catch (final Exception e)
		{
			setSummary(android.R.string.unknownName);
		}
	}

	@Override
	protected void showDialog(final Bundle state)
	{
		// check whether we are in a PreferenceScreen or standalone.
		if (null != getPreferenceManager())
		{
			// PreferenceScreen -> use super
			super.showDialog(state);
			return;
		}

		// standalone -> can't use super because PreferenceManager is null
		final Context context = getContext();

		final Builder builder = new AlertDialog.Builder(context)
				.setIcon(getDialogIcon())
				.setTitle(getDialogTitle())
				.setPositiveButton(getPositiveButtonText(), this)
				.setNegativeButton(getNegativeButtonText(), this);

		final View contentView = LayoutInflater.from(context).inflate(getLayoutResource(), null);
		if (contentView != null)
		{
			onBindDialogView(contentView);
			builder.setView(contentView);
		}
		else
		{
			builder.setMessage(getDialogMessage());
		}

		onPrepareDialogBuilder(builder);

		// Create the dialog
		final Dialog dialog = builder.create();

		dialog.setOnDismissListener(this);
		dialog.show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.preference.DialogPreference#onDismiss(android.content.DialogInterface)
	 * 
	 * @since 13 août 2012 10:47:42
	 */
	@Override
	public void onDismiss(final DialogInterface dialog)
	{
		if (null != getPreferenceManager())
		{
			// standalone -> can't use super because PreferenceManager is null
			super.onDismiss(dialog);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.preference.DialogPreference#onPrepareDialogBuilder(android.app.AlertDialog.Builder)
	 * 
	 * @since 13 août 2012 11:32:23
	 */
	@Override
	protected void onPrepareDialogBuilder(final Builder builder)
	{
		builder.setInverseBackgroundForced(true);
		super.onPrepareDialogBuilder(builder);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.preference.DialogPreference#onBindDialogView(android.view.View)
	 * 
	 * @since 13 août 2012 10:07:33
	 */
	@Override
	protected void onBindDialogView(final View content)
	{
		final Context context = getContext();
		final PackageManager pm = context.getPackageManager();

		final ImageView icon = (ImageView) content.findViewById(android.R.id.icon);
		if (null != icon)
		{
			final Drawable d = context.getApplicationInfo().loadIcon(pm);
			icon.setImageDrawable(d);
		}

		final TextView title = (TextView) content.findViewById(android.R.id.title);
		if (null != title)
		{
			final CharSequence t = context.getApplicationInfo().loadLabel(pm);
			title.setText(t);
		}

		final TextView version = (TextView) content.findViewById(android.R.id.summary);
		if (null != version)
		{
			version.setText(getSummary());
		}
		final TextView copy = (TextView) content.findViewById(android.R.id.copy);
		if (null != copy)
		{
			final CharSequence t = context.getString(R.string.pvi_about_copyright);
			copy.setText(t);
		}
	}

	/**
	 * @see #showDialog(Bundle)
	 * 
	 * @since 13 août 2012 10:23:55
	 * @author cte
	 */
	public void showDialog()
	{
		showDialog(null);
	}
}

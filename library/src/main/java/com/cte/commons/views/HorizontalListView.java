package com.cte.commons.views;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Scroller;

import java.util.LinkedList;
import java.util.Queue;

public class HorizontalListView extends AdapterView<ListAdapter> implements Runnable
{

	public boolean mAlwaysOverrideTouch = true;
	protected ListAdapter mAdapter;
	private int mLeftViewIndex = -1;
	private int mRightViewIndex = 0;
	protected int mCurrentX;
	protected int mNextX;
	private int mMaxX = Integer.MAX_VALUE;
	private int mDisplayOffset = 0;
	protected Scroller mScroller;
	private GestureDetector mGesture;
	private final Queue<View> mRemovedViewQueue = new LinkedList<View>();
	private OnItemSelectedListener mOnItemSelected;
	private OnItemClickListener mOnItemClicked;
	private OnItemLongClickListener mOnItemLongClicked;
	private boolean mDataChanged = false;
	private boolean mScrolling;

	public HorizontalListView(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
		initView();
	}

	private synchronized void initView()
	{
		this.mLeftViewIndex = -1;
		this.mRightViewIndex = 0;
		this.mDisplayOffset = getWidth() / 2;
		this.mCurrentX = 0;
		this.mNextX = 0;
		this.mMaxX = Integer.MAX_VALUE;
		this.mScroller = new Scroller(getContext());
		this.mGesture = new GestureDetector(getContext(), this.mOnGesture);
	}

	@Override
	public void setOnItemSelectedListener(final OnItemSelectedListener listener)
	{
		this.mOnItemSelected = listener;
	}

	@Override
	public void setOnItemClickListener(final OnItemClickListener listener)
	{
		this.mOnItemClicked = listener;
	}

	@Override
	public void setOnItemLongClickListener(final OnItemLongClickListener listener)
	{
		this.mOnItemLongClicked = listener;
	}

	private final DataSetObserver mDataObserver = new DataSetObserver()
	{

		@Override
		public void onChanged()
		{
			synchronized (HorizontalListView.this)
			{
				HorizontalListView.this.mDataChanged = true;
			}
			setEmptyView(getEmptyView());
			invalidate();
			requestLayout();
		}

		@Override
		public void onInvalidated()
		{
			reset();
			invalidate();
			requestLayout();
		}

	};

	@Override
	public ListAdapter getAdapter()
	{
		return this.mAdapter;
	}

	@Override
	public View getSelectedView()
	{
		// TODO: implement
		return null;
	}

	@Override
	public void setAdapter(final ListAdapter adapter)
	{
		if (this.mAdapter != null)
		{
			this.mAdapter.unregisterDataSetObserver(this.mDataObserver);
		}
		this.mAdapter = adapter;
		this.mAdapter.registerDataSetObserver(this.mDataObserver);
		reset();
	}

	private synchronized void reset()
	{
		initView();
		removeAllViewsInLayout();
		requestLayout();
	}

	@Override
	public void setSelection(final int position)
	{
		// TODO: implement
	}

	private void addAndMeasureChild(final View child, final int viewPos)
	{
		ViewGroup.LayoutParams params = child.getLayoutParams();
		if (params == null)
		{
			params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		}

		addViewInLayout(child, viewPos, params, true);
		child.measure(MeasureSpec.makeMeasureSpec(child.getLayoutParams().width, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(child.getLayoutParams().height, MeasureSpec.EXACTLY));
	}

	@Override
	protected synchronized void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom)
	{
		super.onLayout(changed, left, top, right, bottom);

		if (this.mAdapter == null)
		{
			return;
		}

		if (this.mDataChanged)
		{
			final int oldCurrentX = this.mCurrentX;
			initView();
			removeAllViewsInLayout();
			this.mNextX = oldCurrentX;
			this.mDataChanged = false;
		}

		final boolean computeScrollOffset = this.mScroller.computeScrollOffset();
		if (computeScrollOffset)
		{
			final int scrollx = this.mScroller.getCurrX();
			this.mNextX = scrollx;
		}

		if (this.mNextX <= 0)
		{
			this.mNextX = 0;
			this.mScroller.forceFinished(true);
		}
		if (this.mNextX >= this.mMaxX)
		{
			this.mNextX = this.mMaxX;
			this.mScroller.forceFinished(true);
		}

		final int dx = this.mCurrentX - this.mNextX;

		removeNonVisibleItems(dx);
		fillList(dx);
		positionItems(dx);

		this.mCurrentX = this.mNextX;

		final boolean finished = this.mScroller.isFinished();
		if (!finished)
		{
			post(this);
		}
		else if (this.mScrolling)
		{
			onScrollStateChanged(OnScrollListener.SCROLL_STATE_IDLE);
		}
		this.mScrolling = !finished;
	}

	@Override
	public void run()
	{
		requestLayout();
	}

	private void fillList(final int dx)
	{
		int edge = 0;
		View child = getChildAt(getChildCount() - 1);
		if (child != null)
		{
			edge = child.getRight();
		}
		fillListRight(edge, dx);

		edge = 0;
		child = getChildAt(0);
		if (child != null)
		{
			edge = child.getLeft();
		}
		fillListLeft(edge, dx);

	}

	private void fillListRight(int rightEdge, final int dx)
	{
		while (rightEdge + dx < getWidth() && this.mRightViewIndex < this.mAdapter.getCount())
		{

			final View child = this.mAdapter.getView(this.mRightViewIndex, this.mRemovedViewQueue.poll(), this);
			addAndMeasureChild(child, -1);
			rightEdge += child.getMeasuredWidth();

			if (this.mRightViewIndex == this.mAdapter.getCount() - 1)
			{
				this.mMaxX = this.mCurrentX + rightEdge + getComponentCenter() - getWidth();
			}

			if (this.mMaxX < 0)
			{
				this.mMaxX = 0;
			}
			this.mRightViewIndex++;
		}

	}

	private void fillListLeft(int leftEdge, final int dx)
	{
		while (leftEdge + dx > 0 && this.mLeftViewIndex >= 0)
		{
			final View child = this.mAdapter.getView(this.mLeftViewIndex, this.mRemovedViewQueue.poll(), this);
			addAndMeasureChild(child, 0);
			leftEdge -= child.getMeasuredWidth();
			this.mLeftViewIndex--;
			this.mDisplayOffset -= child.getMeasuredWidth();
		}
	}

	private void removeNonVisibleItems(final int dx)
	{
		View child = getChildAt(0);
		while (child != null && child.getRight() + dx <= 0)
		{
			this.mDisplayOffset += child.getMeasuredWidth();
			this.mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			this.mLeftViewIndex++;
			child = getChildAt(0);

		}

		child = getChildAt(getChildCount() - 1);
		while (child != null && child.getLeft() + dx >= getWidth())
		{
			this.mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			this.mRightViewIndex--;
			child = getChildAt(getChildCount() - 1);
		}
	}

	private void positionItems(final int dx)
	{
		if (getChildCount() > 0)
		{
			this.mDisplayOffset += dx;
			int left = this.mDisplayOffset;
			for (int i = 0; i < getChildCount(); i++)
			{
				final View child = getChildAt(i);
				final int childWidth = child.getMeasuredWidth();
				child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
				left += childWidth;
			}
		}
	}

	public synchronized void scrollTo(final int x)
	{
		this.mScrolling = true;
		this.mScroller.startScroll(this.mNextX, 0, x - this.mNextX, 0);
		requestLayout();
	}

	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev)
	{
		final boolean handled = this.mGesture.onTouchEvent(ev);
		if (!this.mScrolling && ev.getAction() == MotionEvent.ACTION_UP)
		{
			onScrollStateChanged(OnScrollListener.SCROLL_STATE_IDLE);
		}
		return handled;
	}

	protected boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX,
			final float velocityY)
	{
		synchronized (HorizontalListView.this)
		{
			this.mScrolling = true;
			this.mScroller.fling(this.mNextX, 0, (int) -velocityX, 0, 0, this.mMaxX, 0, 0);
		}
		requestLayout();

		return true;
	}

	protected boolean onDown(final MotionEvent e)
	{
		this.mScroller.forceFinished(true);
		return true;
	}

	private final OnGestureListener mOnGesture = new GestureDetector.SimpleOnGestureListener()
	{

		@Override
		public boolean onDown(final MotionEvent e)
		{
			return HorizontalListView.this.onDown(e);
		}

		@Override
		public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX,
				final float velocityY)
		{
			return HorizontalListView.this.onFling(e1, e2, velocityX, velocityY);
		}

		@Override
		public boolean onScroll(final MotionEvent e1, final MotionEvent e2,
				final float distanceX, final float distanceY)
		{

			getParent().requestDisallowInterceptTouchEvent(true);

			synchronized (HorizontalListView.this)
			{
				HorizontalListView.this.mNextX += (int) distanceX;
			}

			requestLayout();

			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(final MotionEvent e)
		{
			final Rect viewRect = new Rect();
			for (int i = 0; i < getChildCount(); i++)
			{
				final View child = getChildAt(i);
				final int left = child.getLeft();
				final int right = child.getRight();
				final int top = child.getTop();
				final int bottom = child.getBottom();
				viewRect.set(left, top, right, bottom);
				if (viewRect.contains((int) e.getX(), (int) e.getY()))
				{
					if (HorizontalListView.this.mOnItemClicked != null)
					{
						HorizontalListView.this.mOnItemClicked.onItemClick(HorizontalListView.this, child, HorizontalListView.this.mLeftViewIndex + 1 + i, HorizontalListView.this.mAdapter.getItemId(HorizontalListView.this.mLeftViewIndex + 1 + i));
					}
					if (HorizontalListView.this.mOnItemSelected != null)
					{
						HorizontalListView.this.mOnItemSelected.onItemSelected(HorizontalListView.this, child, HorizontalListView.this.mLeftViewIndex + 1 + i, HorizontalListView.this.mAdapter.getItemId(HorizontalListView.this.mLeftViewIndex + 1 + i));
					}
					break;
				}

			}
			return true;
		}

		@Override
		public void onLongPress(final MotionEvent e)
		{
			final Rect viewRect = new Rect();
			final int childCount = getChildCount();
			for (int i = 0; i < childCount; i++)
			{
				final View child = getChildAt(i);
				final int left = child.getLeft();
				final int right = child.getRight();
				final int top = child.getTop();
				final int bottom = child.getBottom();
				viewRect.set(left, top, right, bottom);
				if (viewRect.contains((int) e.getX(), (int) e.getY()))
				{
					if (HorizontalListView.this.mOnItemLongClicked != null)
					{
						HorizontalListView.this.mOnItemLongClicked.onItemLongClick(HorizontalListView.this, child, HorizontalListView.this.mLeftViewIndex + 1 + i, HorizontalListView.this.mAdapter.getItemId(HorizontalListView.this.mLeftViewIndex + 1 + i));
					}
					break;
				}

			}
		}

	};

	private int getComponentCenter()
	{
		return getWidth() / 2;
	}

	private void onScrollStateChanged(final int scrollState)
	{
		if (scrollState == OnScrollListener.SCROLL_STATE_IDLE)
		{
			final int first = getFirstVisiblePosition();
			final int last = getLastVisiblePosition();
			final int avg = (first + last) / 2;
			final View view = findCenterView(avg);
			if (null != view) // if no adapter or empty adapter
			{
				final int actualViewCenter = view.getLeft() + view.getWidth() / 2;
				final int scrollBy = getComponentCenter() - actualViewCenter;
				this.scrollBy(scrollBy, 0);
			}
		}
	}

	public View findCenterView(final int position)
	{
		// Console.i("position:", position);
		if (position < 1)
		{
			return getChildAt(0);
		}

		if (position > getChildCount() - 2)
		{
			return getChildAt(getChildCount() - 1);
		}

		final View v = getChildAt(position);

		if (getComponentCenter() < v.getLeft())
		{
			return findCenterView(position - 1);
		}
		if (getComponentCenter() > v.getRight())
		{
			return findCenterView(position + 1);
		}

		return v;
	}
}
package com.cte.commons.views.indexable;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;

public class IndexableListView extends ListView {

    private boolean mIsFastScrollEnabled = false;
    private IndexScroller mScroller = null;
    private GestureDetector mGestureDetector = null;

    public IndexableListView(final Context context) {
        super(context);
    }

    public IndexableListView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public IndexableListView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean isFastScrollEnabled() {
        return this.mIsFastScrollEnabled;
    }

    @Override
    public void setFastScrollEnabled(final boolean enabled) {
        this.mIsFastScrollEnabled = enabled;
        if (this.mIsFastScrollEnabled) {
            if (this.mScroller == null)
            {
                this.mScroller = new IndexScroller(getContext(), this);
            }
        } else {
            if (this.mScroller != null) {
                this.mScroller.hide();
                this.mScroller = null;
            }
        }
    }

    public void setExtraIndexbarMargins(final float left, final float top, final float right, final float bottom)
    {
        if (this.mIsFastScrollEnabled && this.mScroller != null)
        {
            this.mScroller.setExtraIndexbarMargins(left, top, right, bottom);
        }
    }
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);

        // Overlay index bar
        if (this.mScroller != null)
        {
            this.mScroller.draw(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(final MotionEvent ev) {
        // Intercept ListView's touch event
        if (this.mScroller != null && this.mScroller.onTouchEvent(ev))
        {
            return true;
        }

        if (this.mGestureDetector == null) {
            this.mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(final MotionEvent e1, final MotionEvent e2,
                        final float velocityX, final float velocityY) {
                    // If fling happens, index bar shows
                    if (IndexableListView.this.mScroller != null)
                    {
                        IndexableListView.this.mScroller.show();
                    }
                    return super.onFling(e1, e2, velocityX, velocityY);
                }

            });
        }
        this.mGestureDetector.onTouchEvent(ev);

        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(final MotionEvent ev) {
        if (this.mScroller != null && this.mScroller.onTouchEvent(ev))
        {
            return true;
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public void setAdapter(final ListAdapter adapter) {
        super.setAdapter(adapter);
        if (this.mScroller != null)
        {
            this.mScroller.setAdapter(adapter);
        }
    }

    /* (non-Javadoc)
     * @see android.widget.AbsListView#onLayout(boolean, int, int, int, int)
     * @since 5 déc. 2012 12:04:29
     */
    @Override
    protected void onLayout(final boolean changed, final int l, final int t, final int r, final int b)
    {
        super.onLayout(changed, l, t, r, b);
        if (changed && this.mScroller != null)
        {
            this.mScroller.setAdapter(getAdapter());
        }
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (this.mScroller != null)
        {
            this.mScroller.onSizeChanged(w, h, oldw, oldh);
        }
    }


}

/**
 * 
 */
package com.cte.commons.views;

import java.io.File;
import java.io.IOException;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.cte.commons.Console;
import com.cte.commons.Version;

/**
 * 
 * @since 11 févr. 2013 13:36:21
 * @author cte
 */

@TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
public class ZoomImageView extends View implements Runnable
{

	private static final int			INVALID_POINTER_ID	= -1;

	float								mMinZoom			= 0.8f;
	float								mMaxZoom			= 10.0f;

	private final PointF				mOffset				= new PointF();

	private float						mLastTouchX;
	private float						mLastTouchY;
	private int							mActivePointerId	= INVALID_POINTER_ID;

	private final ScaleGestureDetector	mScaleDetector;
	private float						mScaleFactor		= 1.f;

	private BitmapRegionDecoder			decoder;
	Options								options				= new Options();

	private int							sourceWidth;

	private int							sourceHeight;
	final Matrix						mMatrix				= new Matrix();
	int									viewWidth;
	int									viewHeight;

	private boolean						mFitCenter;

	private final Handler				mWorker;
	private boolean						mIsWorkerDecoding	= false;

	private Bitmap						mCachedBitmap;
	private final PointF				mCachedOffset		= new PointF();

	private boolean						mHighQuality;

	public ZoomImageView(final Context context)
	{
		this(context, null, 0);
	}

	public ZoomImageView(final Context context, final AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public ZoomImageView(final Context context, final AttributeSet attrs, final int defStyle)
	{
		super(context, attrs, defStyle);
		setDrawingCacheEnabled(false);
		this.mHighQuality = false;
		this.mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
		this.options.inTempStorage = new byte[64 * 1024];

		final HandlerThread thread = new HandlerThread(toString(), Thread.MIN_PRIORITY);
		thread.start();
		this.mWorker = new Handler(thread.getLooper());

		this.paint = new Paint();
		this.paint.setTextSize(20);
		this.paint.setColor(Color.WHITE);
		this.paint.setShadowLayer(4, 0, 0, Color.BLACK);

	}

	public void setHighQualityDecoding(final boolean highQuality)
	{
		this.mHighQuality = highQuality;
	}

	public void setImageFile(final File filePath, final boolean fitCenter) throws IOException
	{
		clear();
		this.mFitCenter = fitCenter;
		this.decoder = BitmapRegionDecoder.newInstance(filePath.getPath(), false);
		Console.i(filePath.getPath());

		this.sourceWidth = this.decoder.getWidth();
		this.sourceHeight = this.decoder.getHeight();
		if (0 != this.viewWidth)
		{
			adjustOffsetZoomLimits();
		}
		this.mWorker.sendEmptyMessage(1234);
	}

	public void clear()
	{
		final BitmapRegionDecoder oldDecoder = this.decoder;
		if (null != oldDecoder)
		{
			this.decoder = null;
			// delay in case decoder is currenlty used by the worker
			this.mWorker.post(new Runnable() {
				@Override
				public void run()
				{
					Console.v("clear");
					oldDecoder.recycle();
				}
			});
		}
		reset();
		System.gc();
	}

	/**
	 * 
	 * @since 12 févr. 2013 13:37:38
	 * @author cte
	 */
	protected void reset()
	{
		if (null != this.mCachedBitmap)
		{
			this.mCachedBitmap = null;
		}
		this.mMatrix.reset();
		this.mOffset.set(0, 0);
		this.mCachedOffset.set(0, 0);
	}

	/**
	 * @return the min Zoom
	 * @since 12 févr. 2013 12:15:30
	 */
	public float getMinZoom()
	{
		return this.mMinZoom;
	}

	/**
	 * @return the max Zoom
	 * @since 12 févr. 2013 12:15:36
	 */
	public float getMaxZoom()
	{
		return this.mMaxZoom;
	}

	/**
	 * @param max
	 *            the max zoom to set
	 * @since 12 févr. 2013 12:16:32
	 */
	public void setMaxZoom(final float max)
	{
		this.mMaxZoom = max;
	}

	/**
	 * @return the current Zoom
	 * @since 12 févr. 2013 12:15:46
	 */
	public float getCurrentZoom()
	{
		return this.mScaleFactor;
	}

	/**
	 * @param zoom
	 *            the scale factor to set
	 * @since 12 févr. 2013 12:16:51
	 */
	public void setZoom(final float zoom)
	{
		this.mScaleFactor = zoom;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onSizeChanged(int, int, int, int)
	 * 
	 * @since 11 févr. 2013 16:57:13
	 */
	@Override
	protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		// Console.i(w, h, oldw, oldh);
		this.viewWidth = w;
		this.viewHeight = h;
		if (0 != this.sourceWidth)
		{
			adjustOffsetZoomLimits();
		}
	}

	private void adjustOffsetZoomLimits()
	{
		final float fx = (float) this.viewWidth / this.sourceWidth;
		final float fy = (float) this.viewHeight / this.sourceHeight;
		// Console.v(fx, fy);

		if (fx < fy)
		{
			this.mMinZoom = fx / fy;
			this.mMaxZoom = 10 / fx;
			this.mOffset.x = (this.sourceWidth / this.mMinZoom - this.sourceWidth) / 2 * fx;
			this.mOffset.y = 0;
			Console.i("fx < fy minzoom/maxzoon adjusted to", this.mMinZoom, this.mMaxZoom);

		}
		else
		{
			this.mMinZoom = fy / fx;
			this.mMaxZoom = 10 / fy;
			this.mOffset.x = 0;
			this.mOffset.y = (this.sourceHeight / this.mMinZoom - this.sourceHeight) / 2 * fy;
			Console.i("fx > fy minzoom/maxzoon adjusted to", this.mMinZoom, this.mMaxZoom);
		}

		this.mScaleFactor = this.mFitCenter ? this.mMinZoom : 1F;
		postInvalidate();
	}

	@Override
	public boolean onTouchEvent(final MotionEvent ev)
	{
		// Let the ScaleGestureDetector inspect all events.
		this.mScaleDetector.onTouchEvent(ev);

		final int action = ev.getAction();
		switch (action & MotionEvent.ACTION_MASK)
		{
			case MotionEvent.ACTION_DOWN: {
				final float x = ev.getX();
				final float y = ev.getY();

				this.mLastTouchX = x;
				this.mLastTouchY = y;
				this.mActivePointerId = ev.getPointerId(0);
				break;
			}

			case MotionEvent.ACTION_MOVE: {
				final int pointerIndex = ev.findPointerIndex(this.mActivePointerId);
				final float x = ev.getX(pointerIndex);
				final float y = ev.getY(pointerIndex);

				// Only move if the ScaleGestureDetector isn't processing a gesture.
				if (!this.mScaleDetector.isInProgress())
				{
					final float dx = this.mLastTouchX - x;
					final float dy = this.mLastTouchY - y;

					this.mOffset.x += dx * this.mScaleFactor;
					this.mOffset.y += dy * this.mScaleFactor;

					// Console.i("update offset", this.mOffset.y, this.mCachedOffset.y);
					invalidate();
				}

				this.mLastTouchX = x;
				this.mLastTouchY = y;

				break;
			}

			case MotionEvent.ACTION_UP: {
				this.mActivePointerId = INVALID_POINTER_ID;
				break;
			}

			case MotionEvent.ACTION_CANCEL: {
				this.mActivePointerId = INVALID_POINTER_ID;
				break;
			}

			case MotionEvent.ACTION_POINTER_UP: {
				final int pointerIndex = ((ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT);
				final int pointerId = ev.getPointerId(pointerIndex);
				if (pointerId == this.mActivePointerId)
				{
					// This was our active pointer going up. Choose a new
					// active pointer and adjust accordingly.
					final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
					this.mLastTouchX = ev.getX(newPointerIndex);
					this.mLastTouchY = ev.getY(newPointerIndex);
					this.mActivePointerId = ev.getPointerId(newPointerIndex);
				}
				break;
			}
		}

		return true;
	}

	long				start		= System.currentTimeMillis();

	private final Rect	mDecodeRect	= new Rect();

	private float		mPixelsPerPixel;

	private boolean		mDirty;
	final Paint			paint;

	private final Rect	mTempRect	= new Rect();

	private long		mLastDecodeTime;

	private long		mLastResizeTime;

	@Override
	public void run()
	{
		prepareBitmapForScale(ZoomImageView.this.mScaleFactor, ZoomImageView.this.mOffset);
		// SystemClock.sleep(1000);
		ZoomImageView.this.mIsWorkerDecoding = false;
	}

	@Override
	public void onDraw(final Canvas canvas)
	{
		super.onDraw(canvas);
		if (null == this.decoder)
		{
			return;
		}

		if (!this.mIsWorkerDecoding)
		{
			this.mIsWorkerDecoding = true;
			this.mWorker.post(this);
		}

		final Bitmap bitmap = this.mCachedBitmap;
		synchronized (this.mOffset)
		{
			if (null == bitmap)
			{
				canvas.drawText("Decoding...", 10, 30, this.paint);
			}
			else
			{
				canvas.drawColor(Color.BLACK);
				// final long draw = System.currentTimeMillis();
				final int decW = bitmap.getWidth();
				final int decH = bitmap.getHeight();
				// needed because decoded region may be smaller that the requested region,
				// so actual scale factor is not exactly mScaleFactor
				final float actualScaleFactor = Math.max((float) this.viewWidth / decW, (float) this.viewHeight / decH);

				computeDecodeRect(this.mTempRect, this.mScaleFactor, this.mOffset);

				final int offsetX = this.mTempRect.left - this.mDecodeRect.left;
				final int offsetY = this.mTempRect.top - this.mDecodeRect.top;
				final float deltaScaleX = (float) this.mDecodeRect.width() / this.mTempRect.width();
				final float deltaScaleY = (float) this.mDecodeRect.height() / this.mTempRect.height();
				// Console.v(offsetX ,offsetY, deltaScaleX , deltaScaleY);

				// this.mMatrix.setTranslate(this.mDecodeRect.left - rect.left , this.mDecodeRect.top - rect.top);
				// this.mMatrix.setRectToRect(new RectF(rect), new RectF(this.mDecodeRect), ScaleToFit.START);
				this.mMatrix.reset();
				this.mMatrix.postScale(actualScaleFactor, actualScaleFactor);
				if (deltaScaleX == 1 && deltaScaleY == 1)
				{
					this.mMatrix.postTranslate(-offsetX / this.mPixelsPerPixel, -offsetY / this.mPixelsPerPixel);
				}
				else
				{
					this.mMatrix.postScale(deltaScaleX, deltaScaleY, canvas.getWidth() / 2, canvas.getHeight() / 2);
				}

				// Console.v("rendering to", mDecodeRect, "with", this.mScaleFactor, "and", ms);
				canvas.drawBitmap(bitmap, this.mMatrix, null);
				canvas.drawText(String.valueOf(((int) (this.mScaleFactor * 100)) / 100F), 10, 30, this.paint);
				canvas.drawText(
						this.mDecodeRect.width() + " x " + this.mDecodeRect.height()
								+ " → " +
								bitmap.getWidth() + " x " + bitmap.getHeight()
						, 10, 60, this.paint);
				canvas.drawText(this.mLastDecodeTime + " ms (" + this.mLastResizeTime + " ms)", 10, 90, this.paint);

				// /canvas.scale(adjustX, adjustY);
				// Console.d("onDraw :", !isdecoding, - this.start + (this.start = System.currentTimeMillis()) , "ms, drawn in", this.start - draw, "ms");
				if (offsetX != 0 || offsetY != 0 || deltaScaleX != 1 || deltaScaleY != 1)
				{
					if (this.mIsWorkerDecoding)
					{
						this.mDirty = true;
					}
					else
					{
						invalidate();
					}
				}
			}
		}
	}

	/**
	 * @param scaleFactor
	 * @since 12 févr. 2013 12:19:19
	 * @author cte
	 * @param posX
	 * @param posY
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void prepareBitmapForScale(final float scaleFactor, final PointF offset)
	{
		final long start = System.currentTimeMillis();
		final Rect rect = new Rect();
		computeDecodeRect(rect, scaleFactor, offset);

		final int maxDim = Math.max(rect.width(), rect.height());
		this.options.inSampleSize = 1;
		if (this.mHighQuality)
		{
			while (maxDim > this.viewWidth * this.options.inSampleSize * 2)
			{
				this.options.inSampleSize *= 2;
			}
		}
		else
		{
			while (maxDim > this.viewWidth * this.options.inSampleSize)
			{
				this.options.inSampleSize *= 2;
			}
		}
		Bitmap bitmap;
		if (Version.IS_HONEYCOMB)
		{
			if (this.options.inBitmap != null
					&& (this.options.inBitmap.getWidth() != rect.width() || this.options.inBitmap.getHeight() != rect.height()))
			{
				this.options.inBitmap = null;
			}
			this.options.inMutable = true;
			try
			{
				bitmap = this.decoder.decodeRegion(rect, this.options);
			}
			catch (final IllegalArgumentException e)
			{
				adjustOffsetZoomLimits();
				return;
			}
		}
		else
		{
			try
			{
				bitmap = this.decoder.decodeRegion(rect, this.options);
			}
			catch (final OutOfMemoryError e)
			{
				System.gc();
				Console.ex(e);
				postInvalidate();
				return;
			}
		}

		if (this.mWorker.hasMessages(1234))
		{
			Console.v("dropping...");
			postInvalidate();
			return;
		}

		final long resize = System.currentTimeMillis();
		final int bw = bitmap.getWidth();
		final int bh = bitmap.getHeight();
		if (bw > 2048)
		{
			final Bitmap resized = Bitmap.createScaledBitmap(bitmap, 2048, bh * 2048 / bw, true);
			bitmap.recycle();
			bitmap = resized;
		}
		else if (bh > 2048)
		{
			final Bitmap resized = Bitmap.createScaledBitmap(bitmap, bw * 2048 / bh, 2048, true);
			bitmap.recycle();
			bitmap = resized;
		}

		if (this.mWorker.hasMessages(1234))
		{
			Console.v("dropping...");
			postInvalidate();
			return;
		}

		final boolean wasNull = null == this.mCachedBitmap;

		synchronized (this.mOffset)
		{
			// Console.v("scaled");
			this.mCachedOffset.set(offset);
			this.mDecodeRect.set(rect);
			this.mCachedBitmap = bitmap;
			final long current = System.currentTimeMillis();
			this.mLastDecodeTime = current - start;
			this.mLastResizeTime = current - resize;
			if (Version.IS_HONEYCOMB)
			{
				this.options.inBitmap = bitmap;
			}
		}

		if (wasNull || this.mDirty)
		{
			postInvalidate();
			this.mDirty = false;
		}
	}

	/**
	 * @param scaleFactor
	 * @param offset
	 * @return
	 * @since 12 févr. 2013 15:27:01
	 * @author cte
	 */
	protected void computeDecodeRect(final Rect decodeRect, final float scaleFactor, final PointF offset)
	{
		final float w = this.sourceWidth;
		final float h = this.sourceHeight;

		// Console.v("rendering from", w, "x", h);

		final float decodeW = w / scaleFactor;
		final float decodeH = h / scaleFactor;
		decodeRect.left = (int) (w - decodeW) / 2;
		decodeRect.right = (int) (w + decodeW) / 2;
		decodeRect.top = (int) (h - decodeH) / 2;
		decodeRect.bottom = (int) (h + decodeH) / 2;

		this.mPixelsPerPixel = Math.min(decodeW / this.viewWidth, decodeH / this.viewHeight);

		decodeRect.offset((int) (offset.x * this.mPixelsPerPixel), (int) (offset.y * this.mPixelsPerPixel));
	}

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener
	{

		@Override
		public boolean onScale(final ScaleGestureDetector detector)
		{
			final float previous = ZoomImageView.this.mScaleFactor;
			ZoomImageView.this.mScaleFactor *= detector.getScaleFactor();

			ZoomImageView.this.mScaleFactor = Math.max(ZoomImageView.this.mMinZoom, Math.min(ZoomImageView.this.mScaleFactor, ZoomImageView.this.mMaxZoom));
			if (previous != ZoomImageView.this.mScaleFactor)
			{
				invalidate();
			}
			return true;
		}
	}
}

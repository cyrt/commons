package com.cte.commons.views.indexable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.WrapperListAdapter;

public class IndexScroller {

    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(final Message msg) {
            super.handleMessage(msg);

            switch (IndexScroller.this.mState) {
                case STATE_SHOWING:
                    // Fade in effect
                    IndexScroller.this.mAlphaRate += (1 - IndexScroller.this.mAlphaRate) * 0.2;
                    if (IndexScroller.this.mAlphaRate > 0.9) {
                        IndexScroller.this.mAlphaRate = 1;
                        setState(STATE_SHOWN);
                    }

                    IndexScroller.this.mListView.invalidate();
                    fade(10);
                    break;
                case STATE_SHOWN:
                    // If no action, hide automatically
                    setState(STATE_HIDING);
                    break;
                case STATE_HIDING:
                    // Fade out effect
                    IndexScroller.this.mAlphaRate -= IndexScroller.this.mAlphaRate * 0.2;
                    if (IndexScroller.this.mAlphaRate < 0.1) {
                        IndexScroller.this.mAlphaRate = 0;
                        setState(STATE_HIDDEN);
                    }

                    IndexScroller.this.mListView.invalidate();
                    fade(10);
                    break;
            }
        }

    };

    private float mAlphaRate;
    private int mState = STATE_HIDDEN;
    private int mListViewWidth;
    private int mListViewHeight;
    private int mCurrentSection = -1;

    private boolean mIsIndexing = false;

    private ListView mListView = null;

    private SectionIndexer mIndexer = null;
    private String[] mSections = null;
    private boolean mCanDraw;

    private final RectF mIndexbarRect = new RectF();
    private final RectF mIndexbarMargins = new RectF();
    private final RectF mPreviewRect = new RectF();
    private final PointF mPreviewOffset = new PointF();
    private final float mPreviewPadding;

    private final Paint mIndexPaint = new Paint();
    private final Paint mIndexbarPaint = new Paint();
    private final Paint mPreviewPaint = new Paint();
    private final Paint mPreviewTextPaint = new Paint();

    private int mHeaderViewsCount;
    private float sectionHeight;
    private float textTopBase;
    private float mPreviewSizeW;
    private float mIndexbarWidth;

    private final float mStandardMargin;
    private final float mPreviewSizeH;
    private final float mRoundCorner;
    private float[] mIndexMeasure;
    private float[] mPreviewMeasure;

    private static final int STATE_HIDDEN = 0;
    private static final int STATE_SHOWING = 1;
    private static final int STATE_SHOWN = 2;
    private static final int STATE_HIDING = 3;

    private final float mIndexTextHeight;

    public IndexScroller(final Context context, final ListView lv) {
        this.mListView = lv;

        setAdapter(this.mListView.getAdapter());

        final float density = context.getResources().getDisplayMetrics().density;
        this.mIndexbarWidth = 20 * density;
        this.mStandardMargin = 10 * density;
        this.mPreviewPadding = 5 * density;
        this.mRoundCorner = 5 * density;

        this.mIndexbarMargins.left = this.mStandardMargin;
        this.mIndexbarMargins.top = this.mStandardMargin;
        this.mIndexbarMargins.right = this.mStandardMargin;
        this.mIndexbarMargins.bottom = this.mStandardMargin;

        final float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;

        this.mIndexPaint.setColor(Color.WHITE);
        this.mIndexPaint.setAntiAlias(true);
        this.mIndexPaint.setTextSize(12 * scaledDensity);

        this.mIndexTextHeight = this.mIndexPaint.descent()-this.mIndexPaint.ascent();

        this.mIndexbarPaint.setColor(Color.BLACK);
        this.mIndexbarPaint.setAntiAlias(true);

        this.mPreviewTextPaint.setColor(Color.WHITE);
        this.mPreviewTextPaint.setAntiAlias(true);
        this.mPreviewTextPaint.setTextSize(50 * scaledDensity);

        this.mPreviewPaint.setColor(Color.BLACK);
        this.mPreviewPaint.setAlpha(96);
        this.mPreviewPaint.setAntiAlias(true);
        this.mPreviewPaint.setShadowLayer(3, 0, 0, Color.argb(64, 0, 0, 0));

        this.mPreviewSizeW = this.mPreviewSizeH = 2 * this.mPreviewPadding + this.mPreviewTextPaint.descent() - this.mPreviewTextPaint.ascent();
    }


    @SuppressWarnings("unused")
    public void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
        this.mListViewWidth = w;
        this.mListViewHeight = h;

        this.mIndexbarRect.left = w - this.mIndexbarMargins.left - this.mIndexbarWidth;
        this.mIndexbarRect.top = this.mIndexbarMargins.top;
        this.mIndexbarRect.right = w - this.mIndexbarMargins.right;
        this.mIndexbarRect.bottom = h - this.mIndexbarMargins.bottom;

        this.mPreviewRect.left = (w - this.mPreviewSizeW) / 2;
        this.mPreviewRect.top = (h - this.mPreviewSizeH) / 2;
        this.mPreviewRect.right = (w + this.mPreviewSizeW) / 2;
        this.mPreviewRect.bottom = (h + this.mPreviewSizeH) / 2;

        setTextOffsets();
        setPreviewOffset();
    }

    /**
     * @return the index bar margins
     * @since 4 déc. 2012 16:11:41
     */
    public void setExtraIndexbarMargins(final float left, final float top, final float right, final float bottom)
    {
        this.mIndexbarMargins.left = this.mStandardMargin + left;
        this.mIndexbarMargins.top = this.mStandardMargin + top;
        this.mIndexbarMargins.right = this.mStandardMargin + right;
        this.mIndexbarMargins.bottom = this.mStandardMargin + bottom;

        final float w = this.mListViewWidth;
        final float h = this.mListViewHeight;
        this.mIndexbarRect.left = w - this.mIndexbarMargins.left - this.mIndexbarWidth;
        this.mIndexbarRect.top = this.mIndexbarMargins.top;
        this.mIndexbarRect.right = w - this.mIndexbarMargins.right;
        this.mIndexbarRect.bottom = h - this.mIndexbarMargins.bottom;

        setTextOffsets();
    }

    /**
     * @param width the new index bar width
     * @since 4 déc. 2012 16:41:00
     */
    public void setIndexbarWidth(final float width)
    {
        final float padded = width + this.mPreviewPadding / 2;
        this.mIndexbarWidth = padded;
        this.mIndexbarRect.left = this.mListViewWidth - this.mIndexbarMargins.left - padded;
    }

    public void setPreviewWidth(final float width)
    {
        final float padded = width + 2 * this.mStandardMargin;
        this.mPreviewSizeW = padded;

        final float w = this.mListViewWidth;

        this.mPreviewRect.left = (w - padded) / 2;
        this.mPreviewRect.right = (w + padded) / 2;

        setPreviewOffset();
    }

    public void setTextOffsets()
    {
    	if (this.mSections==null) return;
        this.sectionHeight = (this.mIndexbarRect.height() - 2 * this.mStandardMargin) / this.mSections.length;
        final float paddingTop = (this.sectionHeight - this.mIndexTextHeight) / 2 + this.mStandardMargin - this.mIndexPaint.ascent();
        this.textTopBase = this.mIndexbarRect.top + paddingTop;
    }

    /**
     * 
     * @since 5 déc. 2012 13:18:55
     * @author cte
     */
    private void setPreviewOffset()
    {
        this.mPreviewOffset.x = this.mPreviewRect.left + this.mPreviewSizeW / 2 - 1;
        this.mPreviewOffset.y = this.mPreviewRect.top + this.mPreviewPadding - this.mPreviewTextPaint.ascent() + 1;
    }

    public void draw(final Canvas canvas) {
        if (this.mState == STATE_HIDDEN)
        {
            return;
        }

        final int alpha = (int) (255 * this.mAlphaRate);
        this.mIndexPaint.setAlpha(alpha);
        this.mIndexbarPaint.setAlpha(alpha / 4);

        canvas.drawRoundRect(this.mIndexbarRect, this.mRoundCorner, this.mRoundCorner, this.mIndexbarPaint);

        float lineBottom = -1F;
        for (int i = 0; i < this.mSections.length; i++) {
            final float paddingLeft = (this.mIndexbarWidth - this.mIndexMeasure[i]) / 2;
            final float lineTop = this.textTopBase + this.sectionHeight * i;
            // check for overlapping, and skip this section if it overlaps the previous one
            if (lineTop >= lineBottom)
            {
                canvas.drawText(
                        this.mSections[i],
                        this.mIndexbarRect.left + paddingLeft,
                        lineTop,
                        this.mIndexPaint);
                lineBottom = lineTop + this.mIndexTextHeight;
            }
            else
            {
                canvas.drawCircle(this.mIndexbarRect.centerX(), lineTop - this.mIndexPaint.descent(), 2F, this.mIndexPaint);
            }
        }

        // Preview is shown when mCurrentSection is set
        if (this.mCurrentSection >= 0) {
            final String previewText = this.mSections[this.mCurrentSection];
            final float previewTextWidth = this.mPreviewMeasure[this.mCurrentSection];

            canvas.drawRoundRect(this.mPreviewRect, this.mRoundCorner, this.mRoundCorner, this.mPreviewPaint);
            canvas.drawText(
                    previewText,
                    this.mPreviewOffset.x - previewTextWidth / 2,
                    this.mPreviewOffset.y,
                    this.mPreviewTextPaint);
        }
    }

    public boolean onTouchEvent(final MotionEvent ev) {
        if (!this.mCanDraw)
        {
            return false;
        }
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // If down event occurs inside index bar region, start indexing
                if (this.mState != STATE_HIDDEN && contains(ev.getX(), ev.getY())) {
                    setState(STATE_SHOWN);

                    // It demonstrates that the motion event started from index bar
                    this.mIsIndexing = true;
                    // Determine which section the point is in, and move the list to that section
                    this.mCurrentSection = getSectionByPoint(ev.getY());

                    final int positionForSection = this.mIndexer.getPositionForSection(this.mCurrentSection);
                    this.mListView.setSelection(positionForSection + this.mHeaderViewsCount);
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (this.mIsIndexing) {
                    // If this event moves inside index bar
                    if (contains(ev.getX(), ev.getY())) {
                        // Determine which section the point is in, and move the list to that section
                        this.mCurrentSection = getSectionByPoint(ev.getY());

                        final int positionForSection = this.mIndexer.getPositionForSection(this.mCurrentSection);
                        this.mListView.setSelection(positionForSection + this.mHeaderViewsCount);
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (this.mIsIndexing) {
                    this.mIsIndexing = false;
                    this.mCurrentSection = -1;
                }
                if (this.mState == STATE_SHOWN)
                {
                    setState(STATE_HIDING);
                }
                break;
        }
        return false;
    }

    public void show() {
        if (this.mCanDraw)
        {
            if (this.mState == STATE_HIDDEN)
            {
                setState(STATE_SHOWING);
            }
            else if (this.mState == STATE_HIDING)
            {
                setState(STATE_HIDING);
            }
        }
    }

    public void hide() {
        if (this.mState == STATE_SHOWN)
        {
            setState(STATE_HIDING);
        }
    }

    public void setAdapter(final Adapter adapter) {
        if (adapter instanceof SectionIndexer) {
            this.mIndexer = (SectionIndexer) adapter;
            this.mSections = (String[]) this.mIndexer.getSections();

            this.mCanDraw = this.mSections != null && this.mSections.length > 1;

            if (!this.mCanDraw)
            {
                return;
            }

            final int length = this.mSections.length;
            this.mIndexMeasure = new float[length];
            this.mPreviewMeasure = new float[length];

            this.mHeaderViewsCount = this.mListView.getHeaderViewsCount();

            float barWidth = this.mIndexbarWidth;
            float previewWidth = this.mPreviewSizeW;
            for (int i = 0; i < length; i++)
            {
                final String item = this.mSections[i];
                barWidth = Math.max(barWidth, this.mIndexMeasure[i] = this.mIndexPaint.measureText(item));
                previewWidth = Math.max(previewWidth, this.mPreviewMeasure[i] = this.mPreviewTextPaint.measureText(item));
            }
            setIndexbarWidth(barWidth);
            setPreviewWidth(previewWidth);
        }
        else if (adapter instanceof WrapperListAdapter)
        {
            // if the adapter is wrapping another, go recursively...
            setAdapter(((WrapperListAdapter) adapter).getWrappedAdapter());
        }
        else
        {
            Log.v("indexer", "adapter not instanceof SectionIndexer");
            this.mCanDraw = false;
        }
    }

    private void setState(final int state) {
        if (state < STATE_HIDDEN || state > STATE_HIDING)
        {
            return;
        }

        this.mState = state;
        switch (this.mState) {
            case STATE_HIDDEN:
                // Cancel any fade effect
                this.mHandler.removeMessages(0);
                break;
            case STATE_SHOWING:
                // Start to fade in
                this.mAlphaRate = 0;
                fade(0);
                break;
            case STATE_SHOWN:
                // Cancel any fade effect
                this.mHandler.removeMessages(0);
                break;
            case STATE_HIDING:
                // Start to fade out after three seconds
                this.mAlphaRate = 1;
                fade(3000);
                break;
        }
    }

    private boolean contains(final float x, final float y) {
        // Determine if the point is in index bar region, which includes the right margin of the bar
        return (x >= this.mIndexbarRect.left && y >= this.mIndexbarRect.top && y <= this.mIndexbarRect.top + this.mIndexbarRect.height());
    }

    private int getSectionByPoint(final float y) {
        if (this.mSections == null || this.mSections.length == 0)
        {
            return 0;
        }
        if (y < this.mIndexbarRect.top + this.mIndexbarMargins.top)
        {
            return 0;
        }
        if (y >= this.mIndexbarRect.top + this.mIndexbarRect.height() - this.mIndexbarMargins.top)
        {
            return this.mSections.length - 1;
        }
        return (int) ((y - this.mIndexbarRect.top - this.mIndexbarMargins.top) / ((this.mIndexbarRect.height() - this.mIndexbarMargins.top - this.mIndexbarMargins.bottom) / this.mSections.length));
    }

    private void fade(final long delay) {
        this.mHandler.removeMessages(0);
        this.mHandler.sendEmptyMessageAtTime(0, SystemClock.uptimeMillis() + delay);
    }
}

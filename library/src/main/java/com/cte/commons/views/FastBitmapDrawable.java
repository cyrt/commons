/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cte.commons.views;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.ImageView.ScaleType;

import java.io.InputStream;

/**
 * Lightweight {@link Drawable} that mimics {@link BitmapDrawable} but with only the basic features.<ul>
 * <li>It can be created only from a {@link Bitmap} or an {@link InputStream}</li>
 * <li>It supports {@link #setAlpha(int) alpha}, {@link #setFilterBitmap(boolean) filtering} and {@link #setColorFilter ColorFilter}s</li>
 * </ul>
 * I does not support {@link Gravity gravity}, {@link ScaleType scaling}, {@link TileMode tiling} or adjusting {@link DisplayMetrics density}
 * and shows as if gravity is {@link Gravity#LEFT LEFT}|{@link Gravity#TOP TOP} and scaling is {@link ScaleType#FIT_XY FIT_XY}.
 * @since 20 sept. 2012 10:17:45
 * @author cte
 * @see com.android.launcher2
 */
public class FastBitmapDrawable extends Drawable
{
    protected Bitmap    mBitmap;
    protected int       mAlpha;
    private int         mWidth;
    private int         mHeight;
    private final Paint mPaint = new Paint(Paint.FILTER_BITMAP_FLAG);

    /**
     * Creates a Bitmap by reading it from the {@link InputStream}.<br>
     * No check is done, this is a raw decoding.
     * 
     * @since 20 sept. 2012 10:20:13
     * @author cte
     */
    public FastBitmapDrawable(final InputStream is)
    {
        this(BitmapFactory.decodeStream(is));
    }

    /**
     * Default constructor. Uses the provided {@link Bitmap} for drawing.
     * @param b
     * @since 20 sept. 2012 10:26:58
     * @author cte
     */
    public FastBitmapDrawable(final Bitmap b)
    {
        this.mAlpha = 255;
        setBitmap(b);
    }

    public void setBitmap(final Bitmap b)
    {
        this.mBitmap = b;
        if (b != null)
        {
            this.mWidth = this.mBitmap.getWidth();
            this.mHeight = this.mBitmap.getHeight();
        }
        else
        {
            this.mWidth = this.mHeight = 0;
        }
    }

    public Bitmap getBitmap()
    {
        return this.mBitmap;
    }

    @Override
    public void draw(final Canvas canvas)
    {
        if (null != mBitmap) {
            final Rect r = getBounds();
            canvas.drawBitmap(this.mBitmap, r.left, r.top, this.mPaint);
        }
    }

    @Override
    public void setColorFilter(final ColorFilter cf)
    {
        this.mPaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity()
    {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setAlpha(final int alpha)
    {
        this.mAlpha = alpha;
        this.mPaint.setAlpha(alpha);
    }

    @Override
    public void setFilterBitmap(final boolean filterBitmap)
    {
        this.mPaint.setFilterBitmap(filterBitmap);
    }

    public int getAlpha()
    {
        return this.mAlpha;
    }

    @Override
    public int getIntrinsicWidth()
    {
        return this.mWidth;
    }

    @Override
    public int getIntrinsicHeight()
    {
        return this.mHeight;
    }

    @Override
    public int getMinimumWidth()
    {
        return this.mWidth;
    }

    @Override
    public int getMinimumHeight()
    {
        return this.mHeight;
    }
}

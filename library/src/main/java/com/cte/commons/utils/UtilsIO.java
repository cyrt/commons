/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.cte.commons.utils;

import java.io.Closeable;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;


/**
 * Provides I/O operations
 * 
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 * @since 1.0.0
 */
public final class UtilsIO
{

	private static final int BUFFER_SIZE = 64 * 1024;

	private UtilsIO()
	{}

	public static String readStream(final InputStreamReader reader) throws IOException
	{
		final StringBuilder builder = new StringBuilder();
		final char[] buffer = new char[BUFFER_SIZE];
		int count;
		while ((count = reader.read(buffer, 0, BUFFER_SIZE)) >= 0)
		{
			builder.append(buffer, 0, count);
		}

		return builder.toString();
	}

	public static void copyStream(final InputStream in, final DataOutput out) throws IOException
	{
		final byte[] bytes = new byte[BUFFER_SIZE];
		int count;
		while ((count = in.read(bytes, 0, BUFFER_SIZE)) >= 0)
		{
			out.write(bytes, 0, count);
		}
	}

	public static void copyStream(final InputStream is, final OutputStream os) throws IOException
	{
		final byte[] bytes = new byte[BUFFER_SIZE];
		int count;
		while ((count = is.read(bytes, 0, BUFFER_SIZE)) >= 0)
		{
			os.write(bytes, 0, count);
		}
	}

	public static void copyStream(final Reader is, final Writer os) throws IOException
	{
		final char[] bytes = new char[BUFFER_SIZE];
		int count;
		while ((count = is.read(bytes, 0, BUFFER_SIZE)) >= 0)
		{
			os.write(bytes, 0, count);
		}
	}

	public static void closeSilently(final Closeable closeable)
	{
		try
		{
			if (null != closeable)
			{
				closeable.close();
			}
		}
		catch (final Exception e)
		{
			// Do nothing
		}
	}
}

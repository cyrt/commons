/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cte.commons.utils;

/**
 * Map of {@code long} to {@code long}. Unlike a normal array of longs, there
 * can be gaps in the indices. It is intended to be more efficient than using a {@code HashMap}.
 * 
 * @hide
 */
public class LongSparseLongArray implements Cloneable
{
	private long[]	mKeys;
	private long[]	mValues;
	private int		mSize;

	/**
	 * Creates a new SparseLongArray containing no mappings.
	 */
	public LongSparseLongArray()
	{
		this(10);
	}

	/**
	 * Creates a new SparseLongArray containing no mappings that will not
	 * require any additional memory allocation to store the specified
	 * number of mappings.
	 */
	public LongSparseLongArray(int initialCapacity)
	{
		initialCapacity = ArrayUtils.idealLongArraySize(initialCapacity);

		this.mKeys = new long[initialCapacity];
		this.mValues = new long[initialCapacity];
		this.mSize = 0;
	}

	@Override
	public LongSparseLongArray clone()
	{
		LongSparseLongArray clone = null;
		try
		{
			clone = (LongSparseLongArray) super.clone();
			clone.mKeys = this.mKeys.clone();
			clone.mValues = this.mValues.clone();
		}
		catch (final CloneNotSupportedException cnse)
		{
			/* ignore */
		}
		return clone;
	}

	/**
	 * Gets the long mapped from the specified key, or <code>0</code> if no such mapping has been made.
	 */
	public long get(final long key)
	{
		return get(key, 0);
	}

	/**
	 * Gets the long mapped from the specified key, or the specified value
	 * if no such mapping has been made.
	 */
	public long get(final long key, final long valueIfKeyNotFound)
	{
		final int i = ArrayUtils.binarySearch(this.mKeys, 0, this.mSize, key);

		if (i < 0)
		{
			return valueIfKeyNotFound;
		}

		return this.mValues[i];
	}

	public long[] getKeys(final boolean copy)
	{
		return copy ? this.mKeys.clone() : this.mKeys;
	}

	/**
	 * Removes the mapping from the specified key, if there was any.
	 */
	public void delete(final long key)
	{
		final int i = ArrayUtils.binarySearch(this.mKeys, 0, this.mSize, key);

		if (i >= 0)
		{
			removeAt(i);
		}
	}

	/**
	 * Removes the mapping at the given index.
	 */
	public void removeAt(final int index)
	{
		System.arraycopy(this.mKeys, index + 1, this.mKeys, index, this.mSize - (index + 1));
		System.arraycopy(this.mValues, index + 1, this.mValues, index, this.mSize - (index + 1));
		this.mSize--;
	}

	/**
	 * Adds a mapping from the specified key to the specified value,
	 * replacing the previous mapping from the specified key if there
	 * was one.
	 */
	public void put(final long key, final long value)
	{
		int i = ArrayUtils.binarySearch(this.mKeys, 0, this.mSize, key);

		if (i >= 0)
		{
			this.mValues[i] = value;
		}
		else
		{
			i = ~i;

			if (this.mSize >= this.mKeys.length)
			{
				growKeyAndValueArrays(this.mSize + 1);
			}

			if (this.mSize - i != 0)
			{
				System.arraycopy(this.mKeys, i, this.mKeys, i + 1, this.mSize - i);
				System.arraycopy(this.mValues, i, this.mValues, i + 1, this.mSize - i);
			}

			this.mKeys[i] = key;
			this.mValues[i] = value;
			this.mSize++;
		}
	}

	/**
	 * Returns the number of key-value mappings that this SparseIntArray
	 * currently stores.
	 */
	public int size()
	{
		return this.mSize;
	}

	/**
	 * Given an index in the range <code>0...size()-1</code>, returns
	 * the key from the <code>index</code>th key-value mapping that this
	 * SparseLongArray stores.
	 */
	public long keyAt(final int index)
	{
		return this.mKeys[index];
	}

	/**
	 * Given an index in the range <code>0...size()-1</code>, returns
	 * the value from the <code>index</code>th key-value mapping that this
	 * SparseLongArray stores.
	 */
	public long valueAt(final int index)
	{
		return this.mValues[index];
	}

	/**
	 * Returns the index for which {@link #keyAt} would return the
	 * specified key, or a negative number if the specified
	 * key is not mapped.
	 */
	public int indexOfKey(final long key)
	{
		return ArrayUtils.binarySearch(this.mKeys, 0, this.mSize, key);
	}

	/**
	 * Returns an index for which {@link #valueAt} would return the
	 * specified key, or a negative number if no keys map to the
	 * specified value.
	 * Beware that this is a linear search, unlike lookups by key,
	 * and that multiple keys can map to the same value and this will
	 * find only one of them.
	 */
	public int indexOfValue(final long value)
	{
		for (int i = 0; i < this.mSize; i++)
		{
			if (this.mValues[i] == value)
			{
				return i;
			}
		}

		return -1;
	}

	/**
	 * Removes all key-value mappings from this SparseIntArray.
	 */
	public void clear()
	{
		this.mSize = 0;
	}

	/**
	 * Puts a key/value pair into the array, optimizing for the case where
	 * the key is greater than all existing keys in the array.
	 */
	public void append(final long key, final long value)
	{
		if (this.mSize != 0 && key <= this.mKeys[this.mSize - 1])
		{
			put(key, value);
			return;
		}

		final int pos = this.mSize;
		if (pos >= this.mKeys.length)
		{
			growKeyAndValueArrays(pos + 1);
		}

		this.mKeys[pos] = key;
		this.mValues[pos] = value;
		this.mSize = pos + 1;
	}

	private void growKeyAndValueArrays(final int minNeededSize)
	{
		final int n = ArrayUtils.idealLongArraySize(minNeededSize);

		final long[] nkeys = new long[n];
		final long[] nvalues = new long[n];

		System.arraycopy(this.mKeys, 0, nkeys, 0, this.mKeys.length);
		System.arraycopy(this.mValues, 0, nvalues, 0, this.mValues.length);

		this.mKeys = nkeys;
		this.mValues = nvalues;
	}
}

/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cte.commons.utils;

import java.util.ArrayList;
import java.util.Arrays;

import android.util.SparseArray;

import com.cte.commons.Console;

/**
 * A {@link StopWatch} records start, laps and stop, and print them to logcat.
 */
public class StopWatch
{
	private static final SparseArray<StopWatch>	WATCHES		= Console.DEBUG_MODE ? new SparseArray<StopWatch>() : null;

	private final String						mLabel;

	private final ArrayList<Long>				mTimes		= new ArrayList<Long>();
	private final ArrayList<String>				mLapLabels	= new ArrayList<String>();

	private int									align;

	/**
	 * Creates a new {@link StopWatch} at position i.
	 * If one was already there, it will be stopped and destroyed.
	 * 
	 * @param i
	 * @param label
	 */
	public static void start(final int i, final String label)
	{
		if (null != WATCHES)
		{
			final StopWatch old = WATCHES.get(i);
			WATCHES.append(i, new StopWatch(label));
			if (null != old)
			{
				old.stopAndLog("-");
			}
		}
	}

	public static void lap(final int i, final String lapLabel)
	{
		try
		{
			if (null != WATCHES)
			{
				WATCHES.get(i).lap(lapLabel);
			}
		}
		catch (final Exception e)
		{
			// ignored
		}
		Console.i(i, lapLabel);
	}

	public static void stopAndLog(final int i, final String stopLabel)
	{
		try
		{
			if (null != WATCHES)
			{
				WATCHES.get(i).stopAndLog(stopLabel);
				WATCHES.remove(i);
			}
		}
		catch (final Exception e)
		{
			// ignored
		}
	}

	private StopWatch()
	{
		this.mLabel = null;
	}

	private StopWatch(final String label)
	{
		this.mLabel = label;
		this.align = label.length();
		lap("");
	}

	/**
	 * Record a lap.
	 */
	protected void lap(final String lapLabel)
	{
		this.mTimes.add(System.currentTimeMillis());
		this.mLapLabels.add(lapLabel);
		this.align = Math.max(this.align, lapLabel.length());
	}

	/**
	 * Stop it and log the result, if the total time >= {@code timeThresholdToLog}.
	 */
	protected void stopAndLog(final String stopLabel)
	{
		lap(stopLabel);

		final int size = this.mTimes.size();
		final long start = this.mTimes.get(0);
		final long stop = this.mTimes.get(size - 1);
		final long total = stop - start;

		final StringBuilder sb = new StringBuilder();
		sb.append(this.mLabel);
		sb.append(" +");
		sb.append(total);
		sb.append(":\n");

		final char[] pad = new char[this.align];
		Arrays.fill(pad, ' ');

		long last = start;
		for (int i = 1; i < size; i++)
		{
			final long current = this.mTimes.get(i);
			final String label = this.mLapLabels.get(i);
			final int padding = this.align - label.length();
			sb.append('\n');
			sb.append(label);
			sb.append(pad, 0, padding);
			sb.append(" +");
			final long delta = current - last;
			sb.append(delta);
			if (delta < 1000)
			{
				sb.append(' ');
				if (delta < 100)
				{
					sb.append(' ');
					if (delta < 10)
					{
						sb.append(' ');
					}
				}
			}
			sb.append(" | ");
			sb.append(current - start);
			last = current;
		}
		Console.v(sb);

		final int me = WATCHES.indexOfValue(this);
		if (me > -1)
		{
			final int key = WATCHES.keyAt(me);
			WATCHES.remove(key);
		}
	}
}

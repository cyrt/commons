/**
 *
 */
package com.cte.commons.utils;

import android.Manifest.permission;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.cte.commons.App;

/**
 * 
 * @since 15 nov. 2012 11:45:56
 * @author cte
 */
public class NetUtils
{
	private NetUtils()
	{
		// we only have static helpers here
	}

	/**
	 * Checks whether a data connection is actually available.
	 * If the connection is temporarily disconnected and reconnecting, this will return <code>false</code>.
	 * 
	 * @param unused
	 *            unused, leave it <code>null</code>
	 * @return
	 */
	public static boolean isNetworkConnected(final Context unused)
	{
		final ConnectivityManager connectivity = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			final NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
			{
				for (final NetworkInfo item : info)
				{
					if (item.isConnected())
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Checks whether a data connection is available or, at least, connecting.
	 * If you want to make sure the network is connected, use {@link #isNetworkConnected} instead,
	 * however it is often enough to know that data is going to be available, and let the network stack handle the connection delay.
	 * 
	 * @return <code>false</code> if data network is totally disabled (e.g. airplane mode, roaming, no coverage)<br>
	 *         <code>true</code> if data network is connected or connecting.
	 */
	public static boolean isNetworkReachable()
	{
		PackageUtils.enforcePermission(permission.ACCESS_NETWORK_STATE);

		final ConnectivityManager connectivity = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			final NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
			{
				for (final NetworkInfo item : info)
				{
					if (item.isConnectedOrConnecting())
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isWiFiActive()
	{
		final ConnectivityManager cm = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		final NetworkInfo active = cm.getActiveNetworkInfo();

		if (null != active)
		{
			if (ConnectivityManager.TYPE_WIFI == active.getType())
			{
				return true;
			}
		}

		return false;
	}

}

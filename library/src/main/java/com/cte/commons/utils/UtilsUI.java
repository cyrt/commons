/**
 *
 */
package com.cte.commons.utils;

import android.content.Context;
import android.content.Intent;
import android.content.Intent.ShortcutIconResource;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.TypedValue;

import com.cte.commons.Console;

/**
 * 
 * @since 7 août 2012 09:49:26
 * @author cte
 */
public class UtilsUI
{
	/**
	 * Force to create a shortcut at first launch.
	 * 
	 * @param context
	 *            The context of your application.
	 * @param app_name
	 *            resource id of the name displayed under the shortcut
	 * @param ic_launcher
	 *            resource id of the drawable used for the shortcut icon
	 * @return <code>true</code> if created
	 */
	public static final boolean installLauncherShortcut(final Context context, final int app_name, final int ic_launcher)
	{
		try
		{
			final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

			if (!preferences.getBoolean(Intent.ACTION_CREATE_SHORTCUT, false))
			{
				final Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
				intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getString(app_name));
				intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, ShortcutIconResource.fromContext(context, ic_launcher));
				intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()));

				context.sendBroadcast(intent);

				return preferences.edit().putBoolean(Intent.ACTION_CREATE_SHORTCUT, true).commit();
			}
		}
		catch (final Exception ex)
		{
			Console.e("Can't create shortcut", ex);
		}
		return false;
	}

	/**
	 * @param context
	 *            A Context.
	 * @param dp
	 *            The number of DIP to convert.
	 * @return The number of pixel of the given DP value.
	 */
	public static int getPixelsFromDp(final Context context, final float dp)
	{
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				dp, context.getResources().getDisplayMetrics());
	}
}

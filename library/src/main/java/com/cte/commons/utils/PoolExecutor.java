package com.cte.commons.utils;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import android.annotation.TargetApi;
import android.app.Service;
import android.os.Build;
import android.os.Handler;

import com.cte.commons.Console;
import com.cte.commons.Version;

/**
 * Variant of {@link ThreadPoolExecutor} with a variable pool size,
 * which will notify a listener after the last thread of the pool has died.
 * It is meant to be used in a foreground {@link Service}, so it can stop when the executor is idle.
 */

public class PoolExecutor extends ThreadPoolExecutor implements Runnable
{
	public static interface OnAfterExecuteListener
	{
		/**
		 * Invoked after a task has been completed, if the working queue is empty.
		 * 
		 * @param threadKeepAlive
		 *            lifetime of the threads in ms
		 */
		void onFinished(PoolExecutor executor);

		/**
		 * Used to obtain a handler where to post {@link Runnable}s.
		 * It is better if the handler is dedicated to this and not shared with something else.
		 * 
		 * @return a {@link Handler}
		 */
		Handler getHandler();

		/**
		 * Extend time between the end of the last task in the pool and the call of {@link #onFinished}.
		 * If 0, {@link #onFinished} will be called almost as soon as the last task dies.
		 * If >=1, this will keep the executor idle for a little while before calling {@link #onFinished}, so it can accept new tasks and resume operation.
		 * 
		 * @return at least 1
		 */
		long getKeepAliveMultiplier();
	}

	/**
	 * The default thread factory
	 */
	static class DefaultThreadFactory implements ThreadFactory
	{
		private final AtomicInteger	threadNumber	= new AtomicInteger(1);
		private final ThreadGroup	group;
		private final String		namePrefix;

		private static final int	PRIORITY		= (Thread.NORM_PRIORITY + Thread.MIN_PRIORITY) / 2;

		DefaultThreadFactory(final String name)
		{
			final SecurityManager s = System.getSecurityManager();
			this.group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
			this.namePrefix = name;
		}

		@Override
		public Thread newThread(final Runnable r)
		{
			final Thread t = new Thread(this.group, r,
					this.namePrefix + this.threadNumber.getAndIncrement(),
					0);
			if (t.isDaemon())
			{
				t.setDaemon(false);
			}
			if (t.getPriority() != PRIORITY)
			{
				t.setPriority(PRIORITY);
			}
			return t;
		}
	}

	private final OnAfterExecuteListener	listener;
	private final long						mKeepAlive;
	private boolean							mFinished;

	public PoolExecutor(final OnAfterExecuteListener clientService, final int queue, final String name)
	{
		super(queue, queue, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new DefaultThreadFactory(name));
		allowCoreThreadTimeOutCompat(true);
		this.listener = clientService;
		this.mKeepAlive = getKeepAliveTime(TimeUnit.MILLISECONDS);
	}

	@Override
	protected void beforeExecute(final Thread t, final Runnable r)
	{
		this.mFinished = false;
		super.beforeExecute(t, r);
		Console.v("starting new task on", t.getName(), "(active:", getPoolSize(), ')');
	};

	@Override
	protected void afterExecute(final Runnable r, final Throwable t)
	{
		super.afterExecute(r, t);
		if (Console.DEBUG_MODE)
		{
			Console.v("ended task", "(active:", getActiveCount(), '/', getPoolSize(), ')');
		}
		if (getQueue().isEmpty() && null != this.listener)
		{
			checkExecutor();
		}
	};

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public void allowCoreThreadTimeOutCompat(final boolean value)
	{
		if (Version.IS_GINGERBREAD)
		{
			super.allowCoreThreadTimeOut(value);
		}
	}

	public void checkExecutor()
	{
		final Handler handler = this.listener.getHandler();
		handler.removeCallbacks(this);
		handler.postDelayed(this, 1000 + this.mKeepAlive * this.listener.getKeepAliveMultiplier());
	}

	@Override
	public void run()
	{
		if (0 == getActiveCount())
		{
			this.mFinished = true;
			this.listener.onFinished(this);
		}
		else
		{
			this.mFinished = false;
		}
	}

	public boolean isFinished()
	{
		return this.mFinished;
	}
}
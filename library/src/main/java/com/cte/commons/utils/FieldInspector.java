/**
 * 
 */
package com.cte.commons.utils;

import com.cte.commons.Console;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * {@link FieldInspector} allows to {@link #toString() print} the names and values of the fields of any {@link Object} provided to its constructors.
 * 
 * @since 20 mars 2013 10:57:27
 * @author cte
 */
public class FieldInspector
{
    private final Object mObject;
    private final String mSeparator;

    /**
     * Default constructor with "\n" as default separator.
     * @param o
     * @since 20 mars 2013 10:57:21
     * @author cte
     */
    public FieldInspector(final Object o)
    {
        this(o, "\n");
    }

    /**
     * Constructor with custom separator.
     * @param o
     * @since 20 mars 2013 10:57:21
     * @author cte
     * @param separator
     */
    public FieldInspector(final Object o, final String separator)
    {
        this.mObject = o;
        this.mSeparator = separator;
    }

    /**
     * @return a {@link String} containing the names and values of the non-static fields of the wrapped {@link Object}.
     *          Protected and private fields are displayed as well as public ones.
     */
    @Override
    public String toString()
    {
        if (null != mObject) try
        {
            final Field[] fields = this.mObject.getClass().getDeclaredFields();
	        final StringBuilder sb = new StringBuilder();
            for (final Field f : fields)
            {
                if (!Modifier.isStatic(f.getModifiers()))
                {
                    f.setAccessible(true);
                    sb.append(f.getName()).append(": ").append(f.get(this.mObject)).append(this.mSeparator);
                }
            }
            sb.setLength(sb.length() - this.mSeparator.length());
            return sb.toString();
        }
        catch (final Exception e)
        {
            Console.e(e);
        }
        return String.valueOf(this.mObject);
    }
}
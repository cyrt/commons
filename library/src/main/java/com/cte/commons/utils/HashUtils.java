/**
 *
 */
package com.cte.commons.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.util.Base64;

/**
 * 
 * @since 15 janv. 2013 10:10:45
 * @author cte
 */
public class HashUtils
{
	private static final MessageDigest	DIGEST_SHA	= obtainInstance();
	private static final char[]			HEX_CHARS	= { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };

	/**
	 * Evil wrapper to avoid a compile time error:
	 * static initializer is not allowed to fail with a checked exception.<br>
	 * 
	 * 
	 * @throws ExceptionInInitializerError
	 *             wrapping the checked {@link java.security.NoSuchAlgorithmException}.
	 *             Not likely to ever happen, except if SHA-256 is someday removed from Android.
	 * @return SHA-256
	 * @since 15 janv. 2013 10:14:00
	 * @author cte
	 */
	private static MessageDigest obtainInstance()
	{
		try
		{
			return MessageDigest.getInstance("SHA-256");
		}
		catch (final java.security.NoSuchAlgorithmException e)
		{
			throw new ExceptionInInitializerError(e);
		}
	}

	public static String asHex(final byte[] buf)
	{
		final char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i)
		{
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}

	/**
	 * 
	 * @param input
	 * @return SHA256 from input
	 * @throws IOException
	 * @since 15 janv. 2013 10:17:54
	 * @author cte
	 */
	public static String computeSha256(final InputStream input) throws IOException
	{
		synchronized (DIGEST_SHA)
		{
			DIGEST_SHA.reset();
			final byte[] buffer = new byte[65536];
			int count;
			try
			{
				while ((count = input.read(buffer)) > -1)
				{
					DIGEST_SHA.update(buffer, 0, count);
				}
			}
			catch (final UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}

			return Base64.encodeToString(DIGEST_SHA.digest(), Base64.NO_WRAP);
		}
	}

	public static String hash(final String name, final boolean base64, final String input) throws IOException
	{
		final InputStream in = new ByteArrayInputStream(input.getBytes());
		return hash(name, base64, in);
	}

	public static String hash(final String name, final boolean base64, final InputStream input) throws IOException
	{
		MessageDigest digest;
		try
		{
			digest = MessageDigest.getInstance(name);
		}
		catch (final NoSuchAlgorithmException e1)
		{
			final IOException t = new IOException();
			t.initCause(e1);
			throw t;
		}
		final byte[] buffer = new byte[65536];
		int count;
		try
		{
			while ((count = input.read(buffer)) > -1)
			{
				digest.update(buffer, 0, count);
			}
		}
		catch (final UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		return base64 ?
				Base64.encodeToString(digest.digest(), Base64.NO_WRAP)
				: asHex(digest.digest());
	}

	/**
	 * 
	 * @param file
	 * @return SHA256 from file
	 * @throws IOException
	 * @since 15 janv. 2013 10:17:54
	 * @author cte
	 */
	public static String computeSha256(final File file) throws IOException
	{
		final FileInputStream fis = new FileInputStream(file);
		final String hash = computeSha256(fis);
		fis.close();

		return hash;
	}
}

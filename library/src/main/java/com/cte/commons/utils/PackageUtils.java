/**
 *
 */
package com.cte.commons.utils;

import java.util.List;
import java.util.NoSuchElementException;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;

import com.cte.commons.App;
import com.cte.commons.Console;
import com.cte.commons.Version;

/**
 * Miscellaneous utility functions, which are of general purpose and might be useful in every other class
 * 
 * @since 23 sept. 2011 17:05:48
 */
@SuppressWarnings("nls")
public class PackageUtils
{
	/**
	 * Retrieves the Authority defined in manifest for a given provider
	 * 
	 * @param provider
	 *            class that extends ContentProvider
	 * @return name of the authority
	 * @throws NoSuchElementException
	 *             if given provider is not defined in manifest
	 * @since 2 févr. 2012 10:45:06
	 * @author cte
	 */
	public static String getProviderAuthority(final Class<? extends ContentProvider> provider)
	{
		return Version.IS_GINGERBREAD ? getProviderAuthorityModern(provider) : getProviderAuthorityCompat(provider);
	}

	/**
	 * 
	 * @param forAuthority
	 * @return the proper content:// Uri for the given authority
	 * @since 30 mars 2012 14:42:01
	 * @author cte
	 */
	public static Uri getProviderUri(final String forAuthority)
	{
		return Uri.parse("content://" + forAuthority);
	}

	private static String getProviderAuthorityCompat(final Class<? extends ContentProvider> provider)
	{
		final Context app = App.getContext();
		final String providerName = provider.getName();
		try
		{
			final ProviderInfo[] providers = app.getPackageManager().getPackageInfo(app.getPackageName(), PackageManager.GET_PROVIDERS).providers;
			for (final ProviderInfo info : providers)
			{
				if (providerName.equals(info.name) && null != info.authority)
				{
					return info.authority;
				}
			}
		}
		catch (final PackageManager.NameNotFoundException ignored)
		{ /* exception cannot happen */}

		throw new NoSuchElementException(providerName + " is not defined in your Manifest"); //$NON-NLS-1$
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	static String getProviderAuthorityModern(final Class<? extends ContentProvider> provider)
	{
		final Context app = App.getContext();
		try
		{
			final ComponentName component = new ComponentName(app, provider);
			final ProviderInfo info = app.getPackageManager().getProviderInfo(component, 0);
			if (null != info.authority)
			{
				return info.authority;
			}
		}
		catch (final PackageManager.NameNotFoundException e)
		{ /* exception must not happen */}

		throw new NoSuchElementException(provider.getName() + " is not defined in your Manifest"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return {@code true} if the process hosting context is one of our private sub-processes
	 * @since 8 mars 2012 16:49:47
	 * @author cte
	 * @param context
	 *            hosted context
	 */
	public static boolean isProcessInBackground()
	{
		final String processName = getMyProcessName();
		return null == processName ? true : (processName.indexOf(':') >= 0);
	}

	/**
	 * Retrieve the name of the current process
	 * 
	 * @param context
	 * @return
	 */
	public static String getMyProcessName()
	{
		final int pid = android.os.Process.myPid();

		final ActivityManager am = (ActivityManager) App.getContext().getSystemService(Context.ACTIVITY_SERVICE);
		final List<RunningAppProcessInfo> list = am.getRunningAppProcesses();
		for (final RunningAppProcessInfo info : list)
		{
			if (info.pid == pid)
			{
				return info.processName;
			}
		}

		return null;
	}

	/**
	 * 
	 * @return the application's versionName
	 */
	public static String getApplicationVersionName()
	{
		final Context context = App.getContext();
		try
		{
			final PackageInfo pkgi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			if (null != pkgi)
			{
				return pkgi.versionName;
			}
		}
		catch (final NameNotFoundException e)
		{/* ignored */}

		return "<missing>";
	}

	/**
	 * @return the application's versionCode
	 * 
	 */
	public static int getApplicationVersionCode()
	{
		final Context context = App.getContext();
		try
		{
			final PackageInfo pkgi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			if (null != pkgi)
			{
				return pkgi.versionCode;
			}
		}
		catch (final NameNotFoundException e)
		{/* ignored */}

		return -1;
	}

	/**
	 * Prevent a code from running on the main thread.<br>
	 * It may be used before running a code that must not be in the UI thread, like networking or heavy file access.<br>
	 * This method is only enabled in {@link Console#DEBUG_MODE debug mode}, it is a no-op in releases, so there won't be a runtime penalty in released apps.
	 */
	public static void preventMainThread()
	{
		if (Console.DEBUG_MODE)
		{
			if (Looper.myLooper() != null && Looper.myLooper() == Looper.getMainLooper())
			{
				Console.e("This is not allowed on the main thread.");
				throw new SecurityException("This is not allowed on the main thread.");
			}
		}
	}

	/**
	 * Checks if <b>permission<b> is granted, either because it's declared in the Manifest or it is granted through an IPC from another app.
	 * 
	 * @param permission
	 * @return <code>true</code> if and only if the current context is allowed to use <b>permission<b>
	 */
	public static boolean checkPermission(final String permission)
	{
		final int granted = App.getContext().checkCallingOrSelfPermission(permission);
		return PackageManager.PERMISSION_GRANTED == granted;
	}

	/**
	 * Checks whether a <b>permission</b> is granted to us and throws a {@link SecurityException} if this is not the case.<br>
	 * This method should be used before entering a code that requires a given permission, to get a fast, clear (so more manageable) exception.<br>
	 * This method is only enabled in {@link Console#DEBUG_MODE debug mode}, it is a no-op in releases, so there won't be a runtime penalty in released apps.
	 * 
	 * @param permission
	 */
	public static void enforcePermission(final String permission)
	{
		if (Console.DEBUG_MODE && !checkPermission(permission))
		{
			Console.e(permission, "is not granted to this process. Check your AndroidManifest!");
			throw new SecurityException(permission + " is not granted to this process. Check your AndroidManifest!");
		}
	}
}

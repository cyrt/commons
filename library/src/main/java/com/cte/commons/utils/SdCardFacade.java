package com.cte.commons.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.os.Environment;

/**
 * An facade for the SD Card.<br/>
 * May be used to add files/images ... on the SD Card for testing purposes.
 * 
 */
public class SdCardFacade
{

    /**
     * Adds a the file matching the given resource id in the specified folder.
     * 
     * @param context The context to use to retrieve the matching file.
     * @param pathToParentFolder A path to the folder where to put the file. (will be created if needed)<br>
     *        Don't add {@link Environment#getExternalStorageDirectory()} as it is automatically added.
     * @param fileName The name of the file to add.
     * @param rawResourceId The resource id of the file to add.
     * @return the file
     * @throws IOException if an I/O operation failed
     */
    public static File addFile(final Context context, final String pathToParentFolder, final String fileName, final int rawResourceId) throws IOException
    {
        final File parent = addFolder(pathToParentFolder);
        final File file = new File(parent, fileName);

        if (!file.exists())
        {
            final InputStream in = context.getResources().openRawResource(rawResourceId);
            final FileOutputStream out = new FileOutputStream(file);

            copyFileAndClean(in, out);

            in.close();
            out.close();
        }
        return file;
    }

    /**
     * Removes a file from the specified parent folder.
     * 
     * @param pathToParentFolder A path to the folder containing the file.<br/>
     *        Don't add {@link Environment#getExternalStorageDirectory()} as it is automatically added.
     * @param fileName The name of the file to delete.
     * @return {@code true} if the file has been properly removed, {@code false} otherwise.
     */
    public static boolean removeFile(final String pathToParentFolder, final String fileName)
    {
        final File file = new File(Environment.getExternalStorageDirectory() + "/" + pathToParentFolder + "/" + fileName);
        return removeFile(file);
    }

    /**
     * Removes a file from the specified parent folder.
     * 
     * @param file A file object pointing to the file to remove.
     * @return {@code true} if the file has been properly removed, {@code false} otherwise.
     */
    public static boolean removeFile(final File file)
    {
        boolean hasBeenDeleted = false;
        if (file.isFile())
        {
            hasBeenDeleted = file.delete();
        }
        return hasBeenDeleted;
    }

    /**
     * Create the folder at the end point of this path. Creates missing parents if needed.
     * 
     * @param pathToFolder The path to the folder to create. <br/>
     *        Don't add {@link Environment#getExternalStorageDirectory()} as it is automatically added.
     * @return {@code true} if the folder already exists or has successfully been created.
     * @throws IOException
     */
    public static File addFolder(final String pathToFolder) throws IOException
    {
        final File folder = new File(Environment.getExternalStorageDirectory(), pathToFolder);
        folder.mkdirs();
        if (!folder.isDirectory())
        {
            throw new IOException("failed to create SDCard directory " + pathToFolder);
        }
        return folder;
    }

    /**
     * Removes the specified folder and everything that's inside it.
     * 
     * @param pathTofolder The path to the folder to remove. <br/>
     *        Don't add {@link Environment#getExternalStorageDirectory()} as it is automatically added.
     */
    public static void removeFolder(final String pathTofolder)
    {
        removeFolder(new File(Environment.getExternalStorageDirectory(), pathTofolder));
    }

    /**
     * Removes the specified folder and everything that's inside it.
     * 
     * @param file A file representing the folder to remove.
     * @return {@code true} if this file was deleted, {@code false} otherwise.
     */
    private static boolean removeFolder(final File file)
    {
        if (file.isDirectory())
        {
            for (final File child : file.listFiles())
            {
                removeFolder(child);
            }
        }
        return file.delete();
    }

    private static void copyFileAndClean(final InputStream in, final OutputStream out) throws IOException
    {
        final byte[] buffer = new byte[65536];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }

}

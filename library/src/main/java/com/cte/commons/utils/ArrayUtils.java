package com.cte.commons.utils;

import java.lang.reflect.Array;
import java.util.Arrays;

import android.text.TextUtils;

public class ArrayUtils
{

	/**
	 * Equivalent of {@link TextUtils#isEmpty} for arrays.
	 * 
	 * @param array
	 * @return <code>true</code> if <b>array</b> is <code>null</code> or zero length
	 */
	public static <T> boolean isEmpty(final T[] array)
	{
		return null == array || 0 == array.length;
	}

	/**
	 * Backport from {@link Arrays} for API <= 8
	 * 
	 * Copies {@code newLength} elements from {@code original} into a new array.
	 * If {@code newLength} is greater than {@code original.length}, the result is padded
	 * with the value {@code 0L}.
	 * 
	 * @param original
	 *            the original array
	 * @param newLength
	 *            the length of the new array
	 * @return the new array
	 * @throws NegativeArraySizeException
	 *             if {@code newLength < 0}
	 * @throws NullPointerException
	 *             if {@code original == null}
	 * @since 1.6
	 */
	public static long[] copyOf(final long[] original, final int newLength)
	{
		if (newLength < 0)
		{
			throw new NegativeArraySizeException(Integer.toString(newLength));
		}
		return copyOfRange(original, 0, newLength);
	}

	/**
	 * Backport from {@link Arrays} for API <= 8
	 * 
	 * Copies elements from {@code original} into a new array, from indexes start (inclusive) to
	 * end (exclusive). The original order of elements is preserved.
	 * If {@code end} is greater than {@code original.length}, the result is padded
	 * with the value {@code 0}.
	 * 
	 * @param original
	 *            the original array
	 * @param start
	 *            the start index, inclusive
	 * @param end
	 *            the end index, exclusive
	 * @return the new array
	 * @throws ArrayIndexOutOfBoundsException
	 *             if {@code start < 0 || start > original.length}
	 * @throws IllegalArgumentException
	 *             if {@code start > end}
	 * @throws NullPointerException
	 *             if {@code original == null}
	 * @since 1.6
	 */
	public static int[] copyOfRange(final int[] original, final int start, final int end)
	{
		if (start > end)
		{
			throw new IllegalArgumentException();
		}
		final int originalLength = original.length;
		if (start < 0 || start > originalLength)
		{
			throw new ArrayIndexOutOfBoundsException();
		}
		final int resultLength = end - start;
		final int copyLength = Math.min(resultLength, originalLength - start);
		final int[] result = new int[resultLength];
		System.arraycopy(original, start, result, 0, copyLength);
		return result;
	}

	/**
	 * Backport from {@link Arrays} for API <= 8
	 * 
	 * Copies elements from {@code original} into a new array, from indexes start (inclusive) to
	 * end (exclusive). The original order of elements is preserved.
	 * If {@code end} is greater than {@code original.length}, the result is padded
	 * with the value {@code 0L}.
	 * 
	 * @param original
	 *            the original array
	 * @param start
	 *            the start index, inclusive
	 * @param end
	 *            the end index, exclusive
	 * @return the new array
	 * @throws ArrayIndexOutOfBoundsException
	 *             if {@code start < 0 || start > original.length}
	 * @throws IllegalArgumentException
	 *             if {@code start > end}
	 * @throws NullPointerException
	 *             if {@code original == null}
	 * @since 1.6
	 */
	public static long[] copyOfRange(final long[] original, final int start, final int end)
	{
		if (start > end)
		{
			throw new IllegalArgumentException();
		}
		final int originalLength = original.length;
		if (start < 0 || start > originalLength)
		{
			throw new ArrayIndexOutOfBoundsException();
		}
		final int resultLength = end - start;
		final int copyLength = Math.min(resultLength, originalLength - start);
		final long[] result = new long[resultLength];
		System.arraycopy(original, start, result, 0, copyLength);
		return result;
	}

	/**
	 * Backport from {@link Arrays} for API <= 8
	 * 
	 * @param array
	 * @param startIndex
	 * @param endIndex
	 * @param value
	 * @return
	 */
	public static int binarySearch(final long[] array, final int startIndex, final int endIndex, final long value)
	{
		checkBinarySearchBounds(startIndex, endIndex, array.length);
		int lo = startIndex;
		int hi = endIndex - 1;

		while (lo <= hi)
		{
			final int mid = (lo + hi) >>> 1;
			final long midVal = array[mid];

			if (midVal < value)
			{
				lo = mid + 1;
			}
			else if (midVal > value)
			{
				hi = mid - 1;
			}
			else
			{
				return mid; // value found
			}
		}
		return ~lo; // value not present
	}

	private static void checkBinarySearchBounds(final int startIndex, final int endIndex, final int length)
	{
		if (startIndex > endIndex)
		{
			throw new IllegalArgumentException();
		}
		if (startIndex < 0 || endIndex > length)
		{
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Appends one element at the end of <b>array</b>.<br>
	 * If array is <code>null</code> or empty, then a new array containing only <b>element</b> will be returned.
	 * 
	 * @param kind
	 *            class of the elements in the array. It may be <code>null</code>, but only if you are sure <b>array</b> can never get <code>null</code>.
	 * @param array
	 * @param element
	 * @return <b>array</b> with <b>element</b> appended.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] appendElement(final Class<T> kind, final T[] array, final T element)
	{
		final T[] result;

		int end;
		if (array != null && (end = array.length) > 0)
		{
			result = newInstance(kind, array, end + 1);
			System.arraycopy(array, 0, result, 0, end);
		}
		else
		{
			end = 0;
			result = newInstance(kind, array, 1);
		}
		result[end] = element;
		return result;
	}

	public static int[] appendInt(final int[] cur, final int val)
	{
		if (cur == null)
		{
			return new int[] {
					val
			};
		}
		final int N = cur.length;
		for (int i = 0; i < N; i++)
		{
			if (cur[i] == val)
			{
				return cur;
			}
		}
		final int[] ret = new int[N + 1];
		System.arraycopy(cur, 0, ret, 0, N);
		ret[N] = val;
		return ret;
	}

	public static boolean contains(final int[] array, final int value)
	{
		for (final int element : array)
		{
			if (element == value)
			{
				return true;
			}
		}
		return false;
	}

	public static <T> boolean contains(final T[] array, final T value)
	{
		for (final T element : array)
		{
			if (element == null)
			{
				if (value == null)
				{
					return true;
				}
			}
			else
			{
				if (value != null && element.equals(value))
				{
					return true;
				}
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] emptyArray(final Class<T> kind)
	{
		return (T[]) Array.newInstance(kind, 0);
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] newInstance(final Class<T> kind, final T[] like, final int size)
	{
		if (like != null)
		{
			return (T[]) Array.newInstance(like.getClass().getComponentType(), size);
		}
		return (T[]) Array.newInstance(kind, size);
	}

	public static boolean equals(final byte[] array1, final byte[] array2, final int length)
	{
		if (array1 == array2)
		{
			return true;
		}
		if (array1 == null || array2 == null || array1.length < length
				|| array2.length < length)
		{
			return false;
		}
		for (int i = 0; i < length; i++)
		{
			if (array1[i] != array2[i])
			{
				return false;
			}
		}
		return true;
	}

	public static int idealBooleanArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need);
	}

	public static int idealByteArraySize(final int need)
	{
		for (int i = 4; i < 32; i++)
		{
			if (need <= (1 << i) - 12)
			{
				return (1 << i) - 12;
			}
		}

		return need;
	}

	public static int idealCharArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need * 2) / 2;
	}

	public static int idealFloatArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need * 4) / 4;
	}

	public static int idealIntArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need * 4) / 4;
	}

	public static int idealLongArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need * 8) / 8;
	}

	public static int idealObjectArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need * 4) / 4;
	}

	public static int idealShortArraySize(final int need)
	{
		return ArrayUtils.idealByteArraySize(need * 2) / 2;
	}

	public static <T> T[] removeElement(final Class<T> kind, final T[] array, final T element)
	{
		if (array != null)
		{
			final int length = array.length;
			for (int i = 0; i < length; i++)
			{
				if (array[i] == element)
				{
					if (length == 1)
					{
						return null;
					}
					final T[] result = newInstance(kind, array, length - 1);
					System.arraycopy(array, 0, result, 0, i);
					System.arraycopy(array, i + 1, result, i, length - i - 1);
					return result;
				}
			}
		}
		return array;
	}

	public static int[] removeInt(final int[] cur, final int val)
	{
		if (cur == null)
		{
			return null;
		}
		final int N = cur.length;
		for (int i = 0; i < N; i++)
		{
			if (cur[i] == val)
			{
				final int[] ret = new int[N - 1];
				if (i > 0)
				{
					System.arraycopy(cur, 0, ret, 0, i);
				}
				if (i < N - 1)
				{
					System.arraycopy(cur, i + 1, ret, i, N - i - 1);
				}
				return ret;
			}
		}
		return cur;
	}

	public static long total(final long[] array)
	{
		long total = 0;
		for (final long value : array)
		{
			total += value;
		}
		return total;
	}

	private ArrayUtils()
	{}
}

package com.cte.commons.utils;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ReflectionUtils
{

	private ReflectionUtils()
	{
		// static
	}

	/**
	 * Builds a new array. This methods uses generic types to obtain an empty array first (hence the varargs), then allocate an array of the right size.
	 * Beware that 2 arrays are actually allocated for every call.
	 * 
	 * @param length
	 * @param array
	 *            varargs trick, don't use this parameter
	 * @return a new array matching the type of the left-hand side with given length
	 */
	@SuppressWarnings("unchecked")
	public static <E> E[] newArray(final int length, final E... array)
	{
		return (E[]) Array.newInstance(array.getClass().getComponentType(), length);
	}

	/**
	 * Attempts to instantiate a new Object matching the type of the LHS of the assignment.
	 * Beware that one additional temporary array is actually allocated for every call.
	 * 
	 * @param array
	 *            varargs trick, don't use this parameter
	 * @return a new instance
	 * @throws InstantiationException
	 *             if there is no accessible default (that is, zero-argument) constructor
	 */
	@SuppressWarnings("unchecked")
	public static <E> E newInstance(final E... array) throws InstantiationException
	{
		try
		{
			return (E) array.getClass().getComponentType().newInstance();
		}
		catch (final IllegalAccessException e)
		{
			final InstantiationException t = new InstantiationException(e.getMessage());
			t.initCause(e);
			throw t;
		}
	}

	/**
	 * Return the class object matching the type of the LHS of the assignment.
	 * Beware that one additional temporary array is actually allocated for every call.
	 * 
	 * @param array
	 * @return class object extracted at runtime.
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked")
	public static <E> Class<E> getClassObject(final E... array) throws InstantiationException
	{
		return (Class<E>) array.getClass().getComponentType();
	}

	/**
	 * @return the class object as defined in the class definition of target.
	 * @since 22 janv. 2014 15:35:48
	 * @author cte
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getGenericClassArgument(final Object target, final int which)
	{
		final Class<?> classObject = target.getClass();
		final Type superclass = classObject.getGenericSuperclass();
		final ParameterizedType genericSuperclass = (ParameterizedType) superclass;
		final Type actualArgument = genericSuperclass.getActualTypeArguments()[which];
		return (Class<T>) actualArgument;
	}

	/**
	 * @return the class object as defined in the interface definition of target.
	 * @since 22 janv. 2014 15:35:48
	 * @author cte
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getGenericInterfaceArgument(final Object target, final int which)
	{
		final Class<?> classObject = target.getClass();
		final Type superclass = classObject.getGenericInterfaces()[0];
		final ParameterizedType genericSuperclass = (ParameterizedType) superclass;
		final Type actualArgument = genericSuperclass.getActualTypeArguments()[which];
		return (Class<T>) actualArgument;
	}
}

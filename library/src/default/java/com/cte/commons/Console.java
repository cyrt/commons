/**
 *
 */
package com.cte.commons;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.test.suitebuilder.annotation.MediumTest;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.cte.commons.utils.ArrayUtils;
import com.cte.commons.utils.FieldInspector;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * For Debugging.<br/>
 * This class provides 3 sets of methods:
 * <ol>
 * <li>{@link #v}, {@link #d}, {@link #i}, {@link #w}, {@link #e}, which allow to print informations of "basic" types (see below)</li>
 * <li>Various {@code print*(...)} methods for printing complex objects with a proper formatting</li>
 * <li>Some utilities like {@link #notImplemented}, {@link #timedInfo}, {@link #notify} for special purposes.</li>
 * </ol>
 * <p>
 * The "basic" types handled by the first set of methods comprise:
 * <ul>
 * <li>all primitive types</li>
 * <li>any type that properly implements {@link Object#toString()}</li>
 * <li>arrays or iterable collections of the above types</li>
 * </ul>
 */
@SuppressWarnings("nls")
public class Console {
    private final static String MY_NAME = Console.class.getName();

    private final static boolean DEBUG_MAIN_TOGGLE = true;

    /**
     * Indicates if we can use {@link android.util.Log} class.
     * All {@code public} methods <b>MUST</b> be encapsulated in block "{@code if (Console.DEBUG_MODE)}" in order to allow Proguard to remove code from released builds.<br/>
     * {@link #DEBUG_MODE} <b>MUST</b> be a statically initialized constant.
     */
    public final static boolean DEBUG_MODE = DEBUG_MAIN_TOGGLE // manual override
            && BuildConfig.DEBUG;    // wrong if not released in debug mode

    /**
     * Minimum log level. Log below this level won't be printed to the console
     */
    public static int DEBUG_MIN_LEVEL = Log.VERBOSE;

    /**
     * Filter to allow debugs from ONLY the classes which filenames are in this list. Leave empty for allowing all classes
     */
    public final static List<String> ALLOWED_FILENAMES = new ArrayList<>();

    /**
     * Filter to REMOVE debugs from the classes which filenames are in this list. Leave empty for allowing all classes
     */
    public final static List<String> FILTERED_FILENAMES = new ArrayList<>();

    /**
     * Special modifier:
     * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
     * and if the following parameter is a number(*),
     * then this number will be displayed as an hexadecimal string.<br>
     * <br>
     * (*): supported types are:
     * <ul>
     * <li>{@link Integer}, {@link Long}, {@link Byte}, {@link Short} and their primitive equivalents,</li>
     * <li>{@link java.util.concurrent.atomic.AtomicInteger}, {@link java.util.concurrent.atomic.AtomicLong}</li>
     * </ul>
     */
    public final static Object HEX = new Object();
    /**
     * Special modifier:
     * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
     * the following parameter will be displayed using a {@link com.cte.commons.utils.FieldInspector}.
     */
    public final static Object FIELDS = new Object();
    /**
     * Special modifier:
     * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
     * it will prevent spaces to be inserted in-between following parameters.
     */
    public final static Object NOSP = new Object();
    /**
     * Special modifier:
     * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
     * it will request spaces to be inserted in-between following parameters.
     */
    public final static Object SP = new Object();

    /**
     * Special modifier:
     * When used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e}, the message will also be toasted.
     * When used as a parameter of {@link #notImplemented}, the message will be toasted instead of notified.
     */
    public final static Object TOAST = new Object();

    /**
     * Special modifier:
     * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
     * it will request the message to be notified in the status bar.
     */
    public final static Object NOTIFY = new Object();

    /**
     * Special modifier:
     * when used as a parameter of {@link #v}, {@link #d}, {@link #i}, {@link #w} or {@link #e},
     * the following parameter will be used as separator when printing arrays of {@link Object}s, {@link java.util.Map}s, {@link Iterable}s or {@link android.os.Bundle}s.
     * If needed, revert to default using {@link #DEFAULT_DELIM} as following parameter.
     */
    public final static Object DELIM = new Object();

    private final static String DEFAULT_DELIM = ", ";
    private static String sDelim = DEFAULT_DELIM;

    /**
     * tag for debug
     */
    private static String sDebugTag = MY_NAME;

    private static char[] sSpaces = {};
    private static long sStartTime = System.currentTimeMillis();
    private static long sDeltaTime = sStartTime;
    private static final ThreadLocal<StringBuilder> sStringBuilder = new ThreadLocal<StringBuilder>() {
        @Override
        protected StringBuilder initialValue() {
            return new StringBuilder(512);
        }
    };

    /**
     * @param debugTag the debug tag that will be used from now on
     * @since 1 août 2012 09:52:57
     */
    public static void setDebugTag(final String debugTag) {
        Console.sDebugTag = debugTag;
        Log.i(debugTag, "Tag updated to " + debugTag + "(" + DEBUG_MODE + ")");
    }

    /**
     * @param number       depth of stack trace
     * @param informations
     * @since 1 mars 2011 11:31:42
     */
    public static void printCallStack(final int number, final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.WARN) {
            final StackTraceElement[] stack = Thread.currentThread().getStackTrace();

            println(Log.WARN, informations);
            final int limit = Math.min(stack.length - 3, number);
            for (int i = 0; i < limit; i++) {
                Log.w(Console.sDebugTag, "at " + stack[i + 3].toString()); //$NON-NLS-1$
            }
        }
    }

    /**
     * @param fileName
     * @return {@code true} to CANCEL log, or {@code false} to allow log to be printed to console
     * @author cte
     * @since 25 oct. 2011 13:31:23
     */
    private final static boolean filterOut(final String fileName) {
        if (FILTERED_FILENAMES.contains(fileName)) {
            return true;
        }

        if (ALLOWED_FILENAMES.size() > 0 && !ALLOWED_FILENAMES.contains(fileName)) {
            return true;
        }

        return false;
    }

    protected static final void println(final int type, final Object... informations) {
        // if (!Log.isLoggable(DEBUG_TAG, type)) return;

        try {
            final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            int index = 2;
            StackTraceElement stackTraceElement;
            do {
                stackTraceElement = stackTrace[index++];
            } while (MY_NAME.equals(stackTraceElement.getClassName()));

            final String fileName = stackTraceElement.getFileName();
            if (filterOut(fileName)) {
                return;
            }

            final String methodName = stackTraceElement.getMethodName();

            boolean withSpaces = true;
            boolean toast = false;
            boolean notify = false;


            final StringBuilder sb = sStringBuilder.get();
            sb.setLength(0);
            sb.append("at ")
                    .append(stackTraceElement.getClassName())
                    .append('.')
                    .append(methodName)
                    .append("(")
                    .append(fileName)
                    .append(":")
                    .append(stackTraceElement.getLineNumber())
                    .append(") ");

            final int start = sb.length();
            for (int length = informations.length, i = 0; i < length; i++) {
                final Object info = informations[i];
                // handle modifiers
                if (info == HEX) {
                    if (i + 1 < length) {
                        final Object next = informations[i + 1];
                        if (next instanceof Integer) {
                            informations[i + 1] = Integer.toHexString((Integer) next);
                        } else if (next instanceof Long) {
                            informations[i + 1] = Long.toHexString((Long) next);
                        } else if (next instanceof Short) {
                            informations[i + 1] = Integer.toHexString((Short) next);
                        } else if (next instanceof Byte) {
                            informations[i + 1] = Integer.toHexString((Byte) next);
                        }
                        // else if (next instanceof Float)
                        // {
                        // informations[i + 1] = Float.toHexString((Float) next);
                        // }
                        // else if (next instanceof Double)
                        // {
                        // informations[i + 1] = Double.toHexString((Double) next);
                        // }
                        else if (next instanceof AtomicInteger) {
                            informations[i + 1] = Integer.toHexString(((AtomicInteger) next).get());
                        } else if (next instanceof AtomicLong) {
                            informations[i + 1] = Long.toHexString(((AtomicLong) next).get());
                        }
                    }
                    continue;
                }
                if (info == FIELDS) {
                    if (i + 1 < length) {
                        informations[i + 1] = new FieldInspector(informations[i + 1]);
                    }
                    continue;
                }
                if (info == NOSP) {
                    withSpaces = false;
                    continue;
                }
                if (info == SP) {
                    withSpaces = true;
                    continue;
                }
                if (info == DELIM) {
                    i++;
                    if (i < length) {
                        sDelim = String.valueOf(informations[i]);
                    }
                    continue;
                }
                if (info == TOAST) {
                    toast = true;
                    continue;
                }
                if (info == NOTIFY) {
                    notify = true;
                    continue;
                }
                // handle infos
                append(sb, info);
                if (withSpaces) {
                    sb.append(' ');
                }
            }

            final String message = sb.toString();

            if (toast) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(App.getContext(), message.substring(start), Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (notify) {
                doNotify(null, message.substring(start));
            }

            switch (type) {
                case Log.ERROR:
                    Log.e(sDebugTag, message);
                    break;
                case Log.WARN:
                    Log.w(sDebugTag, message);
                    break;
                case Log.INFO:
                    Log.i(sDebugTag, message);
                    break;
                case Log.DEBUG:
                    Log.d(sDebugTag, message);
                    break;
                case Log.VERBOSE:
                default:
                    Log.v(sDebugTag, message);
                    break;
            }
        } catch (final Exception e) {/* ignored */}
    }

    /**
     * @param sb
     * @param info
     * @author cte
     * @since 20 avr. 2012 09:49:07
     */
    private static void append(final StringBuilder sb, final Object info) {
        if (info == null) {
            sb.append("<null>");
        } else {
            if (info instanceof Object[]) {
                sb.append('(').append(info.getClass().getSimpleName());
                sb.setLength(sb.length() - 1); // remove [
                sb.append(((Object[]) info).length).append("]) [");
                for (final Object item : (Object[]) info) {
                    sb.append(item).append(sDelim);
                }
                sb.setLength(sb.length() - 2);
                sb.append(']');
            } else if (info instanceof boolean[]) {
                sb.append("(boolean[").append(((boolean[]) info).length).append("]) ");
                sb.append(Arrays.toString((boolean[]) info));
            } else if (info instanceof int[]) {
                sb.append("(int[").append(((int[]) info).length).append("]) ");
                sb.append(Arrays.toString((int[]) info));
            } else if (info instanceof long[]) {
                sb.append("(long[").append(((long[]) info).length).append("]) ");
                sb.append(Arrays.toString((long[]) info));
            } else if (info instanceof char[]) {
                sb.append("(char[").append(((char[]) info).length).append("]) ");
                sb.append(Arrays.toString((char[]) info));
            } else if (info instanceof short[]) {
                sb.append("(short[").append(((short[]) info).length).append("]) ");
                sb.append(Arrays.toString((short[]) info));
            } else if (info instanceof float[]) {
                sb.append("(float[").append(((float[]) info).length).append("]) ");
                sb.append(Arrays.toString((float[]) info));
            } else if (info instanceof double[]) {
                sb.append("(double[").append(((double[]) info).length).append("]) ");
                sb.append(Arrays.toString((double[]) info));
            } else if (info instanceof byte[]) {
                sb
                        .append("(byte[]) {length:")
                        .append(((byte[]) info).length)
                        .append("}");
            } else if (info instanceof Map) {
                @SuppressWarnings("rawtypes") final Map map = (Map) info;
                sb.append("Map(").append(map.size()).append(") {");
                @SuppressWarnings({"unchecked", "rawtypes"}) final Iterator<Entry> it = map.entrySet().iterator();
                while (it.hasNext()) {
                    @SuppressWarnings("rawtypes") final Entry entry = it.next();
                    final Object key = entry.getKey();
                    if (key != info) {
                        sb.append(key);
                    } else {
                        sb.append("(this)");
                    }
                    sb.append('=');
                    final Object value = entry.getValue();
                    if (value != info) {
                        append(sb, value);
                    } else {
                        sb.append("(this)");
                    }
                    if (it.hasNext()) {
                        sb.append(sDelim);
                    }
                }
                sb.append('}');
            } else if (info instanceof Iterable<?>) {
                sb.append(TextUtils.join(sDelim, (Iterable<?>) info));
            } else if (info instanceof Bundle) {
                final Bundle bundle = (Bundle) info;
                final Set<String> keySet = bundle.keySet();
                sb.append("Bundle[");
                for (final String key : keySet) {
                    sb.append(key).append(": ");
                    append(sb, bundle.get(key));
                    sb.append(sDelim);
                }
                sb.setLength(sb.length() - 2);
                sb.append(']');
            } else {
                sb.append(info.toString());
            }
        }
    }

    /**
     * Print some verbose
     *
     * @param informations List of informations to print
     */
    public final static void v(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE) {
            Console.println(Log.VERBOSE, informations);
        }
    }

    /**
     * Print some info
     *
     * @param informations List of informations to print
     */
    public final static void i(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.INFO) {
            Console.println(Log.INFO, informations);
        }
    }

    /**
     * Print some warning
     *
     * @param informations List of informations to print
     */
    public final static void w(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.WARN) {
            Console.println(Log.WARN, informations);
        }
    }

    /**
     * Print some debugs
     *
     * @param informations List of informations to print
     */
    public final static void d(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.DEBUG) {
            Console.println(Log.DEBUG, informations);
        }
    }

    /**
     * Print some error
     *
     * @param informations List of informations to print
     */
    public final static void e(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.ERROR) {
            Console.println(Log.ERROR, informations);
        }
    }

    /**
     * Toast a message
     *
     * @param informations List of informations to print
     */
    public final static void t(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.ERROR) {
            Console.println(Log.INFO, ArrayUtils.appendElement(Object.class, informations, TOAST));
        }
    }

    /**
     * Resets the current timestamp without printing anything.<br>
     * Must be called before first call to {@link #timedInfo}.
     *
     * @author cte
     * @since 8 déc. 2011 11:53:47
     */
    public final static void markTime() {
        sDeltaTime = System.currentTimeMillis();
    }

    /**
     * Print some warning with timestamp, and update the timestamp.<br>
     * The time displayed is the time elapsed since the last call to this method or to {@link #markTime()}
     *
     * @param informations List of informations to print
     */
    public final static void timedInfo(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.WARN) {
            final long current = System.currentTimeMillis();
            final String timestamp = "[+" /* + (current - sStartTime) + '/' */ + (current - sDeltaTime) + "ms]";
            sDeltaTime = current;
            Console.println(Log.INFO, timestamp, TextUtils.join(" ", informations));
        }
    }

    /**
     * Print some warning and resets timestamp
     *
     * @param informations
     * @since 22 sept. 2011 17:13:23
     */
    public final static void r(final Object... informations) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.WARN) {
            sStartTime = sDeltaTime = System.currentTimeMillis();
            Console.println(Log.WARN, informations);
        }
    }

    private static boolean checkNull(final String label, final Object o) {
        if (null == o) {
            v(label, "is <null>");
            return false;
        }
        return true;
    }

    /**
     * enumerates content of an Intent
     *
     * @param intent
     */
    @MediumTest
    public final static void print(final Intent intent) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("Intent", intent)) {
            v(intent.getAction() + " / data: " + intent.getDataString());
            print(intent.getExtras());
        }
    }

    /**
     * prints indented json
     *
     * @param json
     * @author cte
     * @since 24 nov. 2011 10:59:22
     */
    public final static void print(final JSONObject json) {
        if (Console.DEBUG_MODE && checkNull("JSONObject", json)) {
            try {
                final String string = json.toString(3);
                String line;
                v("output JSON with", json.length(), "children");
                final BufferedReader reader = new BufferedReader(new StringReader(string), 256);
                while ((line = reader.readLine()) != null) {
                    Log.v(sDebugTag, line);
                }
            } catch (final Exception ignored) {
                w("malformed json!");
            }
        }
    }

    /**
     * enumerates content of a Bundle
     *
     * @param bundle
     */
    public final static void print(final Bundle bundle) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("Bundle", bundle)) {
            final StringBuilder sb = sStringBuilder.get();
            final Set<String> keySet = bundle.keySet();
            sb.append("bundle: " + keySet.size());

            int keyWidth = 16;
            for (final String key : keySet) {
                keyWidth = Math.max(keyWidth, key.length());
            }
            for (final String key : keySet) {
                sb.append("\n");
                appendFixedWidth(sb, keyWidth, ": ", key);
                append(sb, bundle.get(key));
            }

            v(sb);
        }
    }

    /**
     * enumerates content of a ContentValues
     *
     * @param values
     */
    @MediumTest
    public final static void print(final ContentValues values) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("ContentValues", values)) {
            final StringBuilder sb = sStringBuilder.get();
            final Set<Entry<String, Object>> valueSet = values.valueSet();
            sb.append("output ContentValues with ").append(valueSet.size()).append(" values:");
            for (final Entry<String, Object> entry : valueSet) {
                sb.append("\n").append(entry.getKey()).append(": ");
                append(sb, entry.getValue());
            }

        }
    }

    public final static void print(final Cursor cursor) {
        print(cursor, false, true);
    }

    /**
     * enumerates content of a Cursor
     *
     * @param cursor
     */
    @MediumTest
    public final static void print(final Cursor cursor, final boolean autoAdjust, final boolean withNulls) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("Cursor", cursor)) {
            if (cursor.isBeforeFirst() || cursor.isAfterLast()) {
                v("cannot dump cursor pointing to ", cursor.getPosition(), ": out of window of size", cursor.getCount());
                return;
            }
            final StringBuilder sb = sStringBuilder.get();

            int columnWidth = 30;
            if (autoAdjust) {
                for (final String col : cursor.getColumnNames()) {
                    columnWidth = Math.max(columnWidth, col.length());
                }
            }
            extractRow(sb, cursor, false, columnWidth, withNulls);
            v("dumping cursor with", cursor.getColumnCount(), "cols:", sb);
        }
    }

    /**
     * Convenience for calling {@link #printAll(android.database.Cursor)}
     *
     * @author cte
     * @since 16 juil. 2012 18:31:16
     */
    public final static void printAll(final Context context, final Uri uri) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("Context", context) && checkNull("Uri", uri)) {
            v(">> Dumping", uri);
            final Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            printAll(cursor);
            if (null != cursor) {
                cursor.close();
            }
        }
    }

    /**
     * Convenience for calling {@link #print(android.database.Cursor)} with a given limit
     *
     * @author cte
     * @since 16 juil. 2012 18:31:16
     */
    public final static void print(final Context context, final Uri uri, final int width) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("Context", context) && checkNull("Uri", uri)) {
            v(">> Dumping", uri);
            final Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            printAll(cursor, width, -1, true);
            if (null != cursor) {
                cursor.close();
            }
        }
    }

    /**
     * {@link #printAll(android.database.Cursor, int, int, boolean)}
     *
     * @param cursor
     * @author cte
     * @since 16 juil. 2012 18:31:16
     */
    public final static void printAll(final Cursor cursor) {
        printAll(cursor, 15, 0, false);
    }

    /**
     * print all rows in a cursor
     *
     * @param cursor
     * @param limit
     * @param head   if {@code true}, display the first rows of the cursor (head), else the last rows (tail)
     * @author cte
     * @since 11 janv. 2012 14:25:13
     */
    public final static void printAll(final Cursor cursor, final int width, final int limit, final boolean head) {
        if (Console.DEBUG_MODE && DEBUG_MIN_LEVEL <= Log.VERBOSE && checkNull("Cursor", cursor)) {
            final int savedPosition = cursor.getPosition();

            final StringBuilder sb = sStringBuilder.get().append("\n");
            for (final String name : cursor.getColumnNames()) {
                appendFixedWidth(sb, width, "|", name);
            }

            sb.setLength(sb.length() - 1);
            sb.append(']');

            final int nbRows = cursor.getCount();

            v("dumping cursor with", nbRows, "rows:", sb.toString());

            final int start, count;
            if (head) {
                start = 0;
                count = limit > 0 ? Math.min(limit, nbRows) : nbRows;
            } else {
                start = limit > 0 ? Math.max(0, nbRows - limit) : 0;
                count = nbRows;
            }
            for (int i = start; i < count; i++) {
                cursor.moveToPosition(i);
                sb.setLength(0);
                extractRow(sb, cursor, true, width, true);

                sb.setLength(sb.length() - 1);
                sb.append(']');

                Log.v(sDebugTag, sb.toString());
            }

            cursor.moveToPosition(savedPosition);
        }
    }

    /**
     * @param sb
     * @author cte
     * @since 11 janv. 2012 14:35:06
     */
    private static void appendFixedWidth(final StringBuilder sb, final int width, final String separator, final Object... values) {
        final int len = width + sb.length();
        for (final Object v : values) {
            sb.append(v);
        }
        if (sb.length() < len) {
            if (sSpaces.length < width) {
                sSpaces = new char[width];
                Arrays.fill(sSpaces, 0, width, ' ');
            }
            sb.append(sSpaces, 0, width);
        }
        sb.setLength(len);
        sb.append(separator);
    }

    /**
     * @param sb
     * @param cursor
     * @param singleLine
     * @param withNulls  TODO
     * @author cte
     * @since 11 janv. 2012 14:15:36
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void extractRow(final StringBuilder sb, final Cursor cursor, final boolean singleLine, final int width, final boolean withNulls) {
        final int count = cursor.getColumnCount();
        for (int c = 0; c < count; c++) {
            String value = null;
            try {
                value = cursor.getString(c);
            } catch (final Exception e) {
                try {
                    // the only value we can't recover as a string is Blob
                    value = "<" + cursor.getBlob(c).length + " bytes>";
                } catch (final Exception impossible) {
                    value = "<?>";
                }
            }
            if (null == value) {
                if (withNulls) {
                    value = "";
                } else {
                    continue;
                }
            }

            String type = "";
            if (Version.IS_HONEYCOMB) {
                switch (cursor.getType(c)) {
                    case Cursor.FIELD_TYPE_BLOB:
                        type = "B:";
                        break;
                    case Cursor.FIELD_TYPE_FLOAT:
                        type = "F:";
                        break;
                    case Cursor.FIELD_TYPE_INTEGER:
                        type = "I:";
                        break;
                    case Cursor.FIELD_TYPE_STRING:
                        type = "S" + value.length() + ":";
                        break;
                    case Cursor.FIELD_TYPE_NULL:
                        type = "N.";
                        break;
                }
            }
            if (singleLine) {
                appendFixedWidth(sb, width, "|", type, value);
            } else {
                sb.append("\n");
                appendFixedWidth(sb, 16, ": ", cursor.getColumnName(c));
                sb.append(type).append(value);
            }
        }
    }

    /**
     * Print an exception with full stacktrace
     *
     * @param exception Exception to print
     */
    public final static void ex(final Throwable exception, final Object... informations) {
        if (Console.DEBUG_MODE) {
            final StringBuilder sb = prepareException(exception, true);
            if (null != informations && informations.length > 0) {
                e(informations, "\n", sb);
            } else {
                e(sb);
            }
        }
    }

    /**
     * Prepare and return a formatted exception with full stacktrace
     *
     * @param exception Exception to print
     * @param forDebug
     * @return a StringBuilder containing the full exception trace
     */
    public final static StringBuilder prepareException(final Throwable exception, final boolean forDebug) {
        final long now = System.currentTimeMillis();
        final StringBuilder sb = new StringBuilder(4096);

        if (forDebug) {
            sb.append("EXCEPTION---START---EXCEPTION\n");
        }

        sb.append(String.format("%tF %tT.%tL %tz : %s", now, now, now, now, exception.getMessage()));
        sb.append(" : ");
        sb.append(exception.toString());
        sb.append(" : ");
        sb.append(exception.getClass().getName());
        sb.append('\n');

        appendTrace(sb, exception.getStackTrace(), 0, exception.getCause());

        if (forDebug) {
            sb.append("EXCEPTION---END---EXCEPTION");
        }

        sb.trimToSize();
        return sb;
    }

    /**
     * Append a trace to a StringBuilder and return it
     *
     * @param StringBuilder String buffer
     * @param trace         Trace to append
     * @param index         Index to start in the trace
     * @param cause         Cause of the trace
     */
    private final static void appendTrace(final StringBuilder StringBuilder, final StackTraceElement[] trace, final int index, final Throwable cause) {
        if (trace == null) {
            return;
        }

        final int size = trace.length;
        StackTraceElement stackTraceElement;
        for (int i = index; i < size; i++) {
            stackTraceElement = trace[i];
            final int lineNumber = stackTraceElement.getLineNumber();
            StringBuilder.append("\t\tat ")
                    .append(stackTraceElement.getClassName())
                    .append('.')
                    .append(stackTraceElement.getMethodName())
                    .append("(");
            if (-2 == lineNumber) {
                StringBuilder.append("Native Method");
            } else {
                StringBuilder.append(stackTraceElement.getFileName())
                        .append(":")
                        .append(lineNumber);
            }
            StringBuilder.append(")\n");
        }

        if (cause != null) {
            StringBuilder.append("Caused by : ");
            StringBuilder.append(cause.getMessage());
            StringBuilder.append(" : ");
            StringBuilder.append(cause.getClass().getName());
            StringBuilder.append('\n');
            appendTrace(StringBuilder, cause.getStackTrace(), 0, cause.getCause());
        }
    }

    /**
     * Log method not implemented now
     *
     * @param messages optional
     */
    public final static void notImplemented(final Context context, final Object... messages) {
        if (Console.DEBUG_MODE) {
            final StackTraceElement trace = Thread.currentThread().getStackTrace()[3];

            Console.w("Not implemented !", trace.getClassName() + '.' + trace.getMethodName(), " at ", trace.getLineNumber(), messages);
            if (null != messages && messages.length > 1) {
                for (final Object m : messages) {
                    if (m == TOAST) {
                        final Object[] t = ArrayUtils.removeElement(null, messages, TOAST);
                        Toast.makeText(App.getContext(), TextUtils.join(", ", t), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            }
            notify(context, "Not implemented !\n" + TextUtils.join(" ", messages));
        }
    }

    /**
     * Sends a notification in status bar
     *
     * @param informations
     * @author cte
     * @since 24 nov. 2011 09:56:45
     */
    public final static void notify(final Context context, final Object... informations) {
        if (Console.DEBUG_MODE) {
            w(ArrayUtils.appendElement(Object.class, informations, NOTIFY));
        }
    }

    private static void doNotify(Context context, final String contentText) {
        if (null == context) {
            context = App.getContext();
        }
        // Get a reference to the NotificationManager:
        final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // prepare the pending intent
        final PendingIntent contentIntent = PendingIntent.getActivity(
                context,
                12345,
                new Intent(),
                PendingIntent.FLAG_ONE_SHOT
        );

        // initialize the notification
        final Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setContentTitle(">>!DEBUG!<<")
                .setContentText(contentText)
                .setTicker(contentText)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentIntent(contentIntent)
                .build();

        // Pass the Notification to the NotificationManager:
        notificationManager.notify(Integer.MAX_VALUE - 126, notification);
    }

}

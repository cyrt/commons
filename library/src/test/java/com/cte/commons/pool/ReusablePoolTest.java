package com.cte.commons.pool;

import android.os.SystemClock;
import android.support.annotation.NonNull;

import com.cte.commons.async.ResultConditionVariable;

import junit.framework.TestCase;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by cyril on 29/09/2016.
 */
public class ReusablePoolTest extends TestCase {

    @Test
    public void testObtain() throws Exception {
        final long testpid = Thread.currentThread().getId();
        assertEquals(getReusablePool().obtain().longValue(), testpid);
        int maxThreads = 10;
        final CountDownLatch lock = new CountDownLatch(maxThreads);
        for (int i = 0; i < maxThreads; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    assertTrue(getReusablePool().obtain().longValue() != testpid);
                    lock.countDown();
                }
            }).start();
        }

        assertTrue(lock.await(10, TimeUnit.SECONDS));
    }

    @NonNull
    private ReusablePool<Long> getReusablePool() {
        return new ReusablePool<Long>(2) {
            @Override
            public Long newInstance() {
                return Thread.currentThread().getId();
            }
        };
    }

}